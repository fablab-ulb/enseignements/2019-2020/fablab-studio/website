# Domestic Agriculture :

## Summary

![titre](images/title_summary.PNG)
### Problematic (EN) :

Calls for Action (Identified by FAO) for the cuban agricultural sector:

- Rainwater harvesting
- Water saving technologies
- Management of infrastructure
- Transportation of goods + fertilizer (high price of diesel fuel)

### Question :
Due to the problem of food security and lack of appropriate transportation of goods to densely populated areas, it would be valuable for cubans to be able to grow their own vegetables in their homes, thus creating self sufficiency. How can we prove to them that this is possible and achieve self sufficiency as closely as possible?

_Hypothesis :_
Our Objectives:
1. Prove to cubans that they can easily grow their vegetables at home (on balconies, terraces or courtyards)
2. Come as close as possible to vegetable security self-sufficiency through analysis of various agricultural systems and finding the most suitable one.

How will we do this?
> In order to find the best solution to achieve our goal, we must first better understand the problem and its context:
1. Looking at recipes, find out what the most curcial vegetabls in a cuban diet is.
2. What quantity of these vegetables does an average cuban consume per day.
3. How much space do we need to grow these quantities per household per year.
4. Using this information, we analyze different agricultrual systems that work in cuban conditions and come up with a specialized system for these household that learns from global and local agricultural disciplines.

______________________

# Agriculture Domestique:

## Sommaire

### Problématique (EN):

Appels à l'action (identifiés par la FAO) pour le secteur agricole cubain:

- Récupération des eaux pluviales
- Technologies d'économie d'eau
- Gestion des infrastructures
- Transport de marchandises + engrais (prix élevé du diesel)

### Question:
En raison du problème de la sécurité alimentaire et du manque de transport approprié des marchandises vers les zones densément peuplées, il serait utile que les Cubains puissent cultiver leurs propres légumes chez eux, créant ainsi l'autosuffisance. Comment pouvons-nous leur prouver que cela est possible et atteindre l'autosuffisance le plus près possible?

_Hypothèse :_
Nos objectifs:
1. Prouvez aux cubains qu'ils peuvent facilement cultiver leurs légumes à la maison (sur les balcons, les terrasses ou les cours)
2. Se rapprocher le plus possible de l'autosuffisance en matière de sécurité végétale en analysant les différents systèmes agricoles et en trouvant celui qui convient le mieux.

Comment allons-nous procéder?
> Afin de trouver la meilleure solution pour atteindre notre objectif, nous devons d'abord mieux comprendre le problème et son contexte:
1. En regardant les recettes, découvrez quels sont les végétariens les plus curieux dans un régime cubain.
2. Quelle quantité de ces légumes consomme un cubain moyen par jour.
3. De combien d'espace avons-nous besoin pour augmenter ces quantités par ménage et par an.
4. En utilisant ces informations, nous analysons différents systèmes agricoles qui fonctionnent dans des conditions cubaines et nous proposons un système spécialisé pour ces ménages qui apprend des disciplines agricoles mondiales et locales.
______________________

# Agricultura Doméstica:

## Resumen

### Problemática (EN):

Llamados a la acción (identificados por la FAO) para el sector agrícola cubano:

- Agua de lluvia en las cosechas
- Tecnologías de ahorro de agua
- Gestión de la infraestructura.
- Transporte de bienes + fertilizantes (alto precio del combustible diesel)

### Pregunta:
Debido al problema de la seguridad alimentaria y la falta de transporte adecuado de bienes a áreas densamente pobladas, sería valioso para los cubanos poder cultivar sus propios vegetales en sus hogares, creando así la autosuficiencia. ¿Cómo podemos demostrarles que esto es posible y lograr la autosuficiencia lo más cerca posible?

_ Hipótesis: _
Nuestros objetivos:
1. Demuestre a los cubanos que pueden cultivar fácilmente sus verduras en casa (en balcones, terrazas o patios)
2. Acérquese lo más posible a la autosuficiencia en seguridad vegetal mediante el análisis de varios sistemas agrícolas y encontrando el más adecuado.

¿Como haremos esto?
> Para encontrar la mejor solución para lograr nuestro objetivo, primero debemos comprender mejor el problema y su contexto:
1. Mirando recetas, descubra cuáles son las verduras más curciales en una dieta cubana.
2. ¿Qué cantidad de estos vegetales consume un cubano promedio por día?
3. ¿Cuánto espacio necesitamos para cultivar estas cantidades por hogar por año?
4. Usando esta información, analizamos diferentes sistemas agrícolas que funcionan en condiciones cubanas y elaboramos un sistema especializado para estos hogares que aprende de las disciplinas agrícolas globales y locales.
