# Domestic Culture - Self Sufficient Vegetable Supply in Cuban Home

## Research Process

We started by gathering as much information as we can about the agricultural sector in Cuba. We needed to analyze the current situation, understand a bit of the history, and find out the main problems Cubans are facing in terms of food security in order to advance in the solutions we were to propose.
Though it is somewhat difficult to gather reliable information about the communist country, we were able to start off with some research prepared by the FAO (United Nations Food and Agriculture Organization. The most important facts we derived were the following:

### 1.	On Organoponics:
-	Havana has added a new word, organoponics, to the urban agriculture vocabulary, and has become a pioneer in a worldwide transition to sustainable agriculture that produces “more with less”. The Cubans called their solution organoponics because it uses an organic substrate, obtained from crop residues, household wastes and animal manure.
-	While Havana’s urban farmers have experimented with hydroponics, that technology depends on a reliable supply of chemical inputs.
-	Organoponic gardens proved ideal for growing crops on poor soils in small urban spaces. A typical organoponic garden is started by making furrows in the soil, then lining the rows with protective barriers of wood, stone, bricks or concrete. The soil quality is gradually improved through the incorporation of organic matter; as organic content increases, so do the levels of soil nutrients and moisture (and the height of the bed).
-	Organopónicos – the term applies to both the technology and the garden – can be applied on building sites, vacant lots and roadsides, and arranged in terraces on sloping land. If the soil is affected by nematodes or fungi, the entire substrate can be replaced. If necessary, the gardens can be disassembled and relocated.
-	With drip irrigation, regular addition of compost and good horticultural practices, the raised beds can produce vegetables all year round, and achieve yields of up to 20 kg per sq m.
-	Havana counted 97 high-yielding organoponics, which produce vegetables such as lettuce, chard, radish, beets, beans, cucumber, tomatoes, spinach and peppers.
-	It is strongly encouraged by the Cuban Government, which created the Havana Provincial Office of Agriculture, seven provincial technical departments and 15 municipal offices to assist the sector. The government has also introduced measures to grant vacant land free of charge for agriculture and to encourage the participation of women and youth.

![](images/Presentation1_CUBA_AGRI__1_-page-005.jpg)

Layering of an Organoponic Bed looks like the following:
![Layering of an Organoponic Bed](images/image__1_.png)


### 2.	Food Security:

-	The total area under agriculture in Havana is estimated at some 35 900 ha, or half the area of Havana Province. Production in 2012 included 63 000 tonnes of vegetables, 20 000 tonnes of fruit, 10 000 tonnes of roots and tubers, 10.5 million litres of cow, buffalo and goat milk and 1 700 tonnes of meat.
-	In addition, 89 000 backyards and 5 100 plots of less than 800 sq m are used by families in the city to grow fruit, vegetables and condiments and to raise small animals, such as poultry and guinea pigs, for household consumption.
-	In densely populated areas, food is produced in containers on rooftops and balconies. In all, some 90 000 Havana residents are engaged in some form of agriculture.
-	conventional agriculture requires around US$40 million worth of mineral fertilizer and US$2.8 million worth of pesticide.
-	The main cost is the diesel fuel needed to transport it an average distance of 10 km to the farmer’s field.
-	Havana also has 28 units that supply high-quality plant seedlings – mainly of tomato, cabbage, lettuce, cucumber, peppers and onions – in root balls, ready for transplanting in the field.

### 3.	Measures that need to be taken:

-	Among the Havana UPA programme’s strategic priorities are to realize the full productive potential of urban agriculture and to accelerate the organization of urban food producers.
-	Increase the output of biofertilizer and seed, strengthen support services, and build the capacities of producers in the management of infrastructure, the use of water-saving technologies and rainwater harvesting, and integrated pest management.

Source: http://www.fao.org/ag/agp/greenercities/en/ggclac/havana.html

### From this we deduced the main issues we are to deal with:
-	Food quality/safety
-	strong financial restrictions for agri sector
-	impacts derived from climatic effects with adverse effects on basic natural resources: land and water.
-	the main cost is the diesel fuel needed to transport pesticides and fertilizer an average distance of 10 km to the farmer’s field.
-	Fao want to increase the output of biofertilizer and seed, strengthen support services, and build the capacities of producers in the management of infrastructure, the use of water-saving technologies and rainwater harvesting, and integrated pest management.

### Finally, the main calls for action were reduced to the following:
-	Rainwater harvesting
-	Water saving technologies
-	Management of infrastructure
-	Transportation of goods + fertilizer (high price of diesel fuel)

![problems](images/Presentation1_CUBA_AGRI__1_-page-003.jpg)

### First Prototype

From there we kind of jumped to a “prototype-type” solution. We sought out to create a quick and easy solution which lets Cubans grow their own vegetables at home while using as least water as possible (because of the shortage of clean water in Cuba). Through this prototype we confronted two problems which were food security and water shortage.
The prototype was based on the idea of a “wicking bed”, in which plants were planted in a pot where at the bottom of it were several water reservoirs that would store the excess water. This excess water would then nourish the soil by capillarity automatically whenever the soil was dry and in need of water.

![first proto](images/first_try.PNG)

As you can see, the prototype is comprised of a plastic water bottle at the bottom acting as a reservoir, with a tube coming out of it for water input, and a cup attached next to it with holes inside it, so that the soil can touch the water in the bottle so that they can have contact and watering can be done by capillarity. Soil is then filled to the top of the “pot” made from a used textile washing powder container, and the seed is planted.

### Housing Typology Analysis

Although we knew this was a viable and important solution, we realized that we needed to take a step back and look for a solution that is more specific to Cuba. We could always integrate the wicking bed in our final proposition later on. Thus, we started looking at Cuban housing typologies to see what we were dealing with. In the end, our proposition had to be functional in these houses, since we are proposing for Cubans to grow their own vegetables at home. The main typologies we deduced were social housing typologies and housing-around-central-courtyard typologies.

Generic Housing typologies:
![typologies 1](images/Presentation1_CUBA_AGRI__1_-page-007.jpg)

As for social housing typologies:
![social](images/Presentation1_CUBA_AGRI__1_-page-010.jpg)

![social real](images/Presentation1_CUBA_AGRI__1_-page-009.jpg)

### Too Quick a Jump to Courtyard/Facade scale

However, our ambitions got the best of us and we sought out to create an even bigger solution incorporating what we have already learned, but making it bigger so that it would take the scale of a building façade in Havana, a densely populated area where nourishment is hard to get transported to, and so we were aiming that the Cubans could grow all the vegetables they need in their backyard or on their building façade.
We quickly learned about Hydroponics and the high yield this system gives. Although it does need a lot of maintenance, mainly in conserving a constant pH level of the water, we realized this could more or less be done through using greywater (which has a high density of nutrients and has a constant pH within the range needed).
The result was the following:
![facade scale](images/facade_scale.PNG)

Our idea here was to combine organoponics and hydroponics for maximum yield, however we later realized that perhaps hydroponics could be too advanced for Cuba and do not have a 100% success probability.

In any case, this is a diagram showing how that works:
![hybrid system](images/Presentation1_CUBA_AGRI__1_-page-019.jpg)
![hybrid system 2](images/Presentation1_CUBA_AGRI__1_-page-016.jpg)

An average pot for medium sized crops is 80 mm diameter, which made an average large water bottle extremely convenient with a diameter of 100mm:
![hybrid system 2](images/Presentation1_CUBA_AGRI__1_-page-017.jpg)

### A Deeper Look at Cuban Culture

Talking to the teachers after this, we realized together that we needed to tackle the problem more thoroughly. We needed to understand exactly how much nutrition an average Cuban family needs per year, and moreover to understand the most essential vegetables they use in their everyday meals. We needed to understand how much space it would need to grow these vegetables, in the most efficient manner of course.
Average household size (according to united nations):
[cuban family](images/Presentation2-CUBA-AGRI-page-012.jpg)

Main ingredients used in Cuban dishes:
[cuban ingredients](images/Presentation2-CUBA-AGRI-page-013.jpg)

### Square Meter Gardening

We sought out for a system that is not too far away from the agricultural practices Cubans are used to, yet much more efficient. We came across a globally acclaimed book titled “Square Foot Gardening” by Mel Bartholomew (1981), which later had a sequel titled “Square Meter Gardening” (2013) in order to account for advancements in this field.
Square foot gardening is the practice of dividing the growing area into small square sections, typically 1 foot on a side, hence the name. The aim is to assist the planning and creating of a small but intensively planted vegetable garden. It's a simple way to create easy-to-manage gardens with raised beds that need a minimum of time spent maintaining them. SFG rapidly gained popularity during the 1980s through Mel's first book and television series and since then has spread across the world, eventually going 'mainstream' with several companies offering ready-to-assemble SFG gardens. SFG advocates claim it produces more, uses less soil and water and takes just 2% of the time spent on a traditional garden. So what makes Square Foot Gardening special and why don't all gardeners use it?

![sq foot garden](images/sq_foot_garden.PNG)

Square Foot Gardening (commonly referred to as SFG) is a planting method that was developed by American author and TV presenter Mel Bartholomew in the 1970s. It's a simple way to create easy-to-manage gardens with raised beds that need a minimum of time spent maintaining them. SFG rapidly gained popularity during the 1980s through Mel's first book and television series and since then has spread across the world, eventually going 'mainstream' with several companies offering ready-to-assemble SFG gardens. SFG advocates claim it produces more, uses less soil and water and takes just 2% of the time spent on a traditional garden. So what makes Square Foot Gardening special and why don't all gardeners use it?
SFG was developed as a reaction to the inefficiencies of traditional gardening. In 1975 Mel Bartholomew had just retired as an engineer and decided to take up gardening as a hobby. It was only natural that he would apply his analytical skills to the problems he encountered. In particular he found the average gardener was spending hours weeding the big gaps between long rows of plants, creating unnecessary work for themselves. It soon became clear that getting rid of rows and using intensive deep-beds could dramatically cut the amount of maintenance the garden required. Add a one-foot square grid on top and it became easy to space and rotate crops.

![square meter garden](images/Square-foot-garden.JPG)

Over the years the SFG system has evolved into a precise set of rules:
-	Create Deep Raised Beds: Typically 4 feet by 4 feet, with a square foot lattice placed on top to visually separate the crops. Beds are between 6 and 12 inches deep which gives the plants plenty of rich nutrients, while maintaining good drainage.
-	Use a Specific Soil Mix: One third each of compost, peat moss and vermiculite. This starts the raised beds completely weed-free as well as being water retentive and full of nutrients.
-	Don't Walk on the Soil: This is now common practice with raised bed gardening but back in the 1970s it was revolutionary to suggest that you wouldn't need to dig your soil if you didn't tread on it.
-	Plant in Squares: To keep the planting simple there are no plant spacings to remember. Instead each square has either 1, 4, 9 or 16 plants in it depending on the size of the plant – easy to position in each square by making a smaller grid in the soil with your fingers. As an exception to this there are a few larger plants that span two squares. Climbing peas and beans are planted in two mini-rows of 4 per square.
-	Thin with Scissors: Instead of pulling up excess plants which can disturb the root systems of the plants you want to grow you snip them off with scissors.
-	Accessorise: As well as details of all the above the All New Square Foot Gardening  book has practical instructions for constructing various accessories including protective cages that easily lift on and off the SFG beds, covers to extend the season and supports for vertical growing.

From here we saw that it is crucial for the soil mix to be made up of Compost, Peat Moss, and Vermiculite. We thus needed to check for the availability of these materials in Cuba.
1.	Compost: Easy to come by in cuba, and is already used as a primary fertilizer for plants in the country (organoponics).
2.	Peat Moss: Hard to come by, but organic alternatives exist. Such alternatives include Leaves or Compost Manure, or Coconut Coir (fibers). These elements, on the contrary, do exist in abundance in Cuba. It supplies the soil with most of the nutrients it needs, while attracting earthworms and other microorganisms that are beneficial to the soil, enriching it along the way
3.	Vermiculite: Also hard to come by since it is produced by large fertilizer companies and is largely based on chemical reactions. Organic Alternative include Sawdust and wood bark. Sawdust loosens the soil and also holds moisture well. Shredded wood bark or wood chips both help loosen the soil and improve water drainage. They absorb water well, but do not hold it for long, so more frequent waterings might be necessary.
This system has been proven to work and many publications online confirm its success. One example of such a publication can be found here: https://www.ag.ndsu.edu/publications/lawns-gardens-trees/the-facts-of-square-foot-gardening

Here are examples of what you can plant in each square foot:
-	9 onions, beets, bush beans, bush peas, garlic or spinach
-	16 carrots or radishes
-	4 lettuce, chard, marigolds or kohlrabi
-	1 tomato, pepper, eggplant, broccoli, cabbage or corn
-	1 squash, cucumber or melon per 2 square feet
-	6 vining plants, such as beans or peas, on trellises

Although many vegetables can be grown in SFG gardens it struggles to accommodate larger plants (squash, melons, main-crop potatoes etc), perennials (globe artichokes, rhubarb) and fruit bushes/trees.

The advantages to SFG are:
-	The soil stays friable (easily crumbled or pulverized) because you never walk in the squares.
-	You can harvest many more vegetables because you’re planting in blocks instead of rows.
-	The squares are much easier to water because you aren’t wasting water between rows. The same holds true for fertilizer.
-	You have less weeding to do because the garden has no rows between plants and every square foot is dedicated to vegetables.
-	Pest control is easier.
-	You rotate crops by square instead of location.
-	The squares are more aesthetic and require far less work.
-	You don’t need to till each spring.
-	You can build trellises at the north ends of the squares to grow vining plants such as peas, beans and squash vertically, which saves even more space
-	This type of garden warms faster and drains better than traditional gardens.
-
The soil in square foot gardens should be a minimum of 6 inches deep, but 12 inches is better to accommodate root crops such as carrots, potatoes and parsnips.
To construct a square foot garden that is slightly above grade, obtain nonrotting wood (cedar or pressure-treated wood free of arsenic) that measures 4 feet by 12 inches by 2 inches and form a square. Then add the soil and add the divisions. The first SFG — 16 square feet — is ready to be planted.

Source: The Facts of Square Foot Gardening (H1597, Reviewed March 2014)
https://www.ag.ndsu.edu/publications/lawns-gardens-trees/the-facts-of-square-foot-gardening

### Quoting from the book: Square Meter Gardening, 2013 (Vol.2 of Square Foot Gardening, 1981 by same author)

Mel Bartholomew began to question the efficiency of the conventional gardening practices he’d been taught:
- Why do we grow vegetables in single rows?
- Why do we plant a whole packet of seeds in one row?
- Why do we have a space of one metre between rows?
- Why is fertiliser and water spread so liberally over huge patches of soil when the plants are cultivated in slim rows?
-
“I asked lots of experts and they all gave me the same answer: because that’s the way we’ve always done it,” he says. “So I decided to invent a new way because their way was too much work.”
After conducting a series of sowing and planting experiments, Bartholomew found that growing vegies in rows was nothing more than a technique passed on from large-field farmers. Growing in small squares proved to be a simpler and more economical solution better suited to the home gardener.
Bartholomew says one box will yield a large garden salad for one person each day. He says four boxes per person – or one 4 x 1 m box – is the most you’d ever need.


### Calculations

From here we began analyzing, through calculations and some extra research, what is the maximum output of vegetables we can grow in this system that covers the food security needed and comes as close as possible to the yearly/daily needs of an individual/family.
Starting from the main vegetables Cubans use in their recipes, we deduced the crops that produce the highest yield and can crow in our proposed system. The calculations took the following form:
![Table of planting](images/prod_timeline.PNG)

We used averages/means of growth time and crop yield for our calculations (no minimum and no maximum).
Thus, theoretically speaking and based on the fact that an average human being requires 180 g of vegetables a day (currently less than 20% of Cubans consume this), our proposed system in fact comes very close to covering the vegetable needs for an average family of 3 by using 1 square meter growing bed.

### Composting

In terms of composting, we researched DIY composting methods which Cubans can do on their balconies or backyards.

[composting](images/composting.PNG)

How to Make a DIY Compost Bin:

STEP 1: DRILL HOLES IN YOUR BIN
Use a drill to make 8 – 10 small holes in the bottom of the container for aeration purposes.

STEP 2: CREATE YOUR BASE
Place some shredded newspaper or dry leaves on the bottom of your compost bin, filling it about 1/8 – 1/4 full. We went the leaves route, since we’ve got PLENTY of those lying around.

STEP 3: ADD DIRT
Place dirt on top of the leaves or newspaper until the container is 1/2 full. Again, PLENTY of that.

STEP 4: TOSS IN YOUR FOOD SCRAPS!
Now place any food scraps or paper products that you’d like to compost. Check out this list for a pretty complete rundown of what you can and can’t compost. Surprisingly, things like lint and eggshells are compost friendly, while lime (too acidic) and dog “waste” (could carry disease) are off-limits

STEP 5: STIR YOUR COMPOST
Give your compost a little stir (very little, in our case) with a shovel or stick, making sure to cover your food scraps with dirt. Canine supervision is optional.

STEP 6: MOISTEN!
Spray with lukewarm water until moist, but not soaking wet. (Note: too much water can be the culprit if your compost starts to smell).

### Tutorial

Throughout the process we constantly tried to anticipate every possible problem. Finally, we thought about the fact that having such a successful garden as a novice gardener is easier said than done. We thus proposed a 3 phase system where Cubans can get into the habit of growing their own crops periodically, moving from one phase to another as the crop grows further and the gardener becomes more comfortable with what he/she is doing.
The 3-phase system is illustrated as follows:

1.	Providing Seedlings

![seeds](images/_pecha.jpg)

2.	Using a small growing kit to grow seedlings to an appropriate height.

![version1](images/KIT.jpg)

3.	When the crop grows to a small size, it is moved into the large square meter growing space for it to produce its vegetables.

![p3](images/finaaaal.png)

From here, the gardener can remain focused on only growing in the large growing bed, after he has understood the process of growing and composting thoroughly.

Check our tutorial for more detailed information!

Thank you.
