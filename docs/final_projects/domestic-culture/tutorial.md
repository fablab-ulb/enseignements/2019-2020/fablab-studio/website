## Home farming kit

## Tutorial

## Phase 1 : providing seeds

![seeds](images/_pecha.jpg)

Staples choosen : Tomatoes - Lettuce - Garlic

Difficulty level : foolproof

### Steps: Easy and cheapest way to provide seeds

-	( 1 to 9 ) : Tomato seeds
-	1- provide one juicy tomato
-	2- cut halfway
-	3-squeeze the interior into one cup
-	4- shake it clear with a bit of water
-	5-conserve the mixture in dry place ideally conserved within plastic jar
-	6- wait few days (2-3days) so the texture get like this : water distincts itself from seeds that go down.
-	7-put through a sieve, get rid of the gelling agent through which orbits the grain
-	8-let them dry for some couple of weeks (2-3)
-	9-up for conservation till 5years in good conditions
-	-	Rest : lettuce and garlic regrowing
-	1-Cut the bottom of the head of lettuce and hold it straight within a spoonful of water.
-	2-A fresh bulb is suffice for self-growing.


### Necessary tools:

-	Water
-	1 Tomato
-	Garlic bulbs
-	Lettuce
-	Envelope (for seed conservation- up to 5 years)

### Seeds provision/ staple :

-	1 Tomato : up to 60 seeds ahead
-	Garlic bulb : acting as the seed (bulb = endless regrowing)
-	Lettuce (Bottom of the head of lettuce = endless regrowing)





## Phase 2 : mobile kit for seedlings

1- Low-tech version :

### Necessary tools:

-	Plastic bottle
-	Toilet paper rolls
-	Egg box
-	wick cord
-	threads
-	tape
-	dirt

We can start by introducing a DIY way to grow your seedlings from the seeds handed in phase 1. This method requires mostly a sef-watering system (wick cord) to avoid water overuse, pesticides.
### Steps:

-	We can start having fun cutting toilet paper rolls to make small pots or containers (for compost, seeds..) that we're gonna attach later to other ones
-	Manage to have the egg box into even compartiments and stick them around the plastic bottle. The latter will be the reservoir
-	Dig little holes as necessary on both of the previous ones for wick cords
-	Make wick cords one end pass through the bottle , the other attached to the egg box shell
-	Fill with 50/50 soil and compost (egg shell mainly)and place seeds
-	Water irrigation is made at the seed's pace



![version1](images/KIT.jpg)




2- 3D Printed version :

![version2](images/KIT1.jpg)

Required STL Files :   [K1](STL/K1.stl)  +   [K3](STL/K3.stl)


K1  represents the first set of molds that defines a cluster made of two identical planter beds with incrasted reservoirs for garlic bulbs and tomato seeds, one adjacent quasi sharped angles component serving for compost and last, central cylinder for seeds.
K3 represents a voronoi-like hydroponic planter that plugs into a bottle and allows growing dirtless.
(other component as a rectangular frame can be added to add more unity to the ensemble)



The vegetables listed below can be started in water, but should be transplanted to dirt for full growth and harvest. Transplatation within very limited areas is next phase's investigation.

## Phase 3 : one square-meter

![p3](images/finaaaal.png)

Of course, this is the most advanced and efficient version you can make. You can also do the same without the water bottles, but you would require more water.
What is important is using non-rotting wood for the frames, cedar wood is a good example.

You have many examples of what you can plant in each square:
-	9 onions, beets, bush beans, bush peas, garlic or spinach
-	16 carrots or radishes
-	4 lettuce, chard, marigolds or kohlrabi
-	1 tomato, pepper, eggplant, broccoli, cabbage or corn
-	1 squash, cucumber or melon per 2 square feet
-	6 vining plants, such as beans or peas, on trellises
-


Have fun!
