# Meteorological buoys
# Tutorial

![Cassandra.Younes](images/toutt.jpg)

A meteorological buoy is an instrument which takes measurements and collects weather and ocean data. With the decline of the weather ship, they have taken a more primary role in measuring conditions over the open seas.  Wind data from buoys has smaller error than that from ships.
Weather buoys, like other types of weather stations, measure parameters such as air temperature above the ocean surface, wind speed (steady and gusting), barometric pressure, and wind direction. Since they lie in oceans and lakes, they also measure water temperature, wave height, and dominant wave period. Raw data is processed and can be logged on board the buoy and then transmitted via radio, cellular, or satellite communications to meteorological centers for use in weather forecasting and climate study.


## Measurements

The data that will be collected from the ocean's state is:

Air:

    -Wind speed

    -Wind direction

    -Air pressure

    -Humidity

Water:

    -Wave height

    -Wave direction

    -Wave period

    -Water temperature

Position:

    -GPS position

    -Orientation


### Difficulty

Hard (Programming, Price of the components, Waterproofing)


### Longevity

Several years (+- 5 years) with the right coating


### What you will need

-A strong hull (+- 33 hours of impression)

-Anemometer (+- 4 hours of impression)

-Wind direction indicator (+- 3 hours of impression)

-Round piece with holes (+- 30 minutes of impression)

-Several screws and bolts

-Bearings

-Clamping joints

-Sensors that can measure or calculate these properties

    -Encoder +- 5€

    -Compass sensor +- 15€

    -Barometer +- 10€

    -Humidity sensor +- 6€

    -Accel/Gyro/Temp +- 15€

    -Thermistor +- 2€

    -GPS Logger +- 45€

    -Arduino UNO +- 20€

    -Radio transmitter +- 25€


-A microcontroller (to process the data)

-A radio transmitter (to send the data on to the shore)

-A battery

-Solar panels

-Epoxy coating

-Anchoring system (rope or chain and weight)


### How to use the electronics and creating a compatible hull

The main goal if this buoy is to measure the properties of the air and water out in the ocean. For this the right sensors are needed. The following is an example of sensors that can be used and their needs for the design.


#### First step

The wind speed and direction will be measured with parts that are outside of the buoy. They will need to be as strong as the hull.

For the wind speed we use a light sensor that gets interrupts by a disc with holes. The interrupt speed is an indication for the rotation speed of an anemometer that turns with a speed that is in relation with the wind speed because they are linked by an axle. Bearings inside the top of the buoy help the axle turn smoothly and are maintained in place as well as the axle with small clamping joints.

![Cassandra.Younes](images/rfegdg.jpg)

For the wind direction we use a compass sensor that rotates with the weather vane to directly measure its orientation. It has to rotate seperatly from the anemometer to indicate the direction, otherwise it will turn constantly, this could be fixed with bearings.
These two components are connected to an axle that is hollow inside to let wires from the sensors pass to collect data that must be sent to the microcontroller inside the buoy. The wires are connected to the weather vane run downwards without getting tangled up by the anemometer.

![Cassandra.Younes](images/fhgffg.jpg)


#### Second step

The barometer and humidity sensor will have to be in contact with the outside air. The difficulty here is to keep them dry during storms.
A way to do this might be to create a small maze based on the buoy's natural orientation (anchor side down). The sensors would be integrated in the top part of this maze and water would have to follow a complicated S-shaped path that can drain itself from water when it is level again.
Here we divided the top buoy with a structure that creates small rooms that are hollowed but could be filled to integrate the maze to let wires go through and also to leave a space in the middle to integrate the system for the light sensor.

![Cassandra.Younes](images/vuedubas.jpg)


#### Third step

An accelerometer/magnetometer/gyroscope module is used to calculate the wave height, direction and period.
To measure the wave height we use the vertical acceleration over a period of time (this should resemble a sine wave) and apply a double integration to calculate the buoy's relative vertical position over this period. Then the wave height can be calculated by subtracting the lowest position from the following highest position for each period.
Once this is calculated, the wave period can be found by measuring the time between two peaks.
As we now know when the buoy is rising and when it is falling we can put this knowledge into relation with its horizontal orientation. When the buoy is going up we see what way it is tilted and in combination with the built-in compass sensor we can indicate the wave direction.


#### Fourth step

A GPS logger shield is used to calculate the global position of the buoy. For this a clear connection with the sky is necessary, perhaps an antenna must be used.


#### Fifth step

The aforementioned data and calculations will be processed by an Arduino Uno. All the sensors and components seen will find place in the bottom of the buoy in a waterproofed box.

![Cassandra.Younes](images/bassssss.jpg)


#### Sixth step

The water temperature can be measured by a thermistor that is integrated in the bottom hull.


#### Seventh step

This data then is transmitted by a radio transmitter. An efficient way to do this is by creating a mesh network of multiple buoys that collect and send on each others information.

![Cassandra.Younes](images/conen.jpg)


#### Eighth step

The battery will maintain the components active and has to be chosen depending on the needs of the electronical components and the solar panels chosen to provide the energy.
The solar panels can be integrated at the top of the buoy with link to the bottom and the battery.

![Cassandra.Younes](images/haut_photosh.jpg)


#### Ninth step

All wireholes into the hull must be sealed off, with an epoxy resin for example, these seals can be permanent.


#### Tenth step

Anchoring technique has to be decided after all the elements and componants are ready, the weight of the buoy has to be calculated and the distance with the ocean ground. The chain or rope has to be three times the lenght of the depth of the ocean.
There are multiple anchoring techniques with different materials and they are more adequate depending on the site data.

##### Anchoring types

![Cassandra.Younes](images/Type-ancrage.PNG)

##### Anchoring materials

![Cassandra.Younes](images/Materiaux-ancrage.PNG)

##### Anchoring site data

![Cassandra.Younes](images/Donnees-site-ancrage.PNG)

Another idea for stability would be to use plastic bottles as a raft that could be flexible and move with the waves.

![Cassandra.Younes](docs/final_projects/meteorological-buoy/images/montage.jpg)


##### Information of the used electronics can be found on following websites:

[An example of an infrared optical detector](https://www.makerstore.com.au/product/robo-sensor-speed/)

[An alternative basic encoder](https://www.antratek.be/rotary-encoder-illuminated-rgb?gclid=CjwKCAiAmNbwBRBOEiwAqcwwpZimwzb6LNTyDy7Aj_UsD4OoFkNLZbyPIVEulZbyd-un3xqrsL6_1BoCDGUQAvD_BwE)

[A compass sensor that can be used for the weather vane](https://www.adafruit.com/product/1120)

[Humidity sensor](https://www.seeedstudio.com/Grove-Temperature-Humidity-Sensor-DHT11.html)

[Barometer](https://www.adafruit.com/product/1893)

[Accelerometer/magnetometer/Gyroscope module documentation](https://www.adafruit.com/product/3387)

[How to use the thermometer](https://starter-kit.nettigo.eu/2010/how-to-measure-temperature-with-arduino-and-mcp9700/)

[Arduino uno](https://store.arduino.cc/arduino-uno-rev3)

[GPS Logger shield](https://starter-kit.nettigo.eu/2010/how-to-measure-temperature-with-arduino-and-mcp9700/)

[Transmitter documentation](https://learn.adafruit.com/adafruit-feather-m0-radio-with-rfm69-packet-radio)

[Example of a mesh network](https://www.instructables.com/id/Simple-Arduino-Wireless-Mesh/)


##### 3D STL Files

[STL File top buoy](STL-Files/hautbouéefinal.stl)

[STL File top lid](STL-Files/couverclehaut.stl)

[STL File top cap](STL-Files/bouuu.stl)

[STL File top down lid](STL-Files/couverclebas.stl)

[STL File anemometer](STL-Files/Anénomètrev12.stl)

[STL File wind direction](STL-Files/diret.stl)

[STL File axle](STL-Files/tigefinale.stl)

[STL File bolt](STL-Files/boulonhaut.stl)

[STL File turning piece](STL-Files/piecev2.stl)

[STL File bottom buoy](STL-Files/basbouéefinal.stl)

[STL File bottom lid](STL-Files/couverclebasbouée.stl)
