# Meteorological Buoy

# Research Process

## Cuba Weather State

Like most Caribbean islands, Cuba is not spared by cyclones. From May / June to November, the country is considered to be in a period of risk. There is on average more rain in these months due to this tropical storm activity. When hurricanes occur they generally aren't a risk to life, however they can cause some interruption to travel plans. The North Atlantic is the most studied tropical basin for high hurricane risk. The forecasts of these events are issued thanks to two centers mainly located in Florida and Canada.


![Cassandra.Younes](images/hurricane.jpg)

[Climate in Cuba](https://www.cubagrouptour.com/eu/information/cuba/weather/cuba-climate-and-weather.html)

[Meteorological Institut of Cuba](http://www.insmet.cu/asp/genesis.asp?TB0=PLANTILLAS&TB1=INICIAL)

[Hurricanes](https://en.wikipedia.org/wiki/Atlantic_hurricane)

Natural disasters are common in Cuba, the most recent one occurred in 2017, it was the first cyclone of category 5 (very high intensity) but not the most deadly, a dozen deaths, in comparison with the one that occured in 1932 which caused more than 3 thousands deaths. If Cubans have proved their resilience following such events, their study of disasters in advance would allow a better management of these cases.


![Cassandra.Younes](images/degat.jpg)

[Hurricane Irma](https://en.wikipedia.org/wiki/Hurricane_Irma)

[Hurricane of Santa Cruz del Sur](https://en.wikipedia.org/wiki/1932_Cuba_hurricane)

Meteorology is a science whose object is the study of atmospheric phenomena in order to understand how they are formed and evolve according to measured parameters. This makes it possible to obtain predictions. An important domain is marine weather forecasting as it relates to maritime and coastal safety and can also predict more information than land weather forecasting.

Speaking of marine weather forecasting and as far as meteorological buoys are concerned, them having the primary role in measuring conditions over the open seas after the decline of weather ships that present more errors (ex: the ship heats the water it is in contact with), Cuba does not seem to have these devices in large numbers.

According to an on-site contact Ernesto Altshuler, professor of physics in Cuba, there would be no more than five on the entire island due to their high price, not less than 10,000 USD. He is also working on a project to make cheaper meteorological buoy but here again money is a problem for financement.


![Cassandra.Younes](images/contact.jpg)

[Weather buoy](https://en.wikipedia.org/wiki/Weather_buoy)

[Weather buoys](http://hurricanescience.net/science/observation/ships/weatherbuoys/)

[Surf charts for Cuba](https://fr.surf-forecast.com/weather_maps/Cuba?over=none&symbols=liveswell&type=nav&fbclid=IwAR1LigTRIevtArr_a2XbltkN-2Jwx5Wzpk4RY7mtSQti4HbP_Eky9JEyPYo)

[Buoy data](https://www.ndbc.noaa.gov/?fbclid=IwAR2vse9xt3wnkA44joSvFlhG1wkmKSHSJ8eSfFrXZcPeBEMRbcQcaNcDAeQ)

[Ernesto Altschuler](http://www.complexperiments.net/EAltshuler/HomeAlt.htm)

Meteorological buoys collect data from the oceans, mainly temperature, wave height, wave period, wave direction and air pressure as well as wind speed.

That is why we want to design a simple meteorological buoy that can be reproduced, and be very cheap so more data can be collected by Cubans to make better predictions of the weather. The simpler the design, the better, so it can be easily understood and reproduced.


![Cassandra.Younes](images/ze.jpg)

-Gutiérrez Sara, "Analyse d’un transfert de technologie entre la Région wallonne et Cuba", Université Libre de Bruxelles, Institut de Gestion de l’Environnement et d’Aménagement du Territoire, Faculté des Sciences (Mémoire), 2009


## Problematic

The weather, in the form of storms especially, has the power to cause a lot of destruction. But if it can be predicted, a lot of problems can be easily avoided. This information can be taken advantage of in many other ways too. The problem is just that Cuba doesn’t have the adequate means to take measurements. They mainly have weather stations on the land, but because the oceans have a big influence on the weather too, these should also be studied.

### Question

Because Meteorological buoys that do this are generally very expensive, we want to develop a simple meteorological buoy that can be recreated a lot cheaper. We wonder what data are useful to collect and how design the whole thing?

### Hypothesis

Our objectives :

-The buoy has to measure wind speed and direction, air pressure and humidity, water temperature and the wave height, direction and period.

-The buoy has to be autonomous for at least a year.

-The buoy has to survive this long too.



How will we do this?

-Electronical sensors will be used to measure the properties out at sea.

-Two movable parts will collect data for the wind speed and direction.

-This data will be processed and transmitted to the shore by an arduino.

-The electronics will be powered by a battery in combination with solar panels.

-The buoy has to be waterproof to protect the electronics.

-The hull has to be strong and shock resistant.


## What measurements?

With an anemometer and a weather vane we can measure the wind speed and direction respectively. Having those measurements can indicate the orientation and the provenance of strong winds and cyclones, as well as their intensity and speed.

The air pressure and humidity will be measured directly with the barometer and humidity sensor. Meteorologists analyze the horizontal variations in atmospheric pressure to locate and track meteorological systems: this makes it possible to define areas of depressions (D) (pressure generally less than 1013 hPa, 760 mmHg), anticyclonic areas (A) (pressure generally greater than 1013 hPa, 760 mmHg) and isobars. Depressions and troughs are generally associated with bad weather. Anticyclones and barometric peaks are favorable for good weather.
Three primary measurements of humidity are widely employed: absolute, relative and specific. Absolute humidity describes the water content of air and is expressed in either grams per cubic metre or grams per kilogram. Relative humidity, expressed as a percentage, indicates a present state of absolute humidity relative to a maximum humidity given the same temperature. Specific humidity is the ratio of water vapor mass to total moist air parcel mass. While humidity itself is a climate variable, it also overpowers other climate variables. The humidity is affected by winds and by rainfall.

Also, we will measure the acceleration and rotation of the buoy with the same sensor that also measures the buoy's orientation, with this data the wave direction, height and period can be measured. Then we can elaborate a sea state, it is the description of the surface of the sea subject to the influence of the wind (which generates the wave system) and the swell.

The water temperature will be measured directly by a thermistor in the bottom hull.

In order to be able to track the buoy well it needs a GPS module that keeps track of its position.

[Wave height measurement](http://www.waveworkshop.org/11thWaves/Presentations/H2%20BenderGuinassoWalpertHowden.pdf)

[GPS site](https://learn.adafruit.com/adafruit-ultimate-gps-logger-shield/soft-serial-connect)

[GPS Data](http://www.gpsinformation.org/dale/nmea.htm)

[Tidal difference](https://upload.wikimedia.org/wikipedia/commons/5/5e/M2_tidal_constituent.jpg)


## First questions on how to do a cheap autonomous buoy

First and foremost we’ll need a strong and waterproof hull to float and house all the components. This hull is the floating body of the buoy. This body has to be held in place by strong anchoring that can withstand strong ocean forces. That leaves us with another question, where in the ocean should we put the buoy, this will be a major factor in the choice of the anchoring with the depth of the water.

![Cassandra.Younes](images/Slide-20.jpg)

One of the first questions we asked ourselves about the realization of this project was the way to achieve it, in 3d printing or in molding, we finally opted for 3d printing as it would allow us to have the choice between the two techniques. Cubans will be free to make their choice.

![Cassandra.Younes](images/Slide-111.jpg)

Waterproofing the buoy is a crucial step, if water gets infiltrated it will compromise the whole system, electronics, as well as the stability, and even drown. Waterproofing 3D prints can be made by using water resistant materials like PET but most importantly by adding perimeters to the print and making the surface thicker.

Another crucial step is adapting the hull to its new environment which is marine water. Corrosion can be avoided by using resistant materials such as PET(low medium resistance) Nylon filament (medium high resistance) or with an Epoxy coating (really high resistance) for more longevity.

![Cassandra.Younes](images/Slide-13.jpg)

The morphology of the buoy influences the stability when it’s facing the force of the water pushing it upwards but also when facing the waves for resistance and finally to use energy from the waves to eventually make the buoy autonomous. A study on the subject tends to say that the round shape seems to be the most adapted form for all these caracteristics.

![Cassandra.Younes](images/Slide-14.jpg)

The buoy has to be autonomous for long periods of time so it will need a battery that can be charged on the spot. This will be done using either solar energy or energy extracted from waves. Solar energy is easy, but it has the flaw that it isn’t as easily available in cloudy weather.

And lastly but not least, what are the components and electronics that will measure the data and what are their needs.

-"Advanced Anchoring and Mooring Study", Sound and Sea Technology engineering solutions, Oregon Wave Energy Trust, November 3 2009

-Hüseyin R. BÖRKLÜ, Erkan HELVACILAR, Veysel ÖZDEMİR, "Conceptual Design of a New Buoy", Gazi University, Technology Faculty, Industrial Design Engineering Department, Ankara, Turkey, December 15 2017

-Meghan Hendry-Brogan, "Design of a Mobile Coastal Communications Buoy", Massachusetts Institute of Technology, Department of Ocean Engineering, September 2004

-L O Wahidin et al 2018 IOP, "Design, construction, and stability test of aerial wireless coastal buoy", Conference Series: Earth and Environmental Science, 2018

[Buoyancy](https://www.mooringsystems.com/buoyancy.htm)

[Waterproofing 3D prints tips](https://www.fabbaloo.com/blog/2017/10/19/waterproofing-your-3d-prints)

[Design and analysis of buoy geometries for a wave energy converter](https://link.springer.com/article/10.1007/s40095-014-0091-7)

[Energy per meter (height?) per wave](https://www.surf-forecast.com/weather_maps/Cuba?type=wwvenergy&fbclid=IwAR3q9i1phslP31gb3EhucaUFbFxh3YoQ4ossuVaEbbWKwz5HOlxL9ZvKtp4)


## First step: printing existing prototypes

We printed a series of object such as the hull of a diy meteorological buoy to test the round shape and the waterproofing technique for 3D prints and it was a success, the buoy floats and is waterproof. It was printed with PLA so it is not corrosion resistant but it was a first step for the tests. The Hull can also be reinforced so it won’t sink with a layer of polystyrene.

![Cassandra.Younes](images/Slide-16.jpg)

The anemometer and weather vane are the two mobile components that must be in direct contact with the wind and will have to be integrated in the top part of the buoy. As these components will have to withstand the forces of nature, they have to be as strong as the hull.

![Cassandra.Younes](images/Slide-3.jpg)

For extra protection, we decided that the electronics will be placed into a waterproof box inside the buoy so that leakage doesn’t destroy them immediately. Wires will go through the box, but these holes can easily be shut and waterproofed. The test was also succesful, we put a piece of paper in the box which was underwater for several days and it was still dry.

![Cassandra.Younes](images/Slide-22.jpg)

This first step was really interesting and useful because we had the opportunity to print elements already made from the internet that are necessary for our project, that way we studied them and decided on what to keep, what to forget and what to improve.

[STL File buoy bottom](STL-Files/Buoy_095mm_bottom_hull.STL)

[STL File buoy top](STL-Files/Buoy_095mm_top_hull.STL)

[STL File waterproof box case](STL-Files/Case_w120_h80_d40.stl)

[STL File waterproof box lid](STL-Files/Lid_w120_h80_d40.stl)

[STL File anemometer cup](STL-Files/01Anem_Cup.stl)

[STL File anemometer center](STL-Files/02Anem_center.stl)

[STL File anemometer base](STL-Files/03Anem_Base.stl)

[STL File direction pointer](STL-Files/11Direc_pointer.stl)

[STL File direction base](STL-Files/12Direc_base.stl)

## Second step: our own prototypes test

For the element that indicates the wind direction, we decided to reduce its central part in order to connect it to the anemometer, at that moment we tried to make an object comprising the axis which is trapped in the interior but has space inside in order to rotate separately from the axis. This object unfortunately lacked balance because it is not fixed, so we will have to modify it.

![Cassandra.Younes](images/Slide-44.jpg)

For the anemometer we decided to simplify it and reduce its risk of breakage by removing the arms that connect the cups to the center of the object, other than that we haven't changed much except adapting a hole that will allow to connect it to the axis and the other objects.

![Cassandra.Younes](images/Slide-55.jpg)

The two elements seen require a whole operating system composed of bearings, a sensor and a piece which is complementary to it, for this we have created a box which would include all this system and which will be embedded in the top of the buoy, it is pierced from the bottom to let the cables coming from the top pass and is pierced aside for the sensor and its cables.

![Cassandra.Younes](images/Slide-6.jpg)

This is the complementary element to the sensor, its particularity is that it has several holes at its ends. Thanks to these holes and to the ratio between the perforated and non-perforated areas that will have to be programmed, we will be able to determine the wind speed. The end of this object will pass between a light sensor which, with the rotation of the axis connected to the anemometer and the encoded data, will be able to determine the wind speed by developing a light / darkness ratio.

![Cassandra.Younes](images/Slide-7.jpg)

For the top of the buoy, it must contain the upper box and therefore has a space dedicated to it, one side of the buoy is full and the other side is hollowed out to let the cables connected to the light sensor pass. The bottom of the box must be able to pass the cables connected to the compass sensor, so there is a hole in the upper part of the buoy which will also let them pass. The box can be attached to the top of the buoy.

![Cassandra.Younes](images/Slide-9.jpg)

What we tried to do was designate a space for each element and try to connect them by superimposing them, though this interlocking creates fragile and exposed areas at the junctions of the elements which must be filled and reinforced to avoid a leak.

![Cassandra.Younes](images/Slide-10.jpg)

![Cassandra.Younes](images/Slide-11.jpg)

By printing these objects it allowed us to realize what are the elements to modify, firstly the balance of the wind direction indicator, then the way in which the objects are connected. It is really enriching to be able to test prototypes, we directly realize what is wrong and we learn from our mistakes to change them and simplify the whole thing.

The next step for the design will be to finish the upper part of the buoy and maybe test it with all its electronic components, but also start the lower part even if we don't have all the components yet and its composition isn’t very complicated, we will also work on how to fit the two parts and the sealing of small faults (hole for screws, fitting, etc.). As soon as we know what solar panels to use we’ll be able to integrate them in the design too.

We also need to think about what kind of anchoring we will use as this will add up with the weight of the buoy so we’ll have to keep the anchoring in mind when choosing the buoy’s final size.

[STL File direction wind](STL-Files/directionvent.stl)

[STL File anemometer](STL-Files/Anénomètrev12.stl)

[STL File Waterpr. Box case](STL-Files/derniere-boite.stl)

[STL File Waterpr. Box lid](STL-Files/hautfin.stl)

[STL File sensor piece](STL-Files/piecev2.stl)

[STL File top buoy](STL-Files/hautbouée.stl)


## Electronics

The anemometer uses a self-made encoder to measure its rotation speed. A light sensor is used that gets a pulse rate relative to the wind speed, the relation between the pulse rate and the wind speed can be measured and this relation can be used to calibrate the output of the anemometer.
The weather vane is equipped with a compass sensor that rotates with the vane and that outputs the orientation of the arrow directly.

With the barometer we will directly measure the air pressure and with the humidity sensor we will directly measure the air humidity. A thermistor measures the water temperature.

![image en ligne](images/Humibaro.jpg)

The Adafruit accelerometer/magnetometer/gyroscope will be used to measure the buoys acceleration and its amount of unlevelness caused by the waves’ motion.
With the measured acceleration we can calculate the wave height and in combination with time we can find the wave period.
By combining the gyroscope, accelerometer and compass sensors we can measure the wave direction as the gyroscope can measure which way the buoy tilts when the accelerometer measures an ascent and this direction compared to the magnetic north by the magnetometer.

![image en ligne](images/bacceloscope.jpg)

This data will be collected by the arduino. On the arduino is an SD logging shield that might be used to store the data if it can't be transmitted and a gps shield which will find the buoy’s location. For this a good connection with the sattelites is needed.
The data will be put into decent format for transmission.

![foto](images/ArduiGPS.jpg)

Transmission will happen with the adafruit feather radio transmitter. This will likely be done by creating a mesh network of several buoys sending on the data from the farther buoys. Maybe an antenna can be used for this too.

![image en ligne](images/Transmitter.jpg)

[An example of a compass sensor that can be used for the weather vane](https://www.adafruit.com/product/1120)

[Humidity sensor](https://www.seeedstudio.com/Grove-Temperature-Humidity-Sensor-DHT11.html)

[Barometer](https://www.adafruit.com/product/1893)

[Accelerometer/barometer/gyroscope documentation](https://www.adafruit.com/product/3387)

[Thermistor datasheet](https://www.mouser.com/datasheet/2/268/20001942F-461622.pdf)

[Arduino uno](https://store.arduino.cc/arduino-uno-rev3)

[GPS Logger shield](https://starter-kit.nettigo.eu/2010/how-to-measure-temperature-with-arduino-and-mcp9700/)

[Transmitter documentation](https://learn.adafruit.com/adafruit-feather-m0-radio-with-rfm69-packet-radio)

## Electronics and Design Fusion

The anemometer works with a light sensor that will be triggered when the turning of the anemometer, this trigger has to be built onto the rotation axis as a round disc with holes.
On top of the anemometer is a separately turning weather vane with a compass sensor in it. This sensor needs to get its data through to the hull, so the axis they turn on has to be hollow.

![Cassandra.Younes](images/systemehaut.jpg)

The barometer and humidity sensor need to be in contact with outside air, but they have to stay dry. So the structure inside the buoy divides it in small rooms that can be filled with cables for the electronics and be filled.

![Cassandra.Younes](images/vuedubas.jpg)

The thermistor will be integrated into the bottom part of the hull so it is in direct contact with the water.
The solar panels will have to be integrated into the top hull.

![Cassandra.Younes](images/haut_photosh.jpg)

The GPS sensor has to be reachable by sattelites, for this an antenna might be needed.
Also, the radiotransmitter has to be able to reach the receiver.

The other sensors as well as the arduino and the battery have to be stored securely in an extra waterproof container.

![Cassandra.Younes](images/bassssss.jpg)


## The result and what still needs to be done

Electronics need to be programmed and assembled, then tested to see how well they work and how much power they need. When this is known the battery and solar panels can be chosen with the Cuban weather in mind (how much sun for how long etc.).
The radiotransmitter has to be tested on transmission distance. After the electronics have been tested some might need to be replaced by components with better properties.
A dry way to hold the barometer and humidity sensor will have to be found.

Anchoring has to be chosen in relation to the water depth and the weight and size of the buoy might have to be modified in order to keep this anchoring afloat.

The hull has to be tested on strength and properly reinforced.
