# Tutorial

## Agitation control tool

> 3D printer

| __Step 1__ | |
| --- | --- |
| Open the .stl file in the slicer of your choice (note that the slicer used during the design was the Prusa slicer). | ![Smooth piece](images/outil d'aide au contrôle/Etape_1.1.jpg) [Smooth piece](STL/jouet_agitation_texture_lisse.stl) [Wave texture](STL/jouet_agitation_texture_1.stl) [Tree texture](STL/jouet_agitation_texture_2.stl) |
| __Step 2__ | |
| Adjust the desired size. At 100%, the piece is approximately 2 cm x 2 cm. It is possible to print the pieces at 125%, 150% or even 200% depending on the desired result. | ![slicer](images/outil d'aide au contrôle/Etape_2.1.jpg) |
| __Step 3__ | |
| Configure the printing in the slicer. Durins the design of the pieces we used the Prusa slicer and a Prusa I3 MK3 printer, so you may need to adjust these settings depending on the printer you are using. | ![3.1](images/outil d'aide au contrôle/Etape_3.1.jpg) ![3.2](images/outil d'aide au contrôle/Etape_3.2.jpg) ![3.3](images/outil d'aide au contrôle/Etape_3.3.jpg) ![3.3](images/outil d'aide au contrôle/Etape_3.3.jpg) ![3.4](images/outil d'aide au contrôle/Etape_3.4.jpg) ![3.5](images/outil d'aide au contrôle/Etape_3.5.jpg) ![3.6](images/outil d'aide au contrôle/Etape_3.6.jpg) | 
| __Step 4__ | |
| Choose the filament of the desired color for the printer, we used white, red and blue PLA. You can use different colors to make your toy just like you want it. | ![4.1](images/outil d'aide au contrôle/Etape_4.1.jpg) |
| __Step 5__ | |
| Assemble your toy and use it! | ![Outil agitation](images/outil d'aide au contrôle/outil_agitation.jpg) |



## Game of Dominoes

> 3D printer

| __Step 1__ | |
| --- | --- |
| Open the .stl file in the slicer of your choice (note that the slicer used during the design was the Prusa slicer). | ![dominoes](images/Domino/Etape_1.1.jpg) [Structure dominoes 1 sur 3](STL/structure_domino_1_sur_3.stl) [Structure dominoes 2 sur 3](STL/structure_domino_2_sur_3.stl) [Structure dominoes 3 sur 3](STL/structure_domino_3_sur_3.stl) [Forme cercle](STL/forme_cercle.stl) [Forme carre](STL/forme_carre.stl) [Forme coeur](STL/forme_coeur.stl) [Forme etoile](STL/forme_etoile.stl) [Forme etoile](STL/forme_etoile.stl) [Forme lune](STL/forme_lune.stl) [Forme triangle](STL/forme_triangle.stl) |
| __Step 2__ | |
| Adjust the desired size. At 100%, the piece is approximately 2.5 cm x 5 cm. It is possible to print the pieces at 125%, 150% or even 200% depending on the desired result. | ![slicer](images/Domino/Etape_2.1.jpg) |
| __Step 3__ | |
| Configure the printing in the slicer. During the design of the different parts we used the Prusa slicer and a Prusa I3 MK3 printer, so you may need to adjust these settings depending on the printer you are using. | ![3.1](images/Domino/Etape_3.1.jpg) ![3.2](images/Domino/Etape_3.2.jpg) ![3.3](images/Domino/Etape_3.3.jpg) ![3.4](images/Domino/Etape_3.4.jpg) ![3.5](images/Domino/Etape_3.5.jpg) ![3.6](images/Domino/Etape_3.6.jpg) |
| __Step 4__ | |
| Choose the filament of the desired color for the structure of the dominoes. You can print the shapes in different colors to make the game more playful. | ![imprimante](images/Domino/imprimante_1.jpg) |
| __Step 5__ | |
| Assemble your dominoes and play! | ![dominoes](images/Domino/Dominos.jpg) |



## Braille writing rule

> 3D printer

| __Step 1__ | |
| --- | --- |
| Open the .stl file in the slicer of your choice (note that the slicer used during the design was the Prusa slicer). | ![regle braille](images/Règle braille/Etape_1.1.jpg) [Ruler braille](STL/Regle_braille.stl) |
| __Step 2__ | |
| Configure the printing in the slicer. For the design of the parts we used the Prusa slicer and a Prusa I3 MK3 printer, so you may need to adjust these settings depending on the printer you are using. | ![2.1](images/Règle braille/Etape_2.1.jpg) ![2.2](images/stylet/Etape_2.2.jpg) ![2.3](images/stylet/Etape_2.3.jpg) ![2.4](images/stylet/Etape_2.4.jpg) ![2.5](images/stylet/Etape_2.5.jpg) ![2.6](images/stylet/Etape_2.6.jpg) |
| __Step 3__ | |
| Choose the filament of the desired color for printing. | ![imprimante](images/stylet/imprimante_.jpg) |
| __Step 4__ | |
| Start writing in Braille! | ![regle braille](images/Règle braille/regle_braille.jpg) |



## Fine motor skills board

> 2-axis digital milling machine

[Board](STL/Planche-déveil-final.f3d)

| __Face A__ | |
| --- | --- |
| To mill the fine motor skills board, you first need to use a 2.2 cm wooden board. For the steps of side A here is an explanatory video: | ![Face A](/images/planche éveil/Planche_éveil_face_A.jpg) |

<iframe width="560" height="315" src="https://www.youtube.com/embed/rv9lIoGm0Xo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> |

| __Face B__ | |
| --- | --- |
| Once side A has been completed, on Fusion 360, turn the board over on side B and follow the steps in the video: | ![Face B](/images/planche éveil/Planche_éveil_face_B.jpg) |

<iframe width="560" height="315" src="https://www.youtube.com/embed/1F-f4bHoaYQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> |

| __Cutting at the CNC__ | |
| --- | --- |
| After setting the cutting on Fusion 360 (or any other 3D modeling software), it remains to export the GCode and import it into the post processor software of the milling machine. In this case, we used a Kinetic-nc milling machine. | ![CNC](images/planche éveil/CNC.jpg) |

> 3D printer (Stylus)

| Step 1 | |
| --- | --- |
| Open the .stl file in the slicer of your choice (note that the slicer used during the design was the Prusa slicer). | ![Stylus](images/stylet/stylet_planche_deveil_v3.jpg) [Stylus](STL/Stylet_planche_deveil.stl) |
| __Step 2__ | |
| Configure the printing in the slicer. For the design of the parts we used the Prusa slicer and a Prusa I3 MK3 printer, so you may need to adjust these settings depending on the printer you are using. | ![2.1](images/stylet/Etape_2.1.jpg) ![2.2](images/stylet/Etape_2.2.jpg) ![2.3](images/stylet/Etape_2.3.jpg) ![2.4](images/stylet/Etape_2.4.jpg) ![2.5](images/stylet/Etape_2.5.jpg) ![2.6](images/stylet/Etape_2.6.jpg) |
| __Step 3__ | |
| Choose the filament of the desired color for printing. | ![imprimante](images/stylet/imprimante_.jpg) |
| __Step 4__ | |
| Start practicing your fine motor skills with writing boards! | ![planche eveil](images/planche éveil/planche_eveil.jpg) |
