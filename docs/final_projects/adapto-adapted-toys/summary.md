# Adapto - Jouets adaptés

### Summary

![Adapto](images/image_synthese.jpg)

### Problématique (FR)

- 360 000 personnes atteintes de handicaps à Cuba dont 1/3 de déficience intellectuelle
- Manque de connaissances et de matériels
- Diagnostic difficile à obtenir, manque d'experts

### Question

> Comment, à l'aide de jouets adaptés et ludiques fabriqués à l’aide d’outils digitaux, peut-on améliorer l'inclusion des personnes en situation de handicaps, dans la société à Cuba?

_Hypothèse :_

À l'aide d'outils d'éveils, les personnes porteuses de handicaps peuvent acquérir et améliorer des compétences de base nécessaires à leur fonctionnement en société et ainsi se rapprocher davantage des normes préétablis. Ces dernières sont souvent très fermées et peu inclusives. En améliorant les connaissances sur les différents handicaps, leurs besoins généraux et quels outils peuvent aider, les familles seront plus outillées pour gérer ce genre de situation.

Le premier objet étudié est un outil pour calmer l'agitation. Les personnes qui ont un trouble de déficit de l'attention avec ou sans hyperactivité, sont particulièrement touchées par cet outil d'aide à la concentration.

D'autres outils ou jeux sont aussi intéressants. Par exemple, des dominos sensoriels pourraient aider les jeunes porteurs de handicaps à associer des couleurs, des textures et des formes. C'est un outil pour développer la motricité fine qui peut aussi bien être utilisé par les personnes aveugles que les personnes ayant une déficience intellectuelle.

Une dernière piste d'idée sont les jeux d'éveils. Les planches d'éveil à l'écriture et les jeux d'associations sont tous des activités de développement cérébral nécessaire à l'éducation des personnes avec des difficultés d'apprentissage.

______________________

### Problematic (EN)

- 360,000 people with disabilities in Cuba, 1/3 of them with intellectual disabilities
- Lack of knowledge and materials
- Diagnosis difficult to obtain, lack of experts

### Question

> How, with the help of adapted and playful toys made using digital tools, can we improve the inclusion of people with disabilities in society in Cuba?

_Hypothesis :_

With the help of awakening tools, people with disabilities can acquire and improve the basic skills necessary for their functioning in society and thus get closer to pre-established standards. These are often very closed and not very inclusive. By improving knowledge about different disabilities, their general needs and what tools can help, families will be better equipped to handle this kind of situation.

The first object studied is a tool to calm agitation. People with attention deficit disorder with or without hyperactivity are particularly affected by this concentration aid tool.

Other tools or games are also interesting. For example, sensory dominoes could help young people with disabilities to combine colors, textures and shapes. It is a tool for developing fine motor skills which can be used by blind people as well as people with intellectual disabilities.

A last idea track are the games of awakenings. Writing boards and games of associations are all brain development activities necessary for the education of people with learning difficulties.

__________________________

### Problemática (ES)

- 360,000 personas con discapacidad en Cuba, de las cuales, 1/3 sufre de discapacidad intelectual
- Falta de conocimiento y de materiales.
- Diagnóstico difícil de obtener, falta de expertos.

### Pregunta
> ¿Cómo, con la ayuda de juguetes adaptados y ludicos hechos con herramientas digitales, podemos mejorar la inclusión de las personas con discapacidad en la sociedad Cubana?

_ Hipótesis: _

Con la ayuda de herramientas de ayuda al despertar cognitivo, las personas con discapacidad pueden adquirir y mejorar las habilidades básicas necesarias para su funcionamiento en la sociedad y así acercarse a los estándares preestablecidos. Estos a menudo son muy cerrados y no muy inclusivos. Al mejorar el conocimiento sobre las diferentes discapacidades, sus necesidades generales y las herramientas que pueden ayudar, las familias estarán mejor equipadas para manejar este tipo de situación.

El primer objeto estudiado es una herramienta para calmar la agitación. Las personas con trastorno por déficit de atención con o sin hiperactividad se ven particularmente afectadas por esta herramienta de ayuda a la concentración.

Otras herramientas o juegos también son interesantes. Por ejemplo, el dominó sensorial podría ayudar a los jóvenes con discapacidades a combinar colores, texturas y formas. Es una herramienta para desarrollar habilidades motoras finas que pueden ser utilizadas tanto por personas ciegas como por personas con discapacidad intelectual.

Una última idea son los juegos de ayuda al despertar cognitivo. Los tableros de escritura para de ayuda al despertar cognitivo y los juegos de combinación son todas actividades de desarrollo cerebral necesarias para la educación de personas con dificultades de aprendizaje.
