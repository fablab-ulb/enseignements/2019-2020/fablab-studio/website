# EN

# Solar Panel Cooker - Tutorial

<img src="../images/CookerFinal.jpg" width="70%">

Here is the tutorial to make a solar cooker to pasteurize water and canned goods.

# Manufacturing of the solar cooker

## Materials

<html>
<table>
<tr align=top>
	<td width="25%">
	<img src="../images/Diapositive17.JPG">
     </td>
     <td>
    <b> Materials </b>
<br> For the structure:
<br> - Cardboard or wooden plate, wire mesh ...
<br> For the mirrors
<br> - Aluminum foil or cut out cans
<br> Others:
<br> - Twine
<br> - Clothespins or elastic
<br> - Brown adhesive tape
<br> - Glue
<br> Note: the glue can be made from polystyrene diluted in acetone or from flour and water.

<b> Tools </b>
<br> - Cutter
<br> - Ruler, set square, protractor
<br> - A pencil
</td>
</tr>
</table>

## Manufacturing

### Step 1

<table>
<tr align=top>
	<td width="70%">
	<img src="../images/planEN.jpg">
     </td>
     <td>
    Transfer the dimensions of each element to the cardboards according to the plan indicated (see image).
<br> Note: the green dotted lines correspond to the folds, the red dots are holes 4 mm in diameter.
     </td>
</tr>
</table>

### Step 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2355.JPG">
     </td>
     <td>
    Cut the elements using a cutter according to the lines made previously.
<br> Other process: Cutting can be done using a laser cutter.
     </td>
</tr>
</table>

### Step 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2177.JPG">
     </td>
     <td>
    Assemble the elements with brown tape as in the photo.
 <br> Note: apply the adhesive tape to both sides of the cooker.
     </td>
</tr>
</table>

### Step 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2182.JPG">
     </td>
     <td>
   Glue the aluminum foil to the front of the cardboard.
<br> Another process: cut cans so as to have flat plates that can then be drilled and hung on the structure using wire.
     </td>
</tr>
</table>

### Step 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2390.JPG">
     </td>
     <td>
   Attach the counterweights to the back of the back panel using tape and string as shown in the photo.
     </td>
</tr>
</table>

### Step 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2185.JPG">
     </td>
     <td>
    In this step we will create a string to orient the front panel at a particular angle for each season.
    <br> A) Cut at least 1m of string
    <br> B) Make a first knot at the end of the string then place the other knots according to the following distances (measured from the first knot):
    <br> - 1 knot at 80.4 cm for the summer (Orientation of the front panel = 45 °)
    <br> - 1 knot at 88.3 cm for spring and autumn (Orientation of the front panel = 30 °)
    <br> - 1 knot 96.8 cm for winter (Orientation of the front panel = 10 °)
    <br> C) Insert end of wire in the knots
    <br> D) Tie the string by inserting it on the side of the first knot in the hole of the front panel, make a second knot so as to have a knot on each side of the cardboard.
     </td>
</tr>
</table>

### Step 7

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2377.JPG">
     </td>
     <td>
    In this step we will proceed with the assembly:
      <br> A) Gather the back and side panels and fasten them with clothespins or rubber bands
      <br> B) Assemble the counterweights as in the photo then place a weight on it (jar filled with water)
     <br> C) Hang the string in the gap at the top of the rear panel by choosing the correct orientation indicated by each knot (see Step 6)
     </td>
</tr>
</table>

# Manufacturing of the temperature indicator

This solar cooker is designed to pasteurize 4 cans at a time. It is therefore necessary to make 4 pasteurization containers. To know when the water is boiling, we will make a temperature indicator that will be inserted in the lid of one of the containers. In the end we will have: 1 container with the temperature indicator and 3 containers without indicator.

## Materials

<html>
<table>
<tr align=top>
	<td width="25%">
   <B> Materials </b>
     <br> - 2 Cans
     <br> - 1 transparent ballpoint pen
     <br> - Sealant
     <br> - Colorant (curry, mercurochrome ...)
     <br> - Water
<br> <b> Tools </b>
<br> - Cutter
<br> - Scissors
<br> - Marker
<br> - Ruler
</td>
</tr>
</table>

## Manufacturing

### Step 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2299.JPG">
     <td>
 - Trace the markings for cutting the cans, we want to recover the bottom of the cans:
   <br> - Make a marking at 1.5 cm and another at 1 cm from the edge
     </td>
</tr>
</table>

### Step 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2305.JPG">
     <td>
   <br> -Cut the cans according to the marking.
    <br> - Pierce the 1 cm piece of the can according to the diameter of the pen (8 mm)
     </td>
</tr>
</table>

### Step 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/curry.jpg">
     <td>
 Pour a pinch of colorant (curry) into the undrilled dish.
     </td>
</tr>
</table>

### Step 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/eau.jpg">
     <td>
   - Embed the holey cup in the other cup so that their cut edges are at the same level.
<br> - Pour water through the hole to the brim. Shake everything.
     </td>
</tr>
</table>

### Step 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2249.JPG">
     </td>
     <td>
- Completely disassemble the transparent billle pen (plug at the end included) so as to obtain a sort of straw. If the pen has its wall pierced, plug the hole with a strip of adhesive tape that is not too wide.
   <br> - Measure the external diameter of the ballpoint pen, this measurement will be used to pierce the lid of the container.
     </td>
</tr>
</table>

### Step 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/styloK.jpg">
     <td>
   - Push the pen into the hole
    <br> - Apply sealant to the base of the pen
     </td>
</tr>
</table>

### Step 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2321.JPG">
     <td>
  Test the indicator by placing it in a pot of boiling water. When the water is boiling indicate the level of the colored water by drawing a mark on the pen.
     </td>
</tr>
</table>

# Manufacture of pasteurization containers (pots)

It is necessary to make 1 container with the temperature indicator and 3 containers without indicator. Skip steps 4 and 5 for containers without indicator.

## Materials

<html>
<table>
<tr align=top>
	<td width="25%">
	<img src="../images/marmite.jpg">
     </td>
     <td>
<b> Materials </b>
     <br> For the container:
     <br> - Large cans of 13cm in diameter and 15cm in height (here: infant milk can)
     <br> - Black paint
     <br> For the cover:
     <br> - Large cans identical to the containers.
     <br> - Wire
     <br> - Fabric
<br> <b> Tools </b>
<br> - Cutter
<br> - Scissors
<br> - Hacksaw
<br> - Marker
<br> - Ruler
</td>
</tr>
</table>

## Manufacturing

### Step 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2232.JPG">
     </td>
     <td>
    Paint the tin can black.
     </td>
</tr>
</table>

### Step 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2325.JPG">
     </td>
     <td>
   -Cut the bottom of a second tin can, the same diameter as the previous one, to make the lid.
    <br> - Drill two diametrically opposite holes.
     </td>
</tr>
</table>

### Step 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2326.JPG">
     </td>
     <td>
    Hang fabric on the circumference so as to cover the cutting part and allow better regularity.
     </td>
</tr>
</table>

Steps 4 and 5 concern only one pasteurization container. For the others go directly to step 6.

### Step 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2334.JPG">
     </td>
     <td>
   Drill an 8mm hole (corresponding to the diameter of the pen) in the middle of the cover.
     </td>
</tr>
</table>

### Step 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2341.JPG">
     <td>
   <br> - Insert the indicator in the cover.
     </td>
</tr>
</table>

### Step 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2344.JPG">
     <td>
   <br> Drill a hole at the top of the can and hang the cover with wire.
     </td>
</tr>
</table>

### Step 7

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2347.JPG">
     <td>
   <br>Hang a string in the ring on the front of the cover that holds the fabric.
     </td>
</tr>
</table>

### Step 8

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2352.JPG">
     <td>
   <br>Wrap the jar to be pasteurized with onion, potato or citrus fillet so that you can remove it without burning yourself at the end of pasteurization at 100 ° C.
     </td>
</tr>
</table>

# Manufacture of the protective bell

## Materials

<html>
<table>
<tr align=top>
<td width="35%">
	<img src="../images/IMG_2231.JPG">
</td>
	<td>
   <b> Materials </b>
     - 2 bottles of 5L water (transparent PET)
     - 1 small jar (+/- 6cm high) or a glass ashtray
<br> <b> Tools </b>
<br> - Cutter
</td>
</tr>
</table>

## Manufacturing

### Step 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2379.JPG">
     <td>
  - Cut two 5L bottles of water (PET) to form an openable and resealable bottle
    <br> - Place a small glass jar at the bottom of the bottle.
     </td>
</tr>
</table>

# Assembly

<img src="../images/mix.jpg">

# Use

## Installation of the solar cooker

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2377.JPG">
     </td>
     <td>
      <br> A) Gather the back and side panels and fasten them with clothespins or rubber bands
      <br> B) Assemble the counterweights as in the photo then place a weight on it (jar filled with water)
     <br> C) Hang the string in the gap at the top of the rear panel by choosing the correct orientation indicated by each knot (see Step 6):
     <br> - In summer: 45 ° -> 1st knot (the other knots hang at the back)
     <br> - In spring and autumn: 30 ° -> 2nd knot
     <br> - In winter: 10 ° -> 3rd knot
     <br> Note: we could color each node in a different color to better identify them and indicate a legend on the back of the cooker.
     <br> D) Install the solar cooker on a flat (leveled) surface in a place that will remain sunny for at least 4 hours.
     </td>
</tr>
</table>

At 08:00 am

## Preparation of preserves

You can use "twist off" jars recovered from canned glass sold commercially. You just have to make sure that the lid has a bump that will sink at the end of pasteurization when the jars are going to cool. They must be in perfect condition: no deformation of the cover, no chips, no rust ...

<img src="../images/aee8eb8c84ee299f054d6a9a99f858a7.jpg" width="25%">

[Les couvercles twist-off](https://www.ebottles.eu/actu-conseils/les-pots-twist-off-pasteurisation-sterilisation-xpa2453.html)

### Step 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/1.png">
     <td>
   Wash the jars with soapy water (washing up liquid).
     </td>
</tr>
</table>

### Step 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/3.png">
     <td>
   Prepare food according to the recipe.
     </td>
</tr>
</table>

### Step 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/4.png">
     <td>
   Fill the jars with the preparation, leaving a space of 1 to 2cm. Make sure the food is covered with water. Close the jars properly.
     </td>
</tr>
</table>

## Pasteurization of canned food

At 10:00 am

### Step 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/filet.jpg">
     <td>
  Put the jar in an onion net (potatoes or citrus)
     </td>
</tr>
</table>

### Step 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2352.JPG">
     <td>
  Put the jar in the container and fill with water. The water should cover the jar 2 to 3cm.
     </td>
</tr>
</table>

### Step 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2381.JPG">
     <td>
Close the container and insert it into the protective envelope
     </td>
</tr>
</table>

### Step 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2386.JPG">
     <td>
  Place the pasteurization containers in the solar cooker, aligned with the edge of the front panel.
     </td>
</tr>
</table>

### Step 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/ombre.jpg">
     <td>
Orient the solar cooker facing the sun -> the shadow of the containers or the cooker must be at the rear and must not be oblique but perpendicular to the hinge of the front panel.
     </td>
</tr>
</table>

### Step 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/pngtree-vector-clock-icon-png-image_927210.jpg">
     <td>
Expose the device to the sun for 3 hours from 10:00 am to 01:00 pm (or from 11:00 am to 02:00 pm). You do not need to be present during this time.
     </td>
</tr>
</table>

A 01:00 pm

### Step 7

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/thermo.png">
     <td>
After 3 hours, if the colored water rises in the temperature indicator at the level of the line, it means that the water is boiling. The level of the indicator may fluctuate, this also means that the water is boiling. Otherwise wait a few minutes until this happens. From this moment pasteurization begins.   
     </td>
</tr>
</table>

### Step 8

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/hourglass-icon-vector-illustration.jpg">
     <td>
Leave the preserves in boiling water for the time indicated by the recipe. Keep the device in the sun, you can reposition the cooker.
     </td>
</tr>
</table>

A 02:00 pm

### Step 9

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/capsule.JPG">
     <td>
When the pasteurization time has elapsed you can remove the preserves.

Caution: Do not touch the metal container and do not immerse your fingers in boiling water! Lift the cover with the string, then take out the canning jar, catching it by the net.

Wait until the jars cool down, the bump on the "twist off" lid should be pressed in, if this is not the case it indicates that the can is failed (see diagram).

Store successful preserves in a cool place where the temperature is below 14 ° C (cellar or refrigerator). Canned acidic foods can be stored at room temperature.
<br> It's best to consume the canned food within 12 months.

Attention: When opening if air escapes, the food has taken a suspicious color or a foul odor is released do not consume the can! If the bump of the twist off is also raised.
     </td>
</tr>
</table>

Source : [Ecoconso - Les conserves](https://www.ecoconso.be/fr/content/conservation-comment-steriliser-pasteuriser-les-fruits-et-legumes)



-----------------
# FR

# Cuiseur solaire à panneaux - Tutoriel

Voici le tutoriel pour fabriquer un cuiseur solaire permettant de pasteuriser l'eau et les conserves.

# Fabrication du cuiseur solaire

## Matériaux

<html>
<table>
<tr align=top>
	<td width="25%">
	<img src="../images/Diapositive17.JPG">
     </td>
     <td>
    <p><b>Matériaux</b>
    <br> Pour la structure :
    <br> - Carton ou plaque de bois, grillage...
    <br>Pour les miroirs
<br> - Papier aluminium ou canettes découpées
<br>Autres :
<br>- Ficelle
<br>- Pinces à linge ou élastiques
<br>- Ruban adhesif marron
<br>- Colle
<br>Remarque : la colle peut être faite à base de polystyrène dilué dans de l'acétone ou à base de farine et d'eau.

<b>Outils</b>
<br> - Cutter
<br>- Régle, équerre, rapporteur
<br>- Un crayon à papier
</td>
</tr>
</table>

## Fabrication

### Etape 1

<table>
<tr align=top>
	<td width="70%">
	<img src="../images/PLANOK.jpg">
     </td>
     <td>
    Reporter les dimensions de chaque élément sur les cartons selon le plan indiqué (voir image).
    <br>Remarque : les pointillés vert correspondent aux plis, les points rouges sont des trous de 4 mm de diamètre.
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2355.JPG">
     </td>
     <td>
    Découper les éléments à l'aide d'un cutter selon les tracés effectués précédement.
    <br> Autre procédé : La découpe peut s'effectuer grâce à une découpeuse laser.
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2177.JPG">
     </td>
     <td>
    Assembler les éléments avec du ruban adhésif marron comme sur la photo.
    <br>Remarque : appliquer le ruban adhésif sur les deux faces du cuiseur.
     </td>
</tr>
</table>

### Etape 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2182.JPG">
     </td>
     <td>
    Coller le papier aluminium sur la face avant du carton.
    <br>Autre procédé : découper des canettes de façon à avoir des plaques plates que l'on peut ensuite percer et accrocher sur la structure à l'aide de fil de fer.
     </td>
</tr>
</table>

### Etape 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2390.JPG">
     </td>
     <td>
   Fixer les contrepoids à l'arrière du panneau arrière à l'aide de ruban adhésif et de ficelle comme sur la photo.
     </td>
</tr>
</table>

### Etape 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2185.JPG">
     </td>
     <td>
     Dans cette étape on va créer une ficelle pour orienter le panneau avant selon un angle particulier pour chaque saison.
   <br>A) Découper au moins 1m de ficelle
   <br>B) Faite un premier noeud à l'extrémité de la ficelle puis placer les autres noeuds selon les distances suivante (mesurées depuis le premier noeud) :
   <br> - 1 noeud à 80,4 cm pour l'été (Orientation du panneau avant = 45°)
   <br>- 1 noeud à 88,3 cm pour le printemps et l'automne (Orientation du panneau avant = 30°)
   <br>- 1 noeud à 96,8 cm pour l'hiver (Orientation du panneau avant = 10°)
   <br>C) Insérer des bouts de fil de fer dans les noeuds
   <br>D) Attacher la ficelle en l'insérant du coté du premier noeud dans le trou du panneau avant, faite un second noeud de manière à avoir un noeud de chaque coté du carton.
     </td>
</tr>
</table>

### Etape 7

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2377.JPG">
     </td>
     <td>
     Dans cette étape on va procéder au montage :
     <br>A) Rassembler les panneaux arrière et latéraux et les attacher avec des pinces à linges ou des élastiques
     <br>B) Assembler les contrepoids comme sur la photo puis placer un poids dessus (bocal rempli d'eau)
    <br>C) Accrocher la ficelle dans l'interstice en haut du panneau arrière en choisissant la bonne orientation indiquée par chaque noeud (voir Etape 6)
     </td>
</tr>
</table>

# Fabrication de l'indicateur de température

Ce cuiseur solaire est conçu pour pasteuriser 4 conserves à la fois. Il faut donc fabriquer 4 récipients de pasteurisation. Pour savoir quand l'eau boue on va fabriquer un indicateur de température que l'on va insérer dans le couvercle d'un des récipients. Au final on aura : 1 récipient avec l'indicateur de température et 3 récipients sans indicateur.

## Matériaux

<html>
<table>
<tr align=top>
	<td width="25%">
   <b>Matériaux</b>
    <br> - 2 Canettes
    <br> - 1 stylo bille transparent
    <br> - Mastic d'étanchéité
    <br> - Colorant (curry, mercurochrome...)
    <br> - Eau
<br><b>Outils</b>
<br> - Cutter
<br> - Ciseaux
<br>- Marqueur
<br> - Règle
</td>
</tr>
</table>

## Fabrication

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2299.JPG">
     <td>
   Tracer les marquages pour la découpe des canettes, on souahaite récupéer le fond des canettes :
   <br>- Faite un marquage à 1,5 cm et un autre à 1 cm du bord
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2305.JPG">
     <td>
   <br>-Découper les canettes selon le marquage.
   <br>- Percer le morceau de canette de 1 cm selon le diamètre (externe) du stylo (8 mm)
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/curry.jpg">
     <td>
   Verser une pincée de colorant (curry) dans la coupelle non trouée.
     </td>
</tr>
</table>

### Etape 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/eau.jpg">
     <td>
   <br>- Encastrer la coupelle trouée dans l'autre coupelle de façon à ce que leurs bord tranchés soient au même niveau.
   <br>- Verser de l'eau par le trou jusqu'au raz bord. Agiter le tout.
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2249.JPG">
     </td>
     <td>
- Démonter entièrement le stylo billle transparent (bouchon à l'extrémité inclus) de manière à obtenir une sorte de paille. Si le stylo a sa paroie percée, boucher le trou avec une bande de ruban adhésif pas trop large.
   - Mesurer le diamtère externe du stylo bille, cette mesure servira pour percer le couvercle du récipient.
     </td>
</tr>
</table>

### Etape 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/styloK.jpg">
     <td>
   <br>- Enfoncer le stylo dans le trou
   <br>- Appliquer du mastic d'étanchéité à la base du stylo
     </td>
</tr>
</table>

### Etape 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2321.JPG">
     <td>
   <br> Tester l'indicateur en le plaçant dans une casserole d'eau bouillante. Quand l'eau boue indiquer le niveau de l'eau colorée en traçant une marque sur le stylo.
     </td>
</tr>
</table>

# Fabrication des récipients de pasteurisation (marmites)

Il faut fabriquer 1 récipient avec l'indicateur de température et 3 récipients sans indicateur. Sautez les étapes 4 et 5 pour les récipients sans indicateur.

## Matériaux

<html>
<table>
<tr align=top>
	<td width="25%">
	<img src="../images/marmite.jpg">
     </td>
     <td>
    <p><b>Matériaux</b>
    <br> Pour le récipient :
    <br> - Grandes boites de conserves de 13cm de diamètre et 15 cm de haut (ici : boites de lait infantile)
    <br>- Peinture noire
    <br> Pour le couvercle :
    <br>- Grandes boites de conserves identiques au récipients.
    <br>- Fil de fer
    <br>- Tissu
<br><b>Outils</b>
<br> - Cutter
<br> - Ciseaux
<br>- Scie à métaux
<br>- Marqueur
<br> - Régle (ruler)
</td>
</tr>
</table>

## Fabrication

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2232.JPG">
     </td>
     <td>
    Peindre la boite de conserve en noir
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2325.JPG">
     </td>
     <td>
    -Découper le bas d'une seconde boite de conserve, de même diamètre que la précédente, pour réaliser le couvercle.
   <br>- Percer deux trous diamétralement opposés.
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2326.JPG">
     </td>
     <td>
    Accrocher du tissu sur la circonférence de manière à couvrir la partie tranchante et permettre une meilleure régularité.
     </td>
</tr>
</table>

Les étapes 4 et 5 ne concernent qu'un seul récipient de pasteurisation. Pour les autres passez directement à l'étape 6.

### Etape 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2334.JPG">
     </td>
     <td>
   Percer un trou de 8mm (correspondant au diamètre du stylo) au milieu du couvercle
     </td>
</tr>
</table>

### Etape 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2341.JPG">
     <td>
   <br> - Insérer l'indicateur dans le couvercle
     </td>
</tr>
</table>

### Etape 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2344.JPG">
     <td>
   <br> Percer un trou en haut de la boite de conserve et accrocher le couvercle avec du fil de fer.
     </td>
</tr>
</table>

### Etape 7

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2347.JPG">
     <td>
   <br>Accrocher une ficelle dans l'anneau à l'avant du couvercle qui retient le tissu
     </td>
</tr>
</table>

### Etape 8

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2352.JPG">
     <td>
   <br>Envelopper le bocal à pasteuriser avec du filet à oignons, patates ou agrumes pour pouvoir le retirer sans vous brûler à la fin de la pasteurisation à 100°C.
     </td>
</tr>
</table>

# Fabrication de la cloche protectrice

## Matériaux

<html>
<table>
<tr align=top>
<td width="35%">
	<img src="../images/IMG_2231.JPG">
</td>
	<td>
   <b>Matériaux</b>
    - 2 bouteilles d'eau de 5L (PET transparent)
    - 1 petit bocal (+/-6cm de haut) ou un cendrier en verre
<br><b>Outils</b>
<br> - Cutter
</td>
</tr>
</table>

## Fabrication

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2379.JPG">
     <td>
   <br>- Découper deux bouteilles de 5L d'eau (PET) pour former une bouteille ouvrable et refermable par emboitement
   <br>- Placer un petit bocal en verre au fond de la bouteille.
     </td>
</tr>
</table>

# Assemblage

<img src="../images/mix.jpg">

# Utilisation

## Montage du cuiseur solaire

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2377.JPG">
     </td>
     <td>
     A) Rassembler les panneaux arrière et latéraux et les attacher avec des pinces à linges ou des élastiques
     <br>B) Assembler les contrepoids comme sur la photo puis placer un poids dessus (bocal rempli d'eau)
    <br>C) Accrocher la ficelle dans l'interstice en haut du panneau arrière en choisissant la bonne orientation indiquée par chaque noeud (voir Etape 6) :
    <br>- En été : 45° -> 1er noeud (les autres noeuds pendent à l'arrière)
    <br>- Au printemps et en automne : 30° -> 2éme noeud
    <br>- En hiver : 10° -> 3éme noeud
    <br>Remarque : on pourrait colorer chaque noeud dans une couleur distincte pour mieux les identifier et indiquer une légende au dos du cuiseur.
    <br>D) Installer le cuiseur solaire sur une surface plane (mis à niveau) dans un endroit qui va rester ensoleillé pendant au moins  4h
     </td>
</tr>
</table>

A 8h

## Préparation des conserves

On peut utiliser des bocaux "twist off" récupérés des conserves en verre vendues dans le commerce.  Il faut juste s'assurer que le couvercle posséde une bosse qui va s'enfoncer à la fin de la pasteurisation quand les bocaux vont refroidir. Ils doivent être en parfait état : aucune déformation du couverle, aucunes ébréchures, pas de rouille...

<img src="../images/aee8eb8c84ee299f054d6a9a99f858a7.jpg" width="25%">

[Les couvercles twist-off](https://www.ebottles.eu/actu-conseils/les-pots-twist-off-pasteurisation-sterilisation-xpa2453.html)

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/1.png">
     <td>
   Laver les bocaux avec de l'eau savoneuse (liquide vaiselle).
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/3.png">
     <td>
   Préparer les aliments selon la recette.
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/4.png">
     <td>
   Remplir les bocaux avec la préparation en laissant un espace de 1 à 2cm. Veiller à ce que les aliments soient recouverts d'eau. Refermer les bocaux correctement.
     </td>
</tr>
</table>

## Pasteurisation des conserves

A 10h

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/filet.jpg">
     <td>
  Mettre le bocal dans un filet à oignons (patates ou agrumes)
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2352.JPG">
     <td>
  Mettre le bocal dans le récipient et remplir d'eau. L'eau doit recouvrir le bocal de 2 à 3cm.
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2381.JPG">
     <td>
  Refermer le récipient et l'inserer dans l'enveloppe protectrice
     </td>
</tr>
</table>

### Etape 4

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/IMG_2386.JPG">
     <td>
  Placer les récipients de pasteurisation dans le cuiseur solaire, alignés au bord du panneau avant.
     </td>
</tr>
</table>

### Etape 5

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/ombre.jpg">
     <td>
 Orienter le cuiseur solaire face au soleil -> l'ombre des récipients ou du cuiseur doit être à l'arrière et ne doit pas être oblique mais perpendiculaire à la charnière du panneau avant.
     </td>
</tr>
</table>

### Etape 6

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/pngtree-vector-clock-icon-png-image_927210.jpg">
     <td>
Exposer le dispositif au soleil pendant 3h de 10h à 13h (ou de 11h à 14h). Vous n'avez pas besoin d'être présent pendant ce temps.
     </td>
</tr>
</table>

A 13h

### Etape 7

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/thermo.png">
     <td>
Après 3h, si l'eau colorée remonte dans l'indicateur de température au niveau du trait cela signifie que l'eau boue. Il se peut que le niveau de l'indicateur fluctue, cela signifie également que l'eau boue. Sinon attendez quelques minutes jusqu'à que ça se produise. A partir de ce moment la pasteurisation commence.   
     </td>
</tr>
</table>

### Etape 8

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/hourglass-icon-vector-illustration.jpg">
     <td>
Laisser les conserves dans l'eau bouillante pendant le temps indiqué par la recette. Maintenez le dispositif au soleil, vous pouvez repositionner le cuiseur.
     </td>
</tr>
</table>

A 14h

### Etape 9

<table>
<tr align=top>
	<td width="35%">
	<img src="../images/capsule.JPG">
     <td>
Quand le temps de pasteurisation est écoulé vous pouvez retirer les conserves.

Attention : Ne touchez pas le récipient métalique et ne plongez pas vos doigts dans l'eau bouillante ! Soulevez le couvercle avec la ficelle, puis sortez le bocal de conserve en l'attrappant par le filet.

Attendez que les bocaux refroidissent, la bosse du couvercle "twist off" devrait être enfoncée, si ce n'est pas le cas cela indique que la conserve est ratée (voir schéma).

Stocker les conserves réussies dans un endroit frais où la température est inférieure à 14°C (cave ou réfrigérateur). Les conserves d'aliments acides peuvent être stockées à température ambiante.
<br>Il est préférable de consommer les conserves dans les 12 mois.

Attention : A l'ouverture si de l'air s'échappe, que les aliments ont pris une couleur suspecte ou qu'une odeur nauséabonde se dégage ne consommez pas la conserve ! Si la bosse du twist off est remontée également.
     </td>
</tr>
</table>

Source : [Ecoconso - Les conserves](https://www.ecoconso.be/fr/content/conservation-comment-steriliser-pasteuriser-les-fruits-et-legumes)
