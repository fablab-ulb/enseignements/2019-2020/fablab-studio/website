# Solar oven :

## Summary

<img src="../images/CookerFinal.jpg" width="70%">

<b>Problematic (FR):</b>

- Non-drinkable water
- Energy consumption from fossil sources: to make drinking water Cubans boil water by consuming fossil energy sources (gas, electricity from petroleum, etc.).
- Pollution: fossil fuels emit greenhouse gases (CO2) harmful to the environment.
- Costly energy
- Shortage: These sources of energy come from other countries, except Cuba undergoes an embargo which weighs on imports which sometimes causes shortages. Alternatives must therefore be found.
- Solar energy: to remedy these problems Cubans could use solar energy to pasteurize water and even food. This energy is clean, free and available in abundance in Cuba, which enjoys strong sunshine all year round.

<b>Question:</b>

How to pasteurize water and food, through the manufacture of canned food, using a device powered by solar energy and which can be easily manufactured, at low cost, with the materials available in Cuba?

<b>Hypothesis:</b>

The solution is a solar oven. It is a device which makes it possible to cook food thanks to the heat of the rays of the sun captured by means of mirrors.
There are several types of solar ovens: box solar ovens use flat mirrors that reflect light to an insulated glass box that generates a greenhouse effect, dishes and tables use mirrors that focus the sun's rays at a high point temperature (+/- 250 ° C), the parabolic cylinders (or tubes) combine concentration and the greenhouse effect; solar panel ovens are simple reflective mirrors.

As canned food takes up more space than conventional dishes, a box-type oven seemed to be the right option because it allows larger dishes to be cooked than concentrated solar ovens, but their construction requires a lot of materials, which are often complicated to obtain. in Cuba (glass, wood, insulation ...). The solution chosen would be a solar panel oven because its design is simpler.
Indeed, these are simple reflective panels (mirrors) which return the sun's rays to a container which absorbs heat (black and metallic).
The temperature of this type of oven can reach 120 ° C which is largely sufficient because the water is pasteurized at 70 ° C (in 15 min) and the preserves at 100 ° C.

Ideally this oven should be light, foldable and steerable. The structure of the oven could be made with various materials (cardboard, wood, wire mesh, etc.), mirrors with aluminum foil or cut out cans plated on the structure.
If it is made of cardboard, the cutting could be carried out with the laser cutter. If it is made of wood it could be cut with the CNC.

______________________

## Résumé

<b>Problématique (EN) : </b>

- Eau non potable
- Consommation d’énergie de sources fossiles : pour rendre l’eau potable les cubains font bouillir l’eau en consommant des sources d’énergies fossiles (gaz, électricité issue du pétrole…).
- Pollution : les énergies fossiles émettent des gaz à effet de serre (CO2) néfastes pour l’environnement.
- Energie couteuse
- Pénurie : Ces sources d’énergie proviennent d’autres pays, hors Cuba subit un embargo qui pèse sur les importations ce qui provoque parfois des pénuries. Il faut alors trouver des alternatives.
- Energie solaire : pour remédier à ces problèmes les cubains pourraient utiliser l’énergie solaire pour pasteuriser l’eau et même les aliments. Cette énergie est propre, gratuite et disponible en abondance à Cuba qui jouit d’un fort ensoleillement toute l’année.

<b> Question :</b>

Comment pasteuriser l’eau et les aliments, à travers la fabrication de conserves, grâce à un dispositif fonctionnant à l’énergie solaire et pouvant être fabriqué facilement, à moindre coût, avec les matériaux disponibles à Cuba ?

<b>Hypothèse:</b>

La solution est un four solaire. C'est un dispositif qui permet de cuire des aliments grâce à la chaleur des rayons du soleil captés au moyen de miroirs.
Il existe plusieurs types de fours solaires : les fours solaires boîtes utilisent des miroirs plats qui réfléchissent la lumière vers une boite vitrée isolée qui génère un effet de serre, les paraboliques et tables utilisent des miroirs qui concentrent les rayons du soleil en un point à haute température (+/-250°C), les cylindro-paraboliques (ou tubes) combinent la concentration et l'effet de serre; les fours solaires à panneaux sont de simples miroirs réflecteurs.

Comme les conserves occupent plus d’espace que les plats classiques un four de type boîte semblait être la bonne option car il permet de faire cuire des plats plus volumineux que les fours solaires à concentration mais leur construction nécessite beaucoup de matériaux souvent compliqués à se procurer à Cuba (vitre, bois, isolants…). La solution choisie serait un four solaire à panneaux car sa conception est plus simple.
En effet il s’agit de simples panneaux réflecteurs (miroirs) qui renvoient les rayons du soleil vers un récipient qui absorbe la chaleur (noir et métallique).
La température de ce type de fours peut atteindre 120°C ce qui est largement suffisant car l’eau est pasteurisée à 70°C (en 15 min) et les conserves à 100°C.

Dans l’idéal ce four doit être léger, pliable et orientable. La structure du four pourrait être fabriquée avec divers matériaux (carton, bois, grillage métallique…), les miroirs avec du papier aluminium ou des canettes découpées plaquées sur la structure.
S’il est fabriqué en carton la découpe pourrait être réalisée avec la découpeuse laser. S’il est en bois il pourrait être découpé avec la CNC.

______________________

## Resumen

<b>Problemática (ES):</b>

- Agua no potable
- Consumo de energía de fuentes fósiles: para hacer que el agua potable cubanos hierva el agua consumiendo fuentes de energía fósil (gas, electricidad del petróleo, etc.).
- Contaminación: los combustibles fósiles emiten gases de efecto invernadero (CO2) perjudiciales para el medio ambiente.
- Energía costosa
- Escasez: estas fuentes de energía provienen de otros países, excepto que Cuba sufre un embargo que pesa sobre las importaciones que a veces causa escasez. Por lo tanto, se deben encontrar alternativas.
- Energía solar: para remediar estos problemas, los cubanos podrían utilizar la energía solar para pasteurizar el agua e incluso los alimentos. Esta energía es limpia, gratuita y está disponible en abundancia en Cuba, que goza de un sol radiante durante todo el año.

<b>Pregunta :</b>

¿Cómo pasteurizar el agua y los alimentos, mediante la fabricación de alimentos enlatados, utilizando un dispositivo alimentado por energía solar y que se pueda fabricar fácilmente, a bajo costo, con los materiales disponibles en Cuba?

<b>Hypothèse: </b>

La solución es un horno solar. Es un dispositivo que permite cocinar alimentos gracias al calor de los rayos del sol capturados mediante espejos.
Hay varios tipos de hornos solares: los hornos solares de caja usan espejos planos que reflejan la luz a una caja de vidrio aislada que genera un efecto invernadero, los platos y las mesas usan espejos que enfocan los rayos del sol en un punto alto temperatura (+/- 250 ° C), los cilindros parabólicos (o tubos) combinan concentración y efecto invernadero; Los hornos de paneles solares son simples espejos reflectantes.

Como la comida enlatada ocupa más espacio que los platos convencionales, un horno tipo caja parecía ser la opción correcta porque permite cocinar platos más grandes que los hornos solares concentrados, pero su construcción requiere muchos materiales, que a menudo son difíciles de obtener. en Cuba (vidrio, madera, aislamiento ...). La solución elegida sería un horno de paneles solares porque su diseño es más simple.
De hecho, estos son simples paneles reflectantes (espejos) que devuelven los rayos del sol a un recipiente que absorbe el calor (negro y metálico).
La temperatura de este tipo de horno puede alcanzar los 120 ° C, que es en gran medida suficiente, ya que el agua se pasteuriza a 70 ° C (en 15 min) y las conservas a 100 ° C.

Idealmente, este horno debe ser ligero, plegable y orientable. La estructura del horno podría estar hecha con varios materiales (cartón, madera, malla de alambre, etc.), espejos con papel de aluminio o latas cortadas chapadas en la estructura.
Si está hecho de cartón, el corte podría realizarse con la cortadora láser. Si está hecho de madera, podría cortarse con el CNC.
