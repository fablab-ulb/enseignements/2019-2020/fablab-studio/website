# Cuiseur solaire - Processus de recherche

<img src="../images/CookerFinal.jpg" width="70%">

# Phase 1 - Définition de la problématique

## Problématique

L'eau n'est pas potable à Cuba. Pour la rendre potable les cubains la font bouillir avec de l'énergie de sources fossile (gaz, éléctricité issue du pétrole...). Ce type d'énergie est poluante (émission de CO2), coûteuse et sujet à pénurie à cause de l'embargo que subit Cuba. En effet Cuba dépend des autres pays pour se fournir en sources d'énergie, l'embargo bloque les importations ce qui cause des pénuries d'énergie. Il faudrait trouver une source d'énergie alternative.
<br>Pour remédier à ces problèmes les cubains pourraient utiliser l’énergie solaire pour pasteuriser l’eau et même les aliments. Cette énergie est propre, gratuite et disponible en abondance à Cuba qui jouit d’un fort ensoleillement toute l’année.

## Workshop

<img src="../images/Diapositive2.JPG" width="50%">

C'est à partir de cette problématique que pendant la semaine projet avec Ernersto Oroza nous avons cherché un moyen de potabiliser l'eau et avec mon groupe on a décidé de pasteuriser l'eau grâce à l'énergie solaire. Nous avons fabriqué un prototype de bouilloire solaire et le résultat de nos expérimenations s'est révélé prometeur. C'est pourquoi j'ai décidé de continuer sur cette voie.

## Question

Pour pousser le potentiel du système un peu plus loin l'idée serait non seulement de pasteuriser l'eau mais aussi les aliments à travers la fabrication de conserves. Les conserves permettent de préserver les aliments pendant plusieurs mois. Dans le contexte de Cuba ce mode de conservation permet de faire des économies d'énergie et financière par rapport à la congélation ou la réfrigération, mais elle permet aussi de constituer des stocks de nourriture en cas de pénurie dû à l'embargo. De plus faire ses conserves soi-même revient moins cher que de les acheter. Cependant faire des conserves nécessite des cuissons longues qui consomment de l'énergie fossile. Comme pour la pasteurisation de l'eau, l'idée est de pouvoir faire des conserves sans rien dépenser avec de l'énergie renouvelable, propre, qui n'est pas sujet aux pénuries.
<br>Mon objectif est de créer un dispositif qui permet de pasteuriser l'eau et les aliments grâce à l'énergie solaire. Ce dispositif doit pourvoir être fabriqué avec les matériaux disponibles à Cuba.

## Les fours solaires

Je me suis donc intéressé aux fours solaires qui sont des dispositifs qui permettent de cuire des aliments grâce à la chaleur des rayons du soleil captés au moyen de miroirs.

Il existe différents types de four solaire qui se différencient par le type de miroir employé et les principes physiques mis en jeu :
<br>- Les fours <b>paraboliques</b> et de type <b>table</b> : ce sont des fours qui concentrent les rayons du soleil en un point à haute tempéarture (+/-250°C)
<br>- Les fours de type <b>boite</b> : ils utilisent des miroirs plats qui réfléchissent la lumière vers une boite vitrée et isolée qui génère un effet de serre.
<br>- Les fours <b>cylindro-paraboliques</b> : combinent la concentration des rayons et l'effet de serre. Le récipient chauffant est enfermé dans un tube en verre sous vide.
<br>- Les fours de type <b>panneaux</b> : ce sont de simples panneaux réflécteurs qui renvoient les rayons du soleil sur un récipient qui absorbe la chaleur.

<img src="../images/Diapositive6.JPG" width="50%">

[Les fours solaires](http://solarcooking.org/francais/plans.htm#Boites_-_Fours_solaires)
<br>[Solar Cooker International](https://www.solarcookers.org/)


## Pasteurisation de l'eau et des conserves


L'eau peut être pasteurisée à 70°C pendant 15 minutes
<br>-> [voir document de l'OMS](https://www.who.int/water_sanitation_health/dwq/Boiling_water_01_15.pdf)

Il y a différentes méthodes pour pasteuriser les conserves :

<b>Dans l'eau</b>
<br>-La pasteurisation à 80°C : elle est intéressante car elle permet de préserver le goût et les nutriments des aliments, mais il est nécessaire d'avoir un indicateur pour savoir si cette température est atteinte. Aussi les fours solaires ne permettent pas une température stable à 80°C, ils se stabilisent à une température supérieur à 100°C.
<br>- La pasteurisation stérilisante à 100°C : cette méthode est la plus commune, par contre elle n'est pas suffisante pour les aliments non acides. Les avis sont assez partagés à ce sujet, certains disent qu'il est possible de faire des conserves d'aliments non acides avec cette méthode à condition de les conserver au frais (température <14°C).
<br>- La stérilisation entre 116 et 140°C : elle détruit tous les micro-organismes y compris les spores. Cette température de l'eau ne peut être atteinte qu'avec une autoclave (autocuiseur) qui fait monter la pression pour augmenter la température au delà de 100°C. Elle est préconisée pour les aliments non acides. Les conserves ainsi préparées peuvent être conservées à température ambiante. Cette méthode n'est pas adaptée au contexte de Cuba, car il faut posséder une autoclave onéreuse et chauffer cette ustensile nécessite une puissance conséquente (équivalente à une plaque au gaz).

<b>Au four</b>
<br>- Pasteurisation au four à 150°C : La pasteurisation peut se faire au four sans plonger les bocaux dans l'eau.

Source : [Ecoconso - Conserves](https://www.ecoconso.be/fr/content/conservation-comment-steriliser-pasteuriser-les-fruits-et-legumes)



## Comment pasteuriser les conserves à 100°C

Choix des bocaux : le mieux est d'utiliser des bocaux twist off ils sont en verre et sont utilisés pour les conserves vendues dans le commerces, donc on peut facilement les récupérer. Il faut juste s'assurer que le couvercle posséde une bosse qui va s'enfoncer à la fin de la pasteurisation quand les bocaux vont refroidir. Si la bosse ne s'enfonce pas cela indique que la conserve est ratée car en refroidissant un vide se crée dans la conserve provoquant l'enfoncement de la bosse, on peut alors entendre un "poc" comme à la première ouverture.

Etape 1 : Nettoyer les bocaux à l'eau savoneuse

Etape 2 : Préparer les aliments selon la recette (voir "El libro de la Familia")

Etape 3 : Mettre les aliments dans les bocaux en laissant un espace de 1 à 2 cm. S'assurer que les aliments sont bien couverts d'eau. Puis fermer les bocaux.

Etape 4 : Mettre les bocaux dans un récipient d'eau. Ils doivent être recouverts d'eau moins 2 cm d'eau. Puis les faire bouillir le temps indiqué par la recette (15 min pour les bocaux de 20 cl; 20 min pour 50 cl selon [cet article](https://www.lexpress.fr/styles/saveurs/sterilisation-des-bocaux-conserves-ou-confiture-mode-d-emploi_1303898.html))

Source : [Ecoconso - Conserves](https://www.ecoconso.be/fr/content/conservation-comment-steriliser-pasteuriser-les-fruits-et-legumes)



# Phase 2 - Recherche du type de four adapté

## Four solaire de type boite

Pour répondre à ma problématique j'ai cherché le type de four solaire le plus approprié. Les fours solaires à concentrations ne permettent pas de chauffer des récipients de grande capacité, ils sont adaptés pour une petite marmite qui ne permet pas de faire des conserves. Il me reste donc deux options : un four solaire de type boite ou de type panneaux.
<br> Dans un premier temps je me suis tourné vers un four solaire boite car ce type de four solaire est très éfficace, la température de cuisson peut monter à 150°C en 30 minutes. Les conserves peuvent donc se faire à 150°C sans eau et donc sans récipient pour les contenir ce qui est un avantage.

J'ai trouvé [un article espagnol](http://www.diariodemarratxi.com/se-presenta-en-baleares-un-proyecto-de-hornos-solares-eficientes/) très intéressant à ce sujet.
<br>Dans les îles Baléares (Espagne), sur l'île de Minorque, l'IBANAT (Instituto BAlear de NATuraleza = Institut Baléare de la Nature) a conçu un four solaire de type boite très efficace et polyvalent capable de faire bouillir de l'eau, de pasteuriser et cuire toutes sortes d'aliments. La température est montée jusqu'à 150°C. Les expérimentations ont été faites fin avril 2016, la température ambiante était de 18°C. <br>Leur projet s'appelle "Ecocina Solar", j'ai trouvé [la page Facebook du projet](https://www.facebook.com/pg/ecocina/posts/) et j'ai découvert qu'ils ont déjà fait des conserves grâce à ce four. En faisant des recherches sur ce type de four j'ai découvert qu'il s'inspire du four [Global Sun Oven](https://www.sunoven.com/around-the-world/benefits/), d'après [l'atlas de la cuisine solaire](https://www.atlascuisinesolaire.com/fonctionnement-et-caracteristiques-four-solaire.php) la température de ce four monte à 150°C en 30 min. Avec ce type de four on peut tout cuire mais il faut compter 3 fois plus de temps de cuisson qu'avec un four classique (électrique ou gaz).

<img src="../images/horno-solar-300x336__1_.jpg">
<img src="../images/All_American_Sun_Oven.jpg" width="25%">

J'ai aussi trouvé de la documentation sur ce type de four :
<br>-[l'atlas de la cuisine solaire](https://www.atlascuisinesolaire.com/fonctionnement-et-caracteristiques-four-solaire.php)
<br>-[Four Atominique](http://four-solaire.iguane.org/)
<br>-[Tutoriel Fablabo](https://fablabo.net/wiki/Four_solaire_30%C2%B0)
<br>- [Bolivia Inti](http://www.boliviainti-sudsoleil.org/spip.php?article453)
<br>- [Solar Cookers International]( https://www.solarcookers.org/)
<br>- [Solar cookers Wiki](https://solarcooking.fandom.com/wiki/Category:Solar_cooker_plans)
<br>- [Tutoriel Low Tech Lab](https://wiki.lowtechlab.org/wiki/Four_solaire_%28cuiseur_type_bo%C3%AEte%29)
<br>- [Brochure Cuiseurs Solaires](https://www.pearltrees.com/s/file/preview/129215780/plansfr.pdf)

## Fours solaires cubains

En faisant des recherches j'ai découvert que ce type de four existait à Cuba [voir four solaire cubain](https://www.cubanet.org/mas-noticias/horno-solar-despierta-curiosidad-y-ahorra-dinero/).

<img src="../images/FourCubain.JPG" width="50%">

Il existe à Cuba le Groupe des Energies Renouvelables Appliquées = Grupo de Energías Renovables Aplicadas (GERA) en espagnol, fondé par le professeur de physique Jorge Bonzon de l'Université de l'Est (Universidad de Oriente) en 1997. Ce groupe fait des recherches sur les énergies renouvelables et fait la promotion des fours solaires. Le GERA a installé des fours solaires dans des lieux publics, principalement à Santiago de Cuba, pour faire connaitre ce procédé auprès de la population. Il organise aussi des activités pédagogiques auprès des enfants des écoles primaires pour les sensibiliser aux énergies renouvelables. Leur dernière activité semble dater de 2016.

[Article SCI](https://solarcooking.fandom.com/wiki/Grupo_de_Energ%C3%ADas_Renovables_Aplicadas)
<br>[Rapport SCI de 2011](https://vignette.wikia.nocookie.net/solarcooking/images/6/65/Applied_Renewable_Energy_Group%2C_Cuba_-_Report_Summer_2011.pdf/revision/latest?cb=20110903185215)
<br> [Facebook du GERA](https://www.facebook.com/pages/category/Nonprofit-Organization/Grupo-de-Energ%C3%ADas-Renovables-Aplicadas-140669672633240/)

Ce groupe a construit différents types de fours solaires mais diffuse principalement les fours solaires de types boites auprès de la population. On peut constater que ces fours boites "cubains" ont tous le même design avec un miroir fixe et conique.


## Prototype 1


Malgré l'existance des fours boîtes à Cuba je me suis focalisé sur ce type de four en essayant d'apporter quelques améliorations au four cubain comme :
<br>- Un sytème pour orienter le four face au soleil en toute saison
<br>- Une forme de miroir plus facile à farbriquer
<br>- L'utilisation de canettes découpées et plaquées sur du bois pour fabriquer le miroir

J'ai décidé de fabriquer un four solaire à quatre miroirs, inspiré du Global Sun Oven. Pour dimensionner mon four solaire je me suis basé sur les capacités d'un four traditionnel. Je voulait que l'on puisse cuisiner des plats familiaux et faire bouillir de l'eau pour toute la famille soit au moins 10L d'eau.

Suite à la fabrication de ce four boîte et aux remarques faites lors de la présentation je me suis rendu compte que ce type de four n'était pas adapté à ma problématique. Mon prototype présentait plusieurs inconvénients :
<br>- trop encombrant : j'ai mal dimensionné mon four, se baser sur la taille d'un four traditionnel n'était pas une bonne idée. Certe cela permet de cuisiner toutes sortes de plats mais pour faire des conserves une dimension plus petite aurait été plus judicieuse.
<br>- trop lourd : quand j'ai conçu mon prototype je n'ai pas pris en compte le poids des matériaux, résultat j'ai eut beaucoup de peine à le déplacer. Ce qui est problématique car mon idée est de créer un four mobile pour que les utilisateurs qui n'ont pas d'habitat ensoleillé puissent le déplacer dans un endroit adapté.
<br>- matériaux indisponibles : le problème majeur sont les matériaux nécessaires pour fabriquer ce type de four. Il faut du bois, du verre et de l'isolant, hors ces matériaux ne se trouvent pas facilement à Cuba. De plus il faut des outils spécifiques que tout le monde ne posséde pas : scie sauteuse, perceuse, coupe-verre.


## Quel four solaire choisir ?

Etant donné les inconvénients du four solaire de type boite, il fallait que je trouve un autre système.

J'ai fait un point sur les différents types de fours et j'ai essayé de trouver les fours avec une fabrication la plus simple possible et qui puissent être adaptée à ma problèmatique de conserves et de manque de matériaux.
<br>J'ai trouvé un four appelé <b>"pneu cuisinier"</b> qui est fabriqué avec une chambre à air de roue de voiture (camion), une plaque de verre et de polystyrène. Sa fabrication est assez simple mais la récupération de tous ces matériaux n'est pas garantie à Cuba. De plus la documentation sur ce type de four est assez mince, on sait juste que ça utilise le principe d'effet de serre et que ça peut cuire du riz. Aussi ce type de four n'est pas orientable ce qui pose problème quand le soleil est bas.

Sources :
<br>[Pneu cuisinier](http://solarcooking.org/francais/tire_eng-fr.htm)
<br>[Tuto Low Tech Lab - Four solaire chambre à air](https://wiki.lowtechlab.org/wiki/Four_solaire_-_chambre_%C3%A0_air#Commentaires)

Finalement j'ai trouvé le type de four solaire qui pourrait correspondre à ma problématique : <b>les cuiseurs de type panneaux</b>. Ce sont en fait de simple panneaux réflécteurs qui vont renvoyer les rayons du soleil sur un récipient conducteur. Ce récipient métallique et de couleur noire va absorber le chaleur des rayons du soleil ce qui va permettre de cuire des aliments. Pour éviter les déperditions à cause du vent, le récipient est enfermé dans un sac de cuisson transparent ou dans une cloche en verre. La température peut monter jusqu'à 120°C en +/-2h. Ce qui est une température suffisante pour pasteuriser à 100°C.

Sources :
<br>[Tous les fours solaires](http://solarcooking.org/francais/plans.htm#Boites_-_Fours_solaires)
<br>[Le Cookit](http://solarcooking.org/francais/cookit-fr.htm)

L'avantage de ce type de four c'est que sa fabrication est simple et ne nécessite qu'un matériaux, en général du carton. Il en existe de différentes formes, mais le principe est toujours le même : renvoyer un maximum de rayons vers le récipient.

# Phase 3 - Les cuiseurs à panneaux

## Les cuiseurs à panneaux

J'ai analysé les différents modèles existants et le Cookit semble être la référence dans ce domaine. Le Cookit a été créé en 1994 par Roger Bernard (FR) et Barbara Kerr (US) en collaboration avec Solar Cooker International. Il se construit grâce à un simple patron en carton et il a fait ses preuves sur le terrains.

<img src="../images/solar_cooker.jpg">

Cependant le problème est que le miroir avant est difficilement orientable. En hiver le miroir avant doit être quasiment déplié ce qui risque de défaire complétement le pliage et apparement en été son efficacité est remise en cause dans les pays près de l'équateur. Certains modèles ont été conçus pour essayer de resoudre ce problème comme le [DSPC](http://solarcooking.org/francais/DSPC-Cooker-fr.htm) ou le [All Season Solar Cooker](https://www.allseasonsolarcooker.com/). Cependant ces modèles utilisent des pliages assez complexes, le DSPC est trop petit pour l'usage que je souhaite en faire et n'a que 2 positions; l'autre nécessite une quantité importante de carton (polypropylène), des vis et l'orientation des réflécteurs semble difficile à appréhender.

<table>
<tr align=top>
	<td>
		DSPC
	</td>
	<td>
		All Season Solar Cooker
	</td>
</tr>
<tr align=top>
	<td width="25%">
		<img src="../images/image003.jpg">
	</td>
	<td width="25%">
		<img src="../images/t%C3%A9l%C3%A9chargement__3_.jpg">
	</td>
</tr>
</table>

L'idée serait donc de concevoir un cuiseur à panneaux qui s'inspire du Cookit en réglant le problème de l'orientation du miroir avant. Il doit pouvoir être fabriqué avec les matériaux disponibles à Cuba. Il faut aussi qu'on puisse faire des conserves avec ce type de four, ce qui n'a jamais été fait.

Source :
[Cookit](https://solarcooking.fandom.com/wiki/CooKit)
[Tous les fours solaires](http://solarcooking.org/francais/plans.htm#Boites_-_Fours_solaires)


## Fonctionnement des cuiseurs à panneaux

Les cuiseurs à panneaux fonctionnent selon plusieurs phénomènes scientifiques :

<table>
<tr align=top>
	<td width="25%">
		<img src="../images/panel.JPG">
	</td>
	<td width="25%">
		<img src="../images/final_55f6b1ba726505.56924389.png">
	</td>
	<td>
		La réfléxion de la lumière : les panneaux réflécteurs réfléchissent les rayons du soleil vers le récipient chauffant.
	</td>
</tr>
<tr align=top>
	<td width="25%">
		<img src="../images/1.JPG">
	</td>
	<td width="25%">
		<img src="../images/p1066i2.jpg">
	</td>
	<td>
		L'absorption de la lumière : le récipient doit accumuler et conduire la chaleur rapidement, ainsi il est préférable qu'il soit en métal et peint en noir. Le métal est un matérieux très conducteur. La couleur noire absorbe l'ensemble du spectre de la lumière blanche dont les infra-rouges responsables de la chaleur. Les couleurs claires au contraire repoussent certaines longueurs d'ondes ce qui engendre une perte d'énergie.
	</td>
</tr>
<tr align=top>
	<td width="25%">
		<img src="../images/panelret.JPG">
	</td>
	<td width="25%">
		<img src="../images/ir_5.jpg">
	</td>
	<td>
		La rétention de chaleur : pour limiter les déperditions thermiques il est conseillé d'enfermer le récipient chauffant dans une enveloppe protectrice. Cette enveloppe est au moins un sac de cuisson pour limiter les déperditions dû au vent, mais l'idéal est d'utiliser une enveloppe en verre ainsi les infra-rouges sont piégés dans l'enveloppe ce qui conntribue à une augmentation plus rapide de la température.
	</td>
</tr>
</table>

## Conditions de fonctionnement

Pour que le système fonctionne au mieux il faut que plusieurs conditions soient réunies :
<br>- L'orientation des panneaux : le cuiseur doit être positionné face au soleil et les panneaux doivent être orientés idéalement pour réfléchir les rayons du soleil vers le récipient chauffant en chaque saison.

<img src="../images/RTEmagicC_solsequi_sm_01.gif.gif">

<br>- La météo : un bon ensoleillement est primordial
<br>- Heure de la journée : le système fonctionne mieux aux heures où le soleil est le plus puissant (de 10h à 14h)
<br>- Enveloppe protéctrice : comme expliqué précédement pour limiter les déperditions thermiques il est conseillé d'enfermer le récipient chauffant dans une enveloppe protectrice. Cette enveloppe est au moins un sac de cuisson pour limiter les déperditions dû au vent, mais l'idéal est d'utiliser une enveloppe en verre.
<br>- Surélévation du récipient : il est conseillé de surélever le récipient de quelques centimètres (environ 6 cm) pour que le dessous du récipient profite des rayons réfléchis sur le sol du cuiseur et pour laisser passer l'air de l'enveloppe tout autour du récipient afin de limiter les dépérditions thermiques.
<br>- L'épaisseur du récipient : le récipient doit être noir et métalique mais aussi le moins épais possible pour qu'il chauffe plus vite.
<br>- La quantité d'eau : une petite quantité d'eau chauffe plus vite.
<br>- La quantité et la taille des aliments : les aliments doivent être découpés en petits morceaux, plus la quantité est limitée plus la cuisson est rapide.
<br>- Le vent : le vent accentue les dépérditions thermiques, il refroidit le récipient c'est pourquoi une enveloppe protectrice est recommandée.

<img src="../images/4.JPG">


## Prototype 2

J'ai donc fabriqué un cuiseur à panneaux. Pour le design de mon cuiseur solaire je me suis inspiré du Cookit tout en essayant d'avoir un panneau orientable en toutes saisons. Je l'ai dimensionné pour faire mes conserves et grâce à fusion j'ai déterminé les angles adéquats pour chaque saison à Cuba.

Comme il est difficile de se procurer du carton à Cuba j'ai cherché un matériau qui pourrait le remplacer et j'ai pensé au grillage de cage à poule. C'est un matériau forcément disponible à Cuba puisqu'ils font de l'élevage de volailles. Cependant si à la campagne les habitants en ont certainement dans leur stock, en ville c'est moins sûr, ils devront peut-être en acheter ce qui peut poser problème pour les plus pauvres. L'idée est de se servir du grillage pour accrocher les feuilles d'aluminium issue de canettes découpées.

Pour l'orientation du panneau avant j'ai pensé à un système de pieds rabattables qui vont le soulever pour avoir la bonne orientation. L'hiver le panneau est à l'horizontale (sans pieds). Il y a 2 pieds : un pour l'été et un pour les autres saisons. Les pieds sont fabriqués en fil de fer épais et sont retenus par des ficelles.
<br> En fabriquant le prototype je me suis rendu compte que le fil de fer que j'avais utilisé pour les pieds n'était pas assez épais car il se tordait et les ficelles n'étaient pas assez tendues alors qu'elles assurent la bonne géométrie de l'angle. Aussi je n'avais pas prévu de système assez rigide pour tenir les trois autres panneaux (arrière et latéraux) en garantissant leur bonne orientation et le grillage a tendance à gondoler. Du coup mon assemblage était assez bancale mais il peut être amélioré avec du fil de fer plus épais (2mm min.) et en renforçant les côtés du grillage avec des tiges fines de bambou pour l'aplanir.
<br>Le cuiseur pourrait même être fait en bambou. Les tiges de bambou assemblées en grille pourraient servir de support aux miroirs fait à partir de canettes d'aluminium découpées.

J'ai montré mon prototype aux profs et finalement ils m'ont conseillé de faire mon prototype en carton. Même si le carton n'est pas toujours disponible à Cuba l'idée est que le cuiseur solaire à panneaux peut être fabriqué avec n'importe quel matériau plane, découpable, léger et pliable sur lequel on peut plaquer les canettes découpées pour faire les miroirs. Ainsi il peut être fait en carton, en bois, en grillage ou en bambou, c'est toujours le même principe il faut juste adapter le système de pliage. Avec du bois on mettra des charnières ou du fil, avec du grillage ou du bambou on assemblera avec du fil, avec le carton en pliera ou on assemblera avec du ruban adhésif marron.


## Prototype 3

<img src="../images/IMG_3442.JPG" width="50%">

Mon troisième prototype était donc un cuiseur à panneau en carton. Comme pour les précédents prototypes j'ai déterminé l'orientation des panneaux grâce au logiciel fusion. Pour faire les conserves dans l'eau à 100°C il me fallait un récipient adapté. Une grande marmite d'eau mettrait trop de temps à chauffer c'est pourquoi j'ai eu l'idée d'utiliser des boites de conserves peintes en noir, ainsi chaque bocal de conserve à pasteuriser est plongé dans un petit récipient d'eau qui va chauffer beaucoup plus vite. Pour l'enveloppe protectrice j'ai utilisé 2 bouteilles en plastique (PET) de 5L d'eau que j'ai découpées de manière à avoir une bouteille ouvrable et refermable pour insérer le récipient.

Cependant il y avait plusieurs améliorations à apporter :
<br>- Vérifier la résistance à la chaleur des matériaux :
<br>Le PET risque de se déformer à cause de la chaleur accumulée dans le récipient qui peut monter à 120°C. Or pour le couvercle de mon récipient j'avais prévu d'utiliser du PET transparent de manière à voir l'eau bouillir signe que la pasteurisation des conserves commence.
<br>- Trouver un moyen de récupérer les bocaux à la fin de la pasteurisation sans se brûler :
<br>J'avais eu l'idée d'attacher une ficelle autour des bocaux pour pouvoir les attraper sans se brûler, mais cette solution nécessite une longue préparation pour chaque bocal. Il faudrait trouver une solution plus simple.
<br>- Trouver un système d'assemblage du cuiseur plus simple et durable :
<br>L'assemblage de mon cuiseur était assez compliqué, le carton s'est vite abimé en seulement 5 montage et démontage.
<br>- Changer l'orientation des panneaux latéraux :
<br>Les panneaux latéraux réfléchissent les rayons du soleil vers le récipient seulement en été, on a donc une perte d'efficacité pendant les autres saisons.

# Phase 4 - Cuiseur à panneaux final

### Dimensionnement

<img src="../images/Diapositive15.JPG">

J'ai dimensionné mon cuiseur pour qu'il puisse pasteuriser 4 conserves à la fois. Chaque dispositif de pasteurisation récipient + enveloppe de protection a un diamètre de 15 cm. Ils se disposent alignés à l'avant sur le sol du cuiseur. La longueur minimale du sol est donc de 60 cm. Les autres mesures découlent des phénomènes de réflexion des rayons du soleil.  

### Orientation des miroirs

J'ai déterminé l'orientation idéale des panneaux pour que le système soit efficace en chaque saison grâce au logiciel fusion. Pour ce faire il faut d'abord connaitre la hauteur du soleil (angle des rayons par rapport au sol horizontal) pour chaque saison à Cuba. On peut se fier aux données trouvé sur internet [Sun Earth Tools](https://www.sunearthtools.com/dp/tools/pos_sun.php?lang=fr) mais on peut aussi la déterminer par calcul en connaissant les coordonnées géographiques de centre de Cuba (aux alentours de Ciego de Avila : Latitude = 21.8°, longitude = -78.7°)-> [voir la méthode](https://energieplus-lesite.be/theories/climat8/ensoleillement-d8/).

La hauteur du soleil en chaque saison est donc (à 1° près):
<br>- En printemps/automne (équinoxes) : 68°
<br>- En été (solstice) : 90°
<br>- En hiver (solstice) : 45°

<br> Ensuite, les inclinaisons et les dimensions des panneaux réflecteurs sont déterminées géométriquement avec fusion afin que les rayons recouvrent l'ensemble de la surface des récipients à chauffer.
On obtient les résultats suivants :
<img src = ./images/Diapositive16.JPG>

### Matériaux
L'avantage de ce type de four est qu'il peut être fabriqué avec des matériaux divers.
<br> Pour les miroirs : des canettes découpées ou du papier aluminium.
<br> Pour la structure : du carton, du bois, du grillage de cage à poule (ou autres), du bambou...
<br>L'important est d'avoir un support rigide sur lequel on va plaquer les canettes découpées ou le papier aluminium.
<br>On va adapter la fabrication en fonction des matériaux disponibles.

## Conserve sans marmite

<img src="../images/IMG_2352.JPG" width="25%">
<img src="../images/IMG_2381.JPG" width="25%">

Le récipient : Comme expliqué précédemment une grande marmite d'eau mettrait trop de temps à chauffer. Un petit récipient métallique adapté à la taille d'un bocal de conserves chaufferait beaucoup plus vite. C'est pourquoi j'ai eu l'idée d'utiliser des grandes boites de conserves peintes en noir. Les boites métalliques de lait infantile ont une taille convenable (13 cm de diamètre pour 15 à 16,5 cm de haut) qui s'adapte à la plupart des bocaux twist off.

Les bocaux : le mieux est d'utiliser des bocaux twist off ils sont en verre et sont utilisés pour les conserves vendues dans le commerce, donc on peut facilement les récupérer. Il faut juste s'assurer que le couvercle possède une bosse qui va s'enfoncer à la fin de la pasteurisation quand les bocaux vont refroidir. Si la bosse ne s'enfonce pas cela indique que la conserve est ratée car en refroidissant un vide se créé dans la conserve provoquant l'enfoncement de la bosse, on peut alors entendre un "poc" comme à la première ouverture.

Le couvercle du récipient : comme expliqué précédemment (voir Prototype 3) je comptais utiliser un couvercle en plastique transparent (PET) pour qu'on puisse voir l'eau bouillir, mais je n'étais pas sûr de la résistance à la chaleur de ce matériau. J'ai donc testé ce matériau et il s'avère qu'il se déforme sous l'effet de la chaleur dès 60°C. Il fallait que je trouve une autre solution. J'ai donc décidé d'utiliser le fond d'une boite de conserve de même diamètre que le récipient. Le problème étant qu'avec ce couvercle métallique je ne peux plus voir l'eau bouillir, il faut donc que je créer un indicateur de température.  

Système pour retirer les bocaux sans se brûler après la pasteurisation : pour ne pas se brûler j'ai eu l'idée de mettre le bocal de conserve dans un filet d'oignons (de patates ou d'agrumes) afin de pouvoir le retirer sans le toucher mais en attrapant le filet qu'on laisse dépasser à l'extérieur du récipient chauffant. Ce matériau résiste bien à la chaleur.

Enveloppe de protection : On peut mettre le récipient dans un sac de cuisson gonflé d'air. Si les sacs de cuisson ne sont pas disponibles la solution que j'ai trouvée est d'utiliser 2 bouteilles d'eau de 5L (en PET transparent) coupées et assemblée pour former une bouteille ouvrable dans laquelle on enferme le récipient. Cependant le PET ne résiste pas à la chaleur, les bouteilles risquent de se déformer au-delà de 50°C, si c'est le cas il faut les changer. Le changement n'est pas un problème car les bouteilles de 5L sont très utilisées à Cuba. Dans l'idéal pour avoir de meilleures performances il faudrait utiliser une enveloppe en verre, comme une mini serre.

## Indicateur de température

J'ai cherché des expériences avec l'eau bouillante dont je pourrais m'inspirer pour fabriquer mon indicateur. J'ai trouvé une expérience sur l'expansion de l'eau chaude [voir l'expérience](http://oceans.taraexpeditions.org/wp-content/uploads/2014/06/ficheeducvfdilatationeauchaude.pdf). Dans cette expérience on remplit d'eau un erlenmeyer à ras bord, l'eau est colorée, puis l'erlenmeyer est fermé avec un bouchon étanche traversé d'une paille. Quand on plonge l'erlenmeyer dans l'eau chaude, l'eau colorée chauffe et remonte dans la paille car l'eau chaude prend plus de volume que l'eau froide, on appel ce phénomène : l'expansion de l'eau.
<br>J'ai donc décidé de reproduire cette expérience en utilisant des canettes découpées et encastrées pour remplacer l'erlenmeyer et un stylo bille transparent pour la paille.
<br>J'ai testé ce dispositif mais je me suis rendu compte que l'eau débordait de la paille, il fallait donc que je calibre mon dispositif.
<br>En faisant des recherches sur l'expansion de l'eau j'ai découvert qu'il existait un pourcentage de dilatation en fonction de la température -> [voir source](https://formation.xpair.com/cours/generalites.htm). A 100°C le volume de l'eau augmente de 4,4%. A partir de ces données j'ai pu calculer la taille du réservoir idéal pour que l'eau ne déborde pas de la paille.

Résultats :
<br>Si le volume du stylo = 2,16 cm3
<br>Alors le volume du réservoir = 49,1 cm3
<br>comme le réservoir est fait avec des canettes de 6,8 cm de diamètre la hauteur d'eau à l'intérieur doit être de 1,3 cm

Détail du calcul :

<img src="../images/CalculRes.jpg" width="70%">

J'ai testé mon indicateur en le posant sur l'eau bouillante et le résultat est assez probant.

<img src="../images/IMG_2321.JPG" width="25%">

Cet indicateur est ensuite intégré au couvercle du récipient de pasteurisation.

<img src="../images/IMG_2352.JPG" width="25%">
<img src="../images/IMG_2347.JPG" width="25%">

## Fabrication

La fabrication est très simple et se fait à l'aide d'outils répandus comme un cutter et des ciseaux. Pour la structure il suffit de découper le carton selon les plans et d'appliquer le papier aluminium ou les canettes découpées dessus. La fabrication du récipient est une simple découpe de boite de conserve.

-> [voir le tutoriel](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/four-solaire/tutorial/)

La découpe du carton peut s'effectuer avec une découpeuse laser

## Expérimentation

Une fois mon cuiseur à panneaux réalisé j'ai voulu l’expérimenter. Comme le temps était ensoleillé en Belgique le 06/01/2020 (avec une température de 7°C et un vent modéré) j'ai voulu tester le principe de mon cuiseur solaire en condition réel, mais j'ai dû l'adapter à la hauteur du soleil en hiver en Belgique qui correspond à une inclinaison des rayons de 18° par rapport au sol (horizontal). J'ai relevé la température de l'eau dans le récipient grâce à une thermomètre sonde de cuisson. J'ai pu constater une augmentation de la température de l'eau de 2°C toutes les 5 minutes.

J'ai ensuite expérimenté mon modèle final conçu pour Cuba en l'exposant à une lampe de 1500 W, je l'ai éloigné de la lampe de manière à avoir une puissance de 1360W/m² qui correspond à la puissance du soleil. J'ai relevé la température avec le thermomètre sonde et j'ai obtenu les mêmes résultats qu'en condition réelle, c'est à dire une augmentation de 2°C toutes les 5 minutes soit 24°C/h. Je peux en déduire que si la température initiale de l'eau est de 30° (température extérieure à Cuba) elle devrait atteindre 100°C en 3h.

Ainsi il est possible de pasteuriser les conserves aux heures où le soleil est le plus puissant c'est à dire de 11h à 14h.

<img src="../images/Diapositive20.JPG">
