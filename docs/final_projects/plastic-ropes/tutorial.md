# Plastic Ropes Tutorial

### First plastic bottle cutter prototype 
_First wood prototype!_

[Tutorial video](https://www.youtube.com/watch?v=zERXKYLG9cM)

Material required: 

* 1 cutter blade
* Wooden Handle
* 4 nails
* Wood saw
* Wood file

![image en ligne](images/image7.jpg) 

> Handable, compact, one cutting thickness.

We encountered some problems because of the quality of the wood used during the cuts. 
Normally we can have several cutting thicknesses thanks to the different slots created, however the wood is fiber, suddenly the blocks are torn off.

### Prototype 2
_First prototype via 3D printing!_

[Link to the model](https://www.thingiverse.com/thing:3512653)

* [Prototype Folder 1](images/pr.stl)

* [Prototype Folder 2](images/bot.stl)

Material required: 

* 1 cutter blade
* 2 nuts and bolts

![image en ligne](images/image8.jpg)

> Compact, modulable cutting thickness, fixable, support for the bottle

### 3 ème prototype 
_Second prototype via 3D printing!_

[Link to the model](https://www.thingiverse.com/thing:1997447)

* [Prototype 3](images/smallblade9.5mmbottlecutter.stl)

Material required: 

* 1 cutter blade
* 2 nuts and bolts

![image en ligne](images/image9.jpg)

> Ultra compact, one cutting thickness, fixable

### Cutting tutorial

* Slide the bottle into the holder
* Determine the width of plastic wire required
* Create the first notch and get a first filament
* Pull the thread

![image en ligne](images/image18.jpg)

### Prototype 4
_Third prototype via 3D printing_

Material required: 

* 1 cutter blade
* 1 nuts and bolts

[Link to the model](https://www.thingiverse.com/thing:1940067)

* [Prototype 4 Folder 1](images/Plastic_Bottle_Cutter_Slider.stl)

* [Prototype 4 Folder 2](images/Plastic_Bottle_Cutter_Center.stl)

![image en ligne](images/image20.JPG)

> We encountered difficulties following the insufficient support of the bottle and the difficulty to slice the plastic with this model.

### Prototype 5
_UTSUMI shredder models_ 

![image en ligne](images/image21.JPG)

[Link to the models](http://utsumi.com.br/pet/English/filetador/index.html)

_HOW TO CREATE YOUR SHREDDER_

![image en ligne](images/image26.jpg)

[Link to the website](http://utsumi.com.br/pet/English/dicas/index.html)

_UTSUMI VIDEO presentation_

[Link to the video](https://youtu.be/UN7TwJa_2U8)

_UTSUMI CRAFTS_

[Link to the website](http://utsumi.com.br/pet/English/artesanato/index.html)

_UTSUMI TIPS_

[Link to the tips](http://utsumi.com.br/pet/English/dicas/index.html)

### Crank
_Via 3D Printing_

[Folder crank 1](images/support-manivelle.stl)

[Folder crank 2](images/Manivelle.stl)

[Folder crank 3](images/tube-vis.stl)

![image en ligne](images/image23.JPG)

![image en ligne](images/image22.JPG)

### Coil
_Handmade_

![image en ligne](images/image24.JPG)

![image en ligne](images/image25.JPG)

## Prototype 6
_Models fixed on wooden board_

Material required: 

* Nails
* Wooden board

[Prototype 6 Folder 1](images/bottle-cutter-modifi__.stl)

We just resumed our basic model to which we made some negligible changes.
 To this we have added a classic spool crank. the assembly was aimed at on a board halfway between them.
 
![image en ligne](images/image28.JPG)

![image en ligne](images/image30.JPG)

[video 1](https://www.youtube.com/watch?v=RdmX0f7ZdYk)

[video 2](https://www.youtube.com/watch?v=PY3at5waTKU)


![image en ligne](images/image29.JPG)
![image en ligne](images/image31.JPG)

he two elements were printed independently of each other before being put together on the same board
- predefine the desired thickness,
- Insert the bottle,
- pull up to the spool level.
- It is necessary to foresee the holes on the edges of the reel to fix the rope.
- Once fixed, operate the crank and turn to the end.


## Chair prototype

 | ![image en ligne](images/banc4.jpg) | ![image en ligne](images/banc_5.jpg) |

- have a frame
- weave the seat
- staple it on the frame

| ![image en ligne](images/banc_2.jpg) | ![image en ligne](images/banc.jpg) |

- Hot water was passed over the seat which allowed it to stiffen and make it more flexible.


