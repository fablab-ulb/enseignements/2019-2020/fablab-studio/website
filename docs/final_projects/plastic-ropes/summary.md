# Plastic ropes, a new way to recycle plastic bottle in Cuba 

### Summary 

![Plastic Ropes](images/summary.jpg)

### Problématique (FR)
- La bouteille plastique PET envahit Cuba 
- L'économie est basée presque exclusivement sur le tourisme
- l'UERMP (The Union of Enterprises for the Recovery of Raw Materials) ne suffit plus à endiguer leur présence excessive sur l'île

### Question.

> Comment créer une alternative au recyclage des bouteilles plastiques PET pouvant leur conférer une plus-value et facilement utilisable par les cubains?

_Hypothèse :_

<ul><li>La première étape est de créer une plus-value à la bouteille plastique, une alternative à la revente directe.
On en récupère et conserve davantage grâce à l'action des habitants.

<li>La seconde étape est la transformation de la bouteille, en matériau, plus précisément en fils.
On les effile grâce à un cutter designé pour la bouteille plastique, un outil simple à produire, peu couteux en matériaux et facilement répandable sur l'île.
Que faire du fil, quel avantage?

<li>Troisième étape, l'artisanat!
La création d'un catalogue d'objets, plumeau de balais, toiles géotextiles, sacs, ... à usage personnel ou à revendre aux touristes. </ul>

### Problematic (EN)
- The PET plastic bottle is invading Cuba with an economy based almost exclusively on tourism. 
- The UERMP (The Union of Enterprises for the Recovery of Raw Materials) is no longer sufficient to stem their excessive presence on the island.

### Question.

> How can we create an alternative to the recycling of plastic PET bottles that can give them added value and that can be easily used by Cubans?

_Hypothesis :_

<ul><li>The first step is to create added value for the plastic bottle, an alternative to direct resale.
The first step is to create added value for the plastic bottle, an alternative to direct resale.

<li>The second step is the transformation of the bottle into material, more precisely thread.
The second step is the transformation of the bottle into material, more precisely into threads, which are cut with a cutter designed for plastic bottles, a tool that is simple to produce, cheap in materials and easily spread on the island.
What to do with the thread, what advantage?

<li>Third step, the craft!
The creation of a catalogue of objects, broom duster, geotextile fabrics, bags, ... for personal use or for resale to tourists. </ul>



## Problematica (ES)
- La botella de plástico PET está invadiendo Cuba con una economía basada casi exclusivamente en el turismo. 
- La UERMP (Unión de Empresas para la Recuperación de Materias Primas) ya no es suficiente para frenar su excesiva presencia en la isla.

## Pregunta.
> ¿Cómo podemos crear una alternativa al reciclaje de las botellas de plástico PET que pueda darles un valor añadido y que pueda ser fácilmente utilizada por los cubanos?

_Hipótesis :_

<ul><li>El primer paso es crear un valor añadido para la botella de plástico, una alternativa a la reventa directa.
El primer paso es crear un valor añadido para la botella de plástico, una alternativa a la reventa directa, y recuperar y conservar más de ella gracias a las acciones de los habitantes.

<li>El segundo paso es la transformación de la botella en material, más precisamente en rosca.
El segundo paso es la transformación de la botella en material, más precisamente en roscas, que se cortan con un cortador diseñado para botellas de plástico, una herramienta sencilla de producir, barata en materiales y fácil de extender en la isla.
¿Qué hacer con el hilo, qué ventaja?

<li>Tercer paso, la nave!
La creación de un catálogo de objetos, escobas, tejidos geotextiles, bolsas, ... para uso personal o para la reventa a los turistas. </ul>
