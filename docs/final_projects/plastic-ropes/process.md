# Plastic Ropes 

## Processus de recherche

### Idée de départ!

Partant de nos premiers travaux sur Cuba, nous avons assimilé tout un tas de connaissances grâce à:

- la traduction de "Con Nuestros Proprios Esfuerzos" 
- au premier rendu sur les filtres à eau 
- nos échanges avec Ernesto Orosa

Nous en avons conclu que les Cubains possédaient:

- un grand esprit de récupération des matériaux 
- une ingéniosité singulière

Nous avons décidé de profiter de ces deux aspects, nous restait à trouver une problématique!

_Hypothèse :_

La bouteille plastique pour créer des chaises!

### Premiers jets.

Nous avons cherché différentes méthodes pour créer des chaises à partir de ces bouteilles plastiques.
Principalement par stackage de celles-ci, petit à petit nous avons découvert l'artisanat de récupération autours de celles-ci.
Littéralement un petit catalogue : 

- plumeau de balai
- sac
- chaise
- objets décoratifs
- ...

![image en ligne](images/image1.jpg)

_Premiers doutes :_

La mise en oeuvre des bouteilles, devoir récupérer plusieurs dizaines de bouteilles identiques et en bon état n'est pas des plus pratiques.
On se rend compte qu'on doit reconsidérer la bouteille, on doit la remettre en question!

### Questionner la bouteille.

Nous avons continué nos recherches sur la bouteille :

_Fonction de départ_

**Comment la décliner ou spécialiser**

![image en ligne](images/Image2.jpg)

![image en ligne](images/image3.jpg)

![image en ligne](images/image17.jpg)

[TrussFormer](https://hpi.de/fileadmin/user_upload/fachgebiete/baudisch/projects/TrussFormer/TrussFormer_UIST2018.pdf)


Modèle test (3D printing)

On se rend compte que ce n'est pas tant la question de la fonction de l'élément bouteille qui nous intéresse.
Que ce soit comme élément structurel ou spécialiser/changer sa fonction de base.


_Matériau_

**Quels autres usages?**

![image en ligne](images/image4.jpg)

L'usage de la bouteille en tant que matière première nous intéresse beaucoup, mais sous quelle forme? Déchiqueté? Fondu? Effilé? 
On élimine les deux premières propositions car, la première rejette du microplastique tandis que la seconde émet de la dioxine (composé cancérigène).
Le plastique effilé nous séduit, on peut utiliser le fil dans l'artisanat et ce sous plein de forme!

**Toutes ces découvertes sont très intéressantes mais quel lien avec Cuba à ce stade? 
Pour le moment rien d'affirmé ou de concluant, nous devions prendre du recul sur le projet et cibler une problématique!**

### Situation Cuba.

Comment connaitre la situation sur place? 

_Ernesto Oroza_

![image en ligne](images/image5.jpg)

http://www.ernestooroza.com/updating-city-theorem-20002010/

On peut observer le type de bouteille, leur format, si elles sont lisses ou non, ...

**On obtient notre inventaire sur Cuba**

![image en ligne](images/image6.jpg)

On peut observer déjà un type d'utilisation de pétrole, potentiellement utilisable pour le plastique aussi?

**Autres pistes**

_Les journaux en ligne_ 

![image en ligne](images/image19.png)

[le recyclage, l'affaire de tous](http://fr.granma.cu/cuba/2017-08-24/le-recyclage-laffaire-de-tous)

[The plastic bottle is invading us](https://translatingcuba.com/the-plastic-bottle-is-invading-us/)

[A mas bloqueo mas reciclaje](https://www.cubahora.cu/blogs/el-foro/a-mas-bloqueo-mas-reciclaje)

[Pomos PET](https://www.cubahora.cu/blogs/el-foro/pomos-pet)

On retient plusieurs éléments importants de ces articles :

- L'économie du pays est essentiellement basée sur le tourisme. 

[Fan de Cuba](https://fancuba.com/histoire-tourisme/), [tourisme à Cuba](https://www.academia.edu/39755310/Le_tourisme_%C3%A0_Cuba_EPE_)

- Le tourisme importe beaucoup de déchets
- La bouteille plastique est un vrai problème par son nombre
- Un système de récupération des ressources premières existe mais ne suffit pas (UERMP et sociétés locales)

C'est une entité cubaine chargée de la collecte, du traitement et de la commercialisation des matériaux et des déchets recyclables générés par l’industrie, les services et la communauté. Marilin Ramos Polanco, responsable des ressources humaines de l’Uermp, nous a parlé du principal défi à relever aujourd’hui par cette entreprise. « Nous avons pour mission de diminuer les importations et obtenir des revenus grâce aux exportations de déchets qui ne peuvent pas être réutilisés dans notre pays. Cette année, nous envisageons de collecter, traiter et exporter environ 400 000 tonnes. [L'UERMP](http://fr.granma.cu/cuba/2017-08-24/le-recyclage-laffaire-de-tous).

![image en ligne](images/Annotation_2019-12-17_1029077.png)

- La revente directe des bouteilles plastiques est possible SI et SEULEMENT SI elles sont encore en bon état

![image en ligne](images/ramasseur.jpg)

![image en ligne](images/collectioneur.jpg)

- Le prix de vente d'1KG de bouteilles est de 5 CUC (soit 0,15 CUC pour une bouteille de 30g). 

[Recyclage](https://www.20minutes.fr/planete/1183645-20130704-20130703-cuba-recyclage-matieres-premieres-privatise-lentement).

**On en déduit une problématique!**

_Exchanges with Cubans._

**Principalement via les réseaux sociaux.**

![image en ligne](images/i.png)

![image en ligne](images/image10.png)

![image en ligne](images/image_13.png)

Nous nous sommes également reiseigné sur les caractéristiques d'une bouteille plastique PET de preférence.
Le PET est une matiére plastique qui se veut entiérement recyclage.

[Le PET](https://www.petrecycling.ch/fr/savoir/matiere-de-valeur-pet/petit-lexique-des-matieres-synthetiques)

[la bouteille PET](http://www.anwa.ch/fileadmin/anwander/pdf/Information_PET.pdf)

**Grâce à ces informations, nous avons pu identifiez un besoin, une demande! On peut ainsi relier notre projet à leur situation, qu'il acquiert une réelle utilité!
Mieux encore, ils ne connaissaient pas ce principe de récupération du plastique!**


## Problématique 
- La bouteille plastique PET envahit Cuba 

![image en ligne](images/Annotation_2019-12-05_220458.png)

![image en ligne](images/Annotation_2019-12-05_2204588.png)

- L'économie est basée presque exclusivement sur le tourisme
- l'UERMP (The Union of Enterprises for the Recovery of Raw Materials) ne suffit plus à endiguer leur présence excessive sur l'île

## Question! 
**Comment créer une alternative au recyclage des bouteilles plastiques PET pouvant leur conférer une plus-value et facilement utilisable par les cubains?**

_Hypothèse :_

- La première étape est de créer une plus-value à la bouteille plastique, une alternative à la revente directe.
On en récupère et conserve davantage grâce à l'action des habitants.

- La seconde étape est la transformation de la bouteille, en matériau, plus précisément en fils.
On les effile grâce à un cutter designé pour la bouteille plastique, un outil simple à produire, peu couteux en matériaux et facilement répandable sur l'île.
Que faire du fil, quel avantage?

- Troisième étape, l'artisanat!
La création d'un catalogue d'objets, plumeau de balais, toiles géotextiles, sacs, ... à usage personnel ou à revendre aux touristes.

**Création du cutter pour bouteilles plastiques**

## 1er prototype de plastic bottle cutter 
_Premier modèle en bois!_

Matériel nécessaire: 
- 1 lame cutter
- morceaux de bois, un manche de 20-25 cm de long
- 4 clous
- scie à bois
- lime à bois

![image en ligne](images/image7.jpg) 

 **Système à la main, compact, une seule largeur de découpe de la bouteille plastique**

## 2 ème prototype
_Premier prototype via 3D printing!_

Matériel nécessaire: 
- 1 lame cutter
- 2 écrous
- 2 rondelles

![image en ligne](images/image8.jpg)

**Compact, largeur de découpe modulable, fixable, support pour la bouteille**

## 3 ème prototype 
_Second modèle via 3D printing!_

Matériel nécessaire: 
- 1 lame cutter
- 2 écrous
- 2 rondelles

![image en ligne](images/image9.jpg)

Le modèle n'était pas des plus pratiques à l'utilisation, surtout à cause de son coté ultra compact.
De plus, on est limite obligé de fixer le modèle sur une table pour éviter d'avoir à la retenir.

 **Ultra compact, une seule largeur, fixable**

## Tutoriel
- Glisser dans le support la bouteille
- Déterminer la largeur de fil plastique nécessaire
- Créer la première entaille et obtenir un premier filament
- Tirer le fil

![image en ligne](images/CUTTER0.png)

![image en ligne](images/image18.jpg)

[vidéo tuto par nos soins :)](https://www.youtube.com/watch?v=5zcX-PbNg48)

## 4 ème prototype
_Troisième modèle via 3D printing!_

![image en ligne](images/image20.JPG)

On rencontre des difficultés avec ce modèle, que ce soit le support de la bouteille ou encore la découpe du plastique.
La découpe saute, le plastique de la bouteille finit toujours par arrêter sa découpe. 

## 5 ème prototype
_Modèle bricolé en suivant l'exemple du plastic shredder d'UTSUMI_

L'idée était de chercher à ajouter de nouvelles étapes dans la production du fil plastique.
On ajoute une manivelle, pour "aider" à tirer le fil plastique et surtout à stocker celui-ci sur des bobines.
On a commencé par répliquer la machine d'Utsumi pour pouvoir observer les points forts et faibles de ce dispositif.

Matériel nécessaire: 

- 1 lame cutter
- 2 écrous
- 2 rondelles 
- 1 profilé métallique troué en U
- 1 profilé métalliqué en U
- Vis
- 1 planche en bois
- 1 barre métallique

![image en ligne](images/image21.JPG)

On a rencontré des difficultés avec les finitions de la machine, plus particulièrement le dispositif de découpe. 
Le fil plastique finissait toujours par bloquer à cause des fentes qui sont en V et pas en U, pas faute d'avoir limé.
Même chose pour le système de support de la bouteille qui ne se fixait pas toujours correctement.
Beaucoup de petits problèmes sur la qualité de l'objet car on n'est pas des artisans, néanmoins cela va nous guider pour la suite.

## Modèle de manivelle
_Via 3D printing_

![image en ligne](images/image22.JPG)

On choisit de créer un modèle de manivelle via l'imprimante 3D, histoire de profiter de la précision de la machine (si pas d'erreur d'impression) et d'obtenir des éléments fonctionnant systématiquement.
Plus facile à dire qu'à faire, les pas de vis en impression ne sont pas toujours les plus précis, néanmoins ça fonctionne relativement bien.

**On obtient la première partie de notre second module nécessaire.**

## Modèle de bobine
_Fait main_

![image en ligne](images/image25.JPG)

On crée une bobine pour stocker le fil plastique, on attache simplement deux bouchons ensemble, pas si simple.
Que ce soit par colle chaude, ou encore l'enrouler de papier collant, il arrive encore aux deux parties de se séparer si la force présente est trop forte ou si le système finit par bloquer.

**On obtient la seconde partie de notre second module nécessaire.**

## Prototype 6
_Modèles fixés sur planche en bois_

![image en ligne](images/image27.jpg)

On effectue quelques modifications sur le prototype 2, il reste le plus performant de ceux utilisés jusque-là, et le plus simple d'utilisation.
On y augmente la hauteur de découpe pour davantage de gabarits disponibles, on y ajoute une barre de graduation, on augmente la hauteur du support,...

On l'associe ensuite à un premier modèle de manivelle, on constate que la dite manivelle, bien qu'elle soit dans le même sens que le fil, n'est pas pratique à l'usage.
Que ce soit son support, trop fragile, ou encore la difficulté à tourner le dispositif, moins pratique que de tirer donc peu d'intérêt pour ce test.

![image en ligne](images/image28.JPG)

![image en ligne](images/image30.JPG)

Le second modèle de manivelle est plus résistant et fonctionne mieux, juste quelques soucis avec la bobine si elle se sépare en deux pendant qu'on tire.
Le modèle fonctionne dans tous les cas, et c'est très pratique de pouvoir directement récupérer le fil plastique stocké sur une bobine.

## Artisanat et possibilités

![image en ligne](images/image34.jpg)

Suivant les gabarits de fils plastiques, on peut créer plein de différents objets, ...
On peut jouer sur les propriétés thermiques du plastique, on peut créer des éléments rigides et resistants, ...

L'idée étant de diffuser ce principe de découpe aux cubains et qu'ils profitent de toutes leurs compétences d'artisanat pour créer la palette d'objets dont ils ont besoin.

![image en ligne](images/image35.jpg)

Test de tressage de petit tabouret en bois.

## Conclusion 
_Plein d'essais, de petites réussites et de ratés!_

![image en ligne](images/image33.jpg)








