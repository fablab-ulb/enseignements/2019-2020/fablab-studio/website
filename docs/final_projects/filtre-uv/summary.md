# UV Filter

# English - Summary

<img src="../images/IMG_4764.jpg" width="40%">

## Problematic

In Cuba, water comes into homes, contaminated with bacteria and viruses. Currently, citizens are boiling water to sterilize it. However, kerosene is limited and boiling water frequently causes burns in children.

## Question

Is there an "accessible" way for Cubans to make a durable UV-C filter, powered by an independent electrical source that can sterilize water without boiling it?

## Hypothesis

In this project, the hypothesis would be to filter running water from Cuba, with a portable UV-C filter and easily connectable to pvc bottles.

## Our goals

The UV-C sterilization technique is a practice already used in treatment plants in many cities (such as Edmonton), in hospitals (sterilization of equipment), as well as in charities such as “Viva con agua” or “Gelsenwasser” in Nepal. But there are also other means such as the "Steripen", or "pond filters" which treat germs present in the water.


These practices all work the same way. UV-C waves generate a germicidal effect on bacteria and viruses. These are always emitted by artificial sources, such as for example UV-C halogen lamps.


The primary objective is to set up a practical tool, which can be manufactured by any person (having access to a Fablab). To do this, we studied the functioning of objects already present in society as mentioned above, the pond filter. To support our understanding, scientific research is directed towards the techniques practiced in particular in Germany and France.


Once all the information had been gathered, the next step was to identify the most affordable standard parts available worldwide that can be imported into Cuba.


At the current stage, we have set up a hypothesis and an experimental tool, which meet the criteria mentioned above. To make this tool, several elements are necessary such as a UV-C halogen lamp (12V 9W), a quartz tube (30cm), a sealing ring (d-5cm), a socket (g23) adaptable for the lamp and a battery combined with a 5V to 12V transformer (see up to 20V). The set would come back at an estimated cost of 35 € (excluding delivery costs) Note that for 3D printing, care must be taken to use ASA plastic, which is UV resistant.

--------------------------------

--------------------------------

# Francais - Résumé

## Problématique

À Cuba, l’eau arrive dans les maisons, contaminée par des bactéries et des virus. Actuellement, les citoyens bouillent l’eau afin de la stériliser. Cependant, le kérosène y est limité et l’eau bouillante provoque fréquemment des brulures chez les enfants.

## Question

Existe-il un moyen « accessible » aux cubains, pour fabriquer un filtre à UV-C durable, alimenté par une source électrique indépendante pouvant stériliser l’eau sans la bouillir ?

## Hypothèse

Dans ce projet, l’hypothèse serait de filtrer l’eau courante de Cuba, avec un filtre UV-C portable et facilement connectable à des bouteilles en pvc.

## Objectifs

La technique de stérilisation par UV-C, est une pratique déjà employée dans des stations d’épuration de nombreuses Villes (telle que Edmonton), dans les hôpitaux (stérilisation de matériel), tout comme dans des associations caritatives comme “Viva con agua” ou “Gelsenwasser” au Nepal. Mais il existe également d'autres moyens comme le “Stéripen”, ou des “filtres à étangs” qui traitent les germes présents dans l’eau.


Ces pratiques fonctionnent toutes de la même manière. Les ondes UV-C engendrent un effet germicide sur les bactéries et les virus. Celle-ci sont toujours émises par des sources artificielles, comme par exemple des lampes halogènes UV-C.


L’objectif premier, est de mettre en place un outil de pratique, pouvant être fabriqué par n’importe quelle personne (ayant accès à un Fablab). Pour ce faire, nous avons étudié le fonctionnement des objets déjà présent dans la société comme cité plus haut, le filtre à étang. Pour appuyer notre compréhension, des recherches scientifiques se sont dirigées vers les techniques pratiquées notamment en Allemagne et France.


Une fois toutes les informations recueillies, l’étape suivante fut de répertorier des pièces standards les plus abordables possible, accessibles partout dans le monde et qui peuvent être importées à Cuba.


Au stade actuel, nous avons mis en place une hypothèse et un outil expérimental, qui répondes aux critères précédemment évoqués. Pour réaliser cet outil, plusieurs éléments sont nécessaires comme une lampe halogène UV-C (12V 9W), un tube en quartz (30cm), un anneau d’étanchéité (d-5cm), une douille (g23) adaptable pour la lampe et une batterie combinée avec un transformateur de 5V en 12V (voir jusqu’à 20V). L’ensemble reviendrais à un cout estimé à 35 € (hors frais de livraison) Notons que pour l’impression en 3D, il faut veiller à utiliser du plastique ASA, qui résiste aux UV.

--------------------------------

--------------------------------

# Spanish - Resumen

## Problemática

En Cuba, el agua llega a los hogares, contaminada con bacterias y virus. Actualmente, los ciudadanos están hirviendo agua para esterilizarla. Sin embargo, el queroseno es limitado y el agua hirviendo con frecuencia causa quemaduras en los niños.

## Pregunta

¿Existe una forma "accesible" para que los cubanos hagan un filtro UV-C duradero, alimentado por una fuente eléctrica independiente que pueda esterilizar el agua sin hervirla?

## Asunción

En este proyecto, la hipótesis sería filtrar el agua corriente de Cuba, con un filtro UV-C portátil y fácilmente conectable a botellas de PVC.

## Nuestros objetivos

La técnica de esterilización por UV-C, es una práctica ya utilizada en estaciones de purificación de muchas ciudades (como Edmonton), en hospitales (esterilización de material), como en asociaciones benéficas como "Viva con agua" o "Gelsenwasser" en Nepal. Pero también hay otros medios como el "Steripen" o "filtros de estanque" que tratan los gérmenes presentes en el agua.


Todas estas prácticas funcionan de la misma manera. Las ondas UV-C generan un efecto germicida sobre bacterias y virus. Estos siempre son emitidos por fuentes artificiales, como por ejemplo lámparas halógenas UV-C.


El objetivo principal es configurar una herramienta práctica, que pueda ser fabricada por cualquier persona (que tenga acceso a un Fablab). Para hacer esto, estudiamos el funcionamiento de los objetos ya presentes en la sociedad como se mencionó anteriormente, el filtro de estanque. Para apoyar nuestra comprensión, la investigación científica se dirige hacia las técnicas practicadas en particular en Alemania y Francia.


Una vez que se recopiló toda la información, el siguiente paso fue identificar las piezas estándar más asequibles disponibles en todo el mundo que se pueden importar a Cuba.


En la etapa actual, hemos puesto en práctica una hipótesis y una herramienta experimental, que cumplen con los criterios mencionados anteriormente. Para hacer esta herramienta, se necesitan varios elementos, como una lámpara halógena UV-C (12V 9W), un tubo de cuarzo (30cm), un anillo de sellado (d-5cm), un casquillo (g23) adaptable para la lámpara y una batería combinada con un transformador de 5V a 12V (ver hasta 20V). El conjunto volvería a un costo estimado de 35 € (excluyendo los costos de envío) Tenga en cuenta que para la impresión 3D, se debe tener cuidado al usar plástico ASA, que es resistente a los rayos UV.
