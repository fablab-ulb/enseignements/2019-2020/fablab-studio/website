# Filtre-UVC

## Processus de recherche

### 1. Renseignement sur les rayon UV dans l'eau. Ce que l'on en a appris.


> **A. Le traitement de l’eau par UV, une méthode efficace pour éliminer les micro-organismes**

- Technique qui nécessite une eau claire en amont.

- Les eaux de surface peuvent être traitées par les UV pour en éliminer les micro-organismes pathogènes.

- Les eaux résiduaires, qui véhiculent tout une variété de bactéries et de virus plus ou moins dangereux pour la santé publique, peuvent également être traitées par UV.


**À quel usage pourront être destinées les eaux traitées par UV?**

- On peut traiter par UV les eaux destinées à la potabilisation, les eaux de process pour la chimie, la pharmacie, dans l’agroalimentaire, les eaux à embouteiller, les eaux de piscine, mais aussi les eaux qui peuvent être réintégrées dans les nappes phréatiques ou encore rejetées dans le milieu naturel… Partout où une eau doit être désinfectée, les UV peuvent être utilisés.


|    |    |
| --- | --- |
| ![image en ligne](images/courbe-dinactivation.jpg) | <ul></li>Courbe d’inactivation des micro-organismes lors d’un traitement UV. C’est avec une longueur d’onde de 254 nm, que les lampes du traitement UV sont les plus efficaces. <li></li>Parmi les germes pathogènes présents dans les eaux usées, on trouvera principalement : les salmonelles, les staphylocoques, les pseudomonas, les campylobacters, les clostridium perfringens et botulinium, le b.cereus, les shigelles, les légionelles, le virus de la poliomyélite, la listeria ainsi que bien d’autres virus. Le traitement UV permettra d’éliminer efficacement ces micro-organismes pathogènes. |

**Les conditions requises pour la désinfection par UV**

Les principaux paramètres à prendre en compte sont :

- Il faut évaluer la transmittance de l’eau (c’est la transparence de l’eau au rayonnement UV émis à 254 nm).

- la couleur (plus une eau sera claire, plus le rayonnement UV pourra la traverser).

- sa turbidité (plus elle sera faible, moins le rayonnement UV émis sera freiné ou détourné de son chemin).

- la teneur en fer et en manganèse de l’eau, qui sont des sels métalliques pouvant précipiter sur les gaines protectrices des lampes.

- la teneur en matières organiques, qui peut absorber la lumière UV à 254 nm.

- le caractère plus ou moins entartrant de l’eau.

**Comment agit ce traitement par UV**

- Les UV agissent efficacement sur la plupart des micro-organismes (tous ceux qui ont un ADN ou un ARN – bactéries, virus, protozoaires etc.), mais avec des doses UV différentes, car leur sensibilité (ou leur résistance) diffère.


**Mécanisme de l’irradiation : effet de l’irradiation UV sur l’ADN**

- Les UV agissent sur l’ADN ou l’ARN des micro-organismes, en modifiant le nucléotide appelé thymine, l’une des quatre bases azotées des micro-organismes. Une fois cette modification effectuée avec la production d’un dimère, le micro-organisme ne peut plus se reproduire et il meurt. La dose UV ou fluence est le paramètre de dimensionnement d’une installation UV. C’est le produit de l’intensité émise par les lampes, multiplié par le temps de contact avec ce rayonnement, soit : intensité UV x temps de contact Watt/m² x seconde = Joule/m². En France, cette dose UV ou fluence doit être de 400 J/m² (400 Joule/m² = 40 mJoule/cm² = 40,000 uWatts/cm²) et dans ces conditions, on respecte les critères microbiologiques de potabilisation des eaux.

GENOUD E., "Le traitement de l’eau par UV, une méthode efficace pour éliminer les micro-organismes" [en ligne], le journal des fluides, Mai-Juin 2010, [Consulté le 19 novembre 2019].
Disponible sur : <http://www.lejournaldesfluides.com/actualite/le-traitement-de-leau-par-uv-une-methode-efficace-pour-eliminer-les-micro-organismes/>

-------------------------------------------

> **B. Les bienfaits des U.V.C.**

Sachez qu'il faut se protéger des UV notamment en les enfermant dans un tube en verre (il empêche les U.V. de passer) lui même emballé dans un papier aluminium (pour protéger les yeux car ils restent cependant dangereux pour eux).

Certains paramètres peuvent influencer efficacité germicide de l'UV sur l'eau:

- la température: pour obtenir une efficacité optimale il faut une eau à 42°C. Si on l'utilise en aquariophilie où la température varie entre 22 et 30°C, l'efficacité est réduite à environ 75 %.

- la quantité à traiter: cette quantité dépend de l'épaisseur de l'eau à traiter. Une épaisseur de 0,5 à 1 cm donne une perméabilité proche de 100 %.

PATETE S., "Les bienfaits des U.V.C."" [en ligne], [Consulté le 19 novembre 2019].
Disponible sur : <http://users.skynet.be/meeki/uv/uv.html>

-------------------------------------------

> **C. Le traitement de l'eau par rayonnement UV**

Principe de fonctionnement:

- Les rayons ultra-violets sont une onde électromagnétique et regroupent des fréquences oscillants entre 10 et 400 nm (10 nm étant la limite des rayons X et 400 nm la limite des radiations visibles).

- Ces radiations UV ont une action photochimique sur les corps, action qui se manifeste par des réactions très diverses telles que :
pigmentation de la peau (pour des longueurs d'onde UV-A comprises entre 315 et 400 nm), vitamination des produits alimentaires (pour des longueurs d'onde UV-B comprises entre 285 et 315 nm), destruction des micro-organismes (pour des longueurs d'onde UV-C comprises entre 200 et 280 nm),
formation d'ozone (pour des longueurs d'onde de l'ordre de 185 nm).

- L'action stérilisante, est due à la perturbation apportée par les radiations ultra-violettes dans la structure chimique des constituants de la cellule vivante, et par suite, de leur fonctionnement. La courbe d'adsorption de l'ADN (acide désoxyribonucléique), véritable support de l'information génétique dans le noyau des cellules, pour des longueurs d'onde comprises entre 200 et 285 nm met en évidence un pic à la longueur d'onde de 257 nm, c'est à dire un profond effet germicide à cette longueur d’onde.

- Suivant la quantité d'énergie UV reçue, la cellule vivante sera soit stérilisée (effet bactériostatique) soit détruite (effet bactéricide). L'effet bactériostatique dans le cas d'une absorption modérée d'énergie UV, permet à la cellule de continuer à vivre, mais sans avoir la possibilité de se reproduire. Cette cellule est donc condamnée à disparaître. L'effet bactéricide, dans le cas d'une absorption d'énergie supérieure à une certaine dose, permet la destruction de la cellule. La dose minimale légale selon la circulaire du 19/01/87 de la Direction Générale de la Santé est de 25 000 micro watt seconde par centimètre carré (mWsec/cm2).

**Avantages et inconvénients**

- L'avantage le plus intéressant est que la désinfection s'accompagne de la formation d'aucun produit de réaction avec les matières organiques de l'eau

- Système adaptable sur un circuit de distribution d'eau déjà en place

- Nécessite que peu d'entretien et son coût de fonctionnement est relativement bas`

Disponible sur : <https://www.emse.fr/~brodhag/TRAITEME/fich19_1.htm> [Consulté le 19 novembre 2019].

----------------------------------------------------

### 2. Recherche sur les matériaux (quartz, ampoule)

> Le quartz

Ce matériau est une espèce minérale du groupe des silicates, sous-groupe des tectosilicates, composé de dioxyde de silicium, ou silice, de formule chimique SiO₂, avec des traces de différents éléments tels que Al, Li, B, Fe, Mg, Ca, Ti, Rb, Na, OH.
Il se présente sous la forme ou bien de grands cristaux incolores, colorés ou fumés, ou bien de cristaux microscopiques d'aspect translucide.
Constituant 12 % (en masse) de la lithosphère, le quartz est le minéral le plus commun (l'oxygène et le silicium sont respectivement les premier et deuxième constituants, par ordre d'importance, de la lithosphère) ; c'est un composant important du granite, dont il remplit les espaces résiduels, et des roches métamorphiques granitiques (gneiss, quartzite) et sédimentaires (sable, grès).

"Quartz (minéral)" [en ligne], Wikipédia, modifié le 1 décembre 2019, [Consulté le 19 novembre 2019], Disponible sur : <https://fr.wikipedia.org/wiki/Quartz_(minéral)>


Au fur et à mesure de nos recherches, nous constatons que l'utilisation de lumière UV-C dans la stérilisation de l'eau est chose connue. Nous avons pu faire la rencontre du "Steripen": Petit tube avec une lumière UV avec lequel on touille dans l'eau durant une certaine durée pour la purifier. Ou encore du "Lifestraw": qui ici présente le même principe mais dans une gourde.

|    |    |
| --- | --- |
|  ![image en ligne](images/steripen.jpg) | ![image en ligne](images/Lifestraw.jpg)
| Site: <https://www.katadyn.com/fr/fr/45001-AQUA-MP-Aqua> | Site: <https://www.lifestraw.com>  |

Une fois une ampoule UV de 9W 12V trouvée, une comparaison fut réalisée entre notre ampoule avec les produits cité plus haut. Avec une rapide calcul, nous constatons que l'utilisation de notre projet de filtre à UV, serait nettement plus efficace.

|    |    |
| --- | --- |
|  ![image en ligne](images/ampoule.jpg) | ![image en ligne](images/calcul.jpg) |


-------------------------------

### 3.  Recherche du modèle

|    |    |
| --- | --- |
| ![image en ligne](images/Teatox.jpg) | Nous nous sommes inspirés de la gourde Teatox. Celle-ci est composée d'un contenant lui-même fait de 2 couches de verres. Au bout de ceux-ci, on retrouve un filtre où placer le thé et enfin un bouchon.  
Site web: <http://www.teatox.fr> |

Premier test pour créer notre filtre à UV. Il s'agit de la chambre à UV. De part et d'autre du modèle, serait visser deux bouteilles d'eau, l'une vide et l'autre remplie.  
<p>Une fois l'ampoule UV allumée, par un mouvement de rotation, l'eau dans la bouteille pleine du haut, se déverserait dans celle vide en bas. <p>

|    |    |
| --- | --- |
|  ![image en ligne](images/Croqui.jpeg)  |  ![image en ligne](images/impression-1.jpg)  |

Au fur et à mesure de nos recherches, notre volonté fut d'augmenter la quantité d'eau filtré. Sachant qu'un ménage cubain consomme en moyenne 3L/jour d'eau. Si nous plaçons 4 bouteilles de respectivement 1,5L, le compte est bon.

|    |    |
| --- | --- |
|  ![image en ligne](images/croquis-2.jpeg)  |  ![image en ligne](images/impression-2.jpeg)  |

Tout projet travaillant avec de l'eau, comprend des problèmes d'étanchéité. Pour cela, nous avons réfléchis à diverses solutions. Deux en furent retenues : l'utilisation d'une **préservatif** ou de **vaseline**.
<p>En effet, l'anneau de la capote sera utilisé pour étanchéifier le tube de quartz se trouvant dans la chambre. Ensuite la vaseline étant hydrophobe, ne se mélange pas avec l'eau et la repousse. <p>

|    |    |
| --- | --- |
|  ![image en ligne](images/capote.jpg)  |  ![image en ligne](images/vaseline.jpg)  |

-------------------------------

### 4. Allimentation électrique

Nous entrons pour nous, dans la phase la plus compliquée du projet, la partie électrique. Un des premiers souhaits, est que le filtre puisse être mobile, transportable en cas de panne électrique.  
<p>C'est pourquoi, pour alimenter la lampe UV, le choix s'est dirigé vers une batterie externe. Pour cela, la présence d'un transformateur de 5V vers du 12V est nécessaire, car chaque port USB envois du 5V et l'ampoule UV a besoin de 12V. <p>

**1°: Verification du nombre de Watt et de Voltage**

|    |    |
| --- | --- |
|  ![image en ligne](images/verification-voltage.JPG)  |  ![image en ligne](images/verification-ampere.JPG)  |

**2°: Essais 1. Avec une machine qui décide voltage et watt**

|    |    |
| --- | --- |
| ![image en ligne](images/Machine.jpg) | Avec l'aide de notre collègue Jeremy et d'une machine. Celle-ci servant à contrôler le nombre de watt et de voltage envoyé dans un élément électrique. <p>Malheureusement, le test ne fut guère satisfaisant, aucun résultat obtenu. <p>  |

**3°: Essais 2. Sur notre courant électrique de 220V**

|    |    |
| --- | --- |
| ![image en ligne](images/220V.png) | Toujours avec Jeremy, l'objectif ici était de tester de faire fonctionner l'ampoule directement sur notre courant électrique. La fatalité arriva, elle a grillé instantanément. <p>Après s'être renseigné auprès d'une boutique d'électricité professionnelle, le vendeur nous explique que nous avons besoin d'un balast pour lancer l'ampoule. <p>  |

**4°: Essais 3. Utilisation d'un balast d'une lampe de secours.**

|    |    |
| --- | --- |
| ![image en ligne](images/lampe-secours.jpg) | Cette fois si, Axel qui est électronicien au sein du fablab, est venu à notre rescousse et restera à nos côtés durant toute la suite des recherches. Sur base d'une ancienne lampe de secours, fut extrait le balast pour le connecté à l'ampoule. Plusieurs essais de connexion furent testées mais rien n'y fait. <p>Un autre essais fut fait en extrayant une plaque électrique d'un écran. Seule une infime lueur fut obtenue. <p>  |

**5°: Essais 4. création de notre propre balast.**

|    |    |
| --- | --- |
| ![image en ligne](images/shéma-electrique.png)  |  L'essais suivant consisté à créer notre propre balast sur base d'un schéma électrique tiré d'internet et remodifier par Axel. Avec ceci, il a fallu se munir d'une série de composant.  
| ![image en ligne](images/impression-plaque-cuivre.jpg)  |  Grace a la CNC a métaux, le schéma électrique fut imprimé sur une plaque de cuivre. La machine découpe les contours, grave les liaisons et perce les trous pour y insérer les composants.   
| ![image en ligne](images/composants.jpg)  |  Une fois la plaque de cuivre obtenue, il a fallu poncer très légèrement sa surface cuivrée avec une papier ponce a grain fin et mouiller pour enlever l'oxydation. <p>L'étape suivante consista à placer les composants et les souder 1 par 1. <p>
| ![image en ligne](images/soudure.JPG)![image en ligne](images/soudure-2.jpg) | Ce ne fut pas chose facile de faire de belles soudures. Les premières n'étaient pas très propres mais étaient fonctionnelles comme on dit ! Une fois tous les branchements effectuer, l'ampoule ne s'allume toujours pas...  <p>Nous avons essayé de créer une bobine par nous-même en comptant le nombre de tour. 30 tours, 4tours et 250 tours, mais rien ne changea. <p>  |

**6°: Essais 5. Utilisation d'un Steup converter.**

|    |    |
| --- | --- |
| ![image en ligne](images/stepup-converter.jpg)  |  Etant donné que tous nos essais précédents n'apportaient pas entière satisfaction. L'achat d'un Step Up Converter fut effectué. Celui-ci permet de booster le voltage du système électrique.   
| ![image en ligne](images/Balast-2.jpg)  |  Ce balast fut utilisé dans un de nos précédent essai. C'est le seul qui ai donné un résultat (même s'il n'était pas fort visible). L'assemblage de lui avec le Step Up devrait être suffisant.   
| ![image en ligne](images/reglage-voltage.jpg)  |  Axel étant toujours avec nous, possédait une petite machine pour pouvoir calibrer le voltage transmit par notre Step Up Converter, car une des composantes du balast supportait un voltage de maximum 25V. Une fois l'ensemble calibré...
| ![image en ligne](images/lumiere.jpg) | **Et la lumière fut !** <p>Il aurait peut-être fallu fournir un plus grand voltage pour que la totalité de la lampe s'allume, mais pour des raisons de sécurité nous avons fait le choix de se limiter à 24V. <p> |


*Le temps met tout en lumière. - Thalès*
