# UV-C filter

## Tutorial

### 1. Explanation of the filter:

The filter being made of plastic (ASA), UV will have no consequence on the UV-C chamber. Plastic bottles (PET), by their necks, are easily attached to the filter. The water in the 2 upper bottles flows using gravity into the UV-C chamber, where it is treated by rays. Thereafter it flows, still using gravity, into the lower (empty) bottles. The filtration procedure takes approximately 90 seconds thanks to the orifices of different sizes and can be repeated as desired.

![image en ligne](images/11.jpg)

**Important information to know:**

- The clearer and more translucent the water to be filtered, the better UV-C can reach and kill bacteria.

- The filter is designed to pass 1L of water in + -90 seconds. It can therefore be connected with 1L, 1.5L or 2L bottles. However, it is important to use 4 bottles of the same volume. This is so that the water, once it has run out, is balanced in the bottles at the bottom and does not overflow. The bottleneck system of the lower bottles contains an air inlet and is therefore not 100% waterproof.

- In order to obtain better flow and a similar amount of water in the bottles, it is advisable to place the filter on a flat surface.

- It is not recommended to fill the bottles to the brim, as there is a risk of water leakage due to the air intake gap.

- UV-C does not pass through the plastic (PET) of the bottles. Only, they are highly mutagenic and attack the tissues of the eyes and skin! It is important to turn on the lamp when you are not in direct contact with the waves (skin and eyes).

- The filter must be printed with ASA, which is UV resistant.

- The filter only works in one direction. If you want to perform a second filtering with the same water in the same bottles. The bottles must be inverted to re-perform the filtering.

------------------------------

### 2. Parts to buy:

|    |    |
| --- | --- |
| ![ampoule](images/ampoule.PNG) | - bulb UV-C Söll   (9W 12V)    (+- 18 Euro) |
| ![Quartz](images/quartz.PNG) | - Quartz                        (+- 4  Euro) |

**Necessary tools:**

- Condom or plastic bag

- Vaseline or greasy substance: Hydrophobic

- Powerbank

- Cables + Cutter + mechanical tools

- 4 identical plastic bottles (PET)

------------------------------

### 3. Files to download:

[P1](STL/P1.stl)  --- [P2](STL/P2.stl) ---  [P3](STL/P3.stl) ---  [P4](STL/P4.stl)

![axo1](images/axo1.png)

**P1 - 4 functions:**

1: There is an interior space which can contain the ballast and the stepup converter.

2: There is a socket that holds the bulb horizontally, followed by two 4mm diameter holes. These openings allow the electrical cables to pass.

3: The exterior flat surface has the function of maintaining, stabilizing and subsequently fixing the "Powerbank" on the filter.

4: By screwing the P1 into P2, it compresses the quartz against the seal, and seals the UV chamber.

**P2 - 3 functions:**

1: The left side has the same diameter as the quartz. It therefore holds the quartz so that it does not float in the UV chamber.

2: The second diameter (larger), is created an opening of 5mm between the wall and between the quartz. It is in this space that water circulates.

3: The holes (upper and lower) have different shapes and sizes. The upper opening is round and of a maximum possible diameter. This way the water enters the UV chamber, even with a pressure under the bottle. The lower opening is oval (3mm x 6mm). The mechanism holding the PET bottle contains a second small round hole 2mm in diameter. This air intake guarantees a constant water flow of 90 seconds.

**P3 - 2 functions:**

1: P3 connects with P2 and P4. (distance imposed because of the size of the quartz)

2: overall, P3 offers good hand support for handling the object during the operation.

**P4 - 2 functions:**

1: on the left side of the axonometry of P4, there are supports for quartz, to avoid bending.

2: Otherwise functions identical to P2

------------------------

### 4. The assembly of the object:

|    |    |
| --- | --- |
| ![chambre_2sur3](images/chambre_0sur3.jpg) |<p> 1: You must take parts P2, P3, P4 and remove all the support structures after 3D printing. <p> <p> 2: With petroleum jelly or another hydrophobic substance, the screw systems must be coated to make them waterproof. <p> <p> 3: It is advisable to screw and slightly unscrew the parts before coating them with petroleum jelly, in order to adjust the parts as well as possible. <p> <p> If two pieces are caught in one another. Use a little soap with warm water to help the plastic expand. But be careful, because human force can also distort the printed parts. <p> |

|    |    |
| --- | --- |
| ![chambre_2sur3](images/chambre_2sur3.jpg)![chambre_et_vaseline](images/chambre_et_vaseline.jpg) |<p> 1: Once the parts have been screwed on, the excess petroleum jelly must be removed. <p> <p> 2: As soon as the 3 parts are assembled, pay attention that the "plug heads" are correctly aligned. Make sure the same holes are placed on the same side. <p> |

|    |    |
| --- | --- |
| ![Quartz](images/quartz_et_préservatif.jpg) |<p> 1: To seal the space between the quartz and the chamber, take a condom with the end cut off. The ring of it must be placed at the end of the curved edge of the quartz. <p> <p> (A small rolled up plastic bag also works -> under pressure) <p> <p> 2: Quartz is a material allowing UV-C to pass. To be as efficient as possible, it must be as clean as possible. Advice: clean it with strong alcohol to increase its yield. <p> |

|    |    |
| --- | --- |
| ![etanchéité](images/chambre_et_preservatif.jpg) ![detail](images/detail_pression.jpg) |<p> 1: Once the quartz has been properly cleaned, it can be gently inserted into the UV chamber. <p> <p> 2: Piece P1 will then press the quartz against the condom and the shell. <p> <p> 3: The part P1 also functions as a socket. It will hold the bulb horizontally in the quartz. <p> |

|    |    |
| --- | --- |
|![2](images/2.jpg)![3](images/3.jpg)![4](images/4.jpg)|1: The cables must be passed through the holes provided, and connect them to the bulb. <p> <p> 2: The bulb can then be fixed in the socket. <p> |

|    |    |
| --- | --- |
| ![5](images/5.jpg)  | 1: P1 can then be screwed in with P2. |

|    |    |
| --- | --- |
| ![13](images/13.jpg) |<p> 1: The last module in red is dedicated to electrical storage. <p> <p> 2: The flat side on the outside is dedicated to the powerbank. This design helps stabilize and secure each powerbank with a rope or other means of attachment. <p> |

|    |    |
| --- | --- |
| ![8](images/8.jpg) | 1: When the 4 parts are correctly assembled, it is then that two PET bottles, empty and sterile, can be screwed starting with the narrow opening. |

|    |    |
| --- | --- |
|  ![9](images/9.jpg)![10](images/10.jpg) |1: Once the 2 empty bottles are screwed on, the 2 bottles filled with unsterilized water can be placed on the table. You must therefore lift the filter with one hand and screw the bottles with the other hand. <p> 2: It is important not to screw with too much pressure, so as not to damage the fixing systems. <P> |

|    |    |
| --- | --- |
| ![12](images/12.jpg)   | 1: The filter is now ready for use. <p> To activate the filtration, you must turn on the UV-C light, take the filled bottles with both hands, turn the object over 180 degrees. The water can then flow using gravity, from one bottle to the other. <p> |

------------------------



### 5. Electronic circuit:

This filter works with a power bank emitting 5V. To operate the 12V bulb, we have to increase the voltage with a ballast and a step-up converter. It is also important to mention that the bulb works with alternating current!

It is important to emphasize that our final goal has not been fully achieved! Indeed, in the rest of this tutorial, you will see that we have recovered a ballast from a television screen in order to light the bulb. Only the one if forced us to be limited in voltage, which resulted in half a light.

|    |    |
| --- | --- |
| ![18](images/18.jpg)   | 1: This ballast was found in a television screen. We have learned that it makes it possible to start the bulb using an electric pulse that ionizes the lamp. |

|    |    |
| --- | --- |
| ![17](images/17.jpg)   |  1: This Step-up helps the ballast to create more power. It boosts the voltage. |

|    |    |
| --- | --- |
| ![15](images/15.jpg)   ![19](images/19.jpg)|1: Front and rear panel |

|    |    |
| --- | --- |
| ![20](images/20.jpg) | 1: Here is the partially lit UV lamp. The more the lamp is on, the more you are sure that the filtration is working. <p> <p>**CAUTION!**<p> It is very important to wear glasses when the light is on. If your eyes meet the lights, it could be harmful to them. <p> |
