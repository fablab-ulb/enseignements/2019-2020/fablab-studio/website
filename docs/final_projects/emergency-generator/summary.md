# Générateur d'urgence 

### Summary

![generator](images/modele_fin.JPG)

### Problématique (FR) : 

- Chaque année à Cuba, la saison des ouragans entraîne des coupures de courant parfois prolongées (1 à 3 semaines) sur l’île
- D’après les personnes interrogées, un des gros problèmes lors des coupures est l’isolement dû au manque de communications avec l’extérieur 
- Peu de groupes électrogènes existent sur l’île, et le pétrole se faisant rares, ceux-ci sont d’autant moins accessibles

### Question : 

> Comment, avec le matériel trouvé sur l’île peut-on charger des téléphones portables en cas de panne d’électricité prolongée ? 

_Hypothèse :_

Le générateur d’urgence doit pouvoir répondre au contexte cubain, prendre peu de place, être gardé en intérieur en cas d’ouragan, et générer 5V et 1A (donc 5W), afin de charger des téléphones portables ou des powerbanks. 

Pour ce faire, nous proposons de fabriquer un moteur Stirling en matériaux de récupération, qui nécessite : une source de chaleur (on propose donc un bruleur à rhum), des aimants en néodyme (que l’on retrouve dans des vieux disques durs), des bobines de cuivre (que l’on peut récupérer dans des vieux micro-ondes par exemple) et des seringues en verre (qui existent à Cuba et peuvent également être récupérées). 

______________________

### Problematic (EN):

- Each year in Cuba, the hurricane season causes power cuts that can last from 1 to 3 weeks on the island
- According to interviewees, one of the big problems during the powercuts is the isolation people suffer from due to the lack of communications with the outside
- Few power diesel generators exist on the island, and with oil being scarce, these are not an option for every household

### Question:

> How, with the equipment found on the island, can we charge mobile phones in the event of a prolonged power outage?

_Hypothesis :_

The emergency generator must be able to respond to the Cuban context, take up little space, be kept indoors in the event of a hurricane, and generate 5V and 1A (therefore 5W), in order to charge cell phones or powerbanks.

To do this, we propose to make a Stirling engine out of recycled materials, which requires: a heat source (we therefore offer a rum burner), neodymium magnets (found in old hard drives), copper coils (which can be recovered in old microwaves for example) and glass syringes (which exist in Cuba and can also be recovered).

______________________


### Problemática (ES) :

- Cada año en Cuba, la temporada de huracanes conduce a cortes de energía a veces prolongados (1 a 3 semanas) en la isla
- Según los entrevistados, uno de los grandes problemas durante los recortes es el aislamiento debido a la falta de comunicación con el exterior.
- Existen pocos generadores en la isla, y debido a la escasez de petróleo, estos son menos accesibles

## Pregunta :

> ¿Cómo, con el equipo que se encuentra en Cuba, se puede cargar teléfonos móviles en caso de un apagón prolongado?

_Suposición:_

El generador de emergencia debe ser capaz de responder al contexto cubano, ocupar poco espacio, mantenerse en el interior en caso de huracán y generar 5V y 1A (por lo tanto, 5W), para cargar teléfonos celulares o bancos de energía.

Para hacer esto, proponemos hacer un motor Stirling con materiales reciclados, que requiere: una fuente de calor (por lo tanto, ofrecemos un quemador de ron), imanes de neodimio (que se encuentran en los discos duros viejos), bobinas de cobre (que se pueden recuperar en microondas viejas, por ejemplo) y jeringas de vidrio (que existen en Cuba y también se pueden recuperar).

<iframe width="560" height="315" src="https://www.youtube.com/embed/nOqqSy_ClCY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
