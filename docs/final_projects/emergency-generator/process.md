# Processus de recherche 

Nous avions commencé notre semestre avec une envie simple, qui était de produire de l'énergie. Pour ce faire, nous avions eu comme idée de fabriquer une éolienne, qui puisse alimenter les foyers cubains en électricité, leur île dépendant des énergies fossiles et du pétrole pour produire de l'électricité dans des centrales thermiques. Après beaucoup de temps passé à bricoler, récupérer des pièces, faire des recherches sur le vent, l'électricité, le climat cubain et les besoins cubains, nous avons remis en question notre idée première, et avons changé notre ligne de mire pour essayer de fabriquer un moteur thermique, capable de recharger des téléphones en cas d'ouragan. 

# L'électricité à Cuba

L'électricité compte pour le tiers du budget mensuel d'une famille cubaine moyenne. Nous voulons les aider à produire leur propre électricité, et être plus autonomes dans leur consommation énergétique.

![image](images/facture.png)

Pour quantifier notre objectif, nous nous penchons sur les tenants et les aboutissants de la consommation en électriicté des ménages cubains. En effet, depuis près de 20 ans, l'électricité est facturée par palliers, pour inciter les usagers à réduire leur consommation. Ainsi, les 100 premiers kWh coûtent 9 pesos, et ssi la consommation atteint les 200 kWh, le prix passe à 44 pesos (5 fois plus). Dès 300 kWh, le prix passe à 114 pesos (11 fois plus). <br> Certains foyers doivent réduire leur consommation de manière draconienne, et évitent d'utiliser certains équipements énergivores (climatiseurs, plaques de cuissons...).

Nous nous fixons dès lors pour objectif de baisser la consommation des cubains en fabriquant une éolienne qui puisse fournir 100 kWh/mois, soit 3300 Wh/jour, représentant ainsi 30% de leur consommation d'énergie, mais 50% de leur facture.

# Phase 1 : Eolienne

## Récupération de matériel : 

Dans un premier temps, nous décidons de nous renseigner sur ce qui compose une éolienne, et essayer de récupérer des composants dans de vieux équipements électroménagers, pour être dans une dynamique de récupération et d'économie circulaire. 
On sait qu'il y a deux manières d'avoir un générateur d'éolienne :
<br> 1. Fabriquer notre propre générateur, en faisant tourner des aimants autour d'une bobine de cuivre.
<br> 2. Récupérer un moteur à aimants permanents ou un moteur pas à pas que l'on branche à l'éolienne.

Dans un premier temps, on désosse un micro-ondes pour voir ce qu'on peut récupérer dedans :

![image](images/recup_mat_1.png)

On récupère des bobines de cuivre et des aimants en graphite.
Les aimants en graphite ne sont pas très puissants, mais les bobines de cuivre peuvent nous intéresser. On décide de faire un stator avec les outils de fabrication numérique, pour commencer à faire des essais de rotation et voir ce que ça donne.

![image](images/Satator.jpg)

Pour avoir accès à plus de matériel de récupération, nous avions fait la connaissance d'un projet chouette, le SAS, porté par l'ERG et l'entreprise sociale CF2D, qui ont un local à Anderlecht, le Recy-K. C'est un projet où on répare plein d'objets déposés en déchetterie, où les employés trouvent leur travail grâce à des politiques de réinsertion sociale dans la Région de Bruxelles-Capitale. 

![montage de l'endroit du Recy-K et des bidules qui s'y trouvent](images/22_nov_2.png)

Nous y avons rencontré un monsieur à qui nous avons expliqué notre projet, et qui nous a dit que si on avait besoin d'aimants, il y avait des aimants en néodyme assez puissants dans les disques durs d'ordinateurs, et qu'on pourrait peut-être en récupérer pour faire notre rotor. Il nous donne 5 disques durs, que l'on s'empresse de désosser, pour récupérer une dizaine d'aimants. 

![montage de personnes qui démontent des disques durs](images/22_nov_1.png)

Une fois les aimants récupérés, on se penche sur l'éolienne en elle-même. On tombe sur un modèle existant, d'éolienne verticale. On avait décidé de faire une éoleinne verticale, entre une éolienne Daerius et une éolienne Savonius, qui a l'avantage de démarrer même en cas de vent faible, et s'adapte aux changements de direction du vent. L'objectif était de fabriquer une éolienne qui puisse être installée dans un contexte rural aussi bien que dans un contexte urbain.

![3D d'une éolienne verticale et des pièces nécessaires à sa fabrication avec les outils d'un FABLAB](images/eolienne1.png)

Nous faisons les calculs nécessaires de dimensionnement à partir de la loi de Betz, qui détermine la relation entre surface au vent, vitesse du vent et puissance résultante. Nous déterminons donc qu'il nous faudra 6 modules de 3 pâles, et commençons à les imprimer à l'imprimante 3D.

## Inspiration primaire : 

<iframe width="560" height="315" src="https://www.youtube.com/embed/e_sZ3tH_15E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Inspiration secondaire : 

<iframe width="560" height="315" src="https://www.youtube.com/embed/_d_H_goHZHg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Contraintes liées à l'éolienne

Après nous être bien lancés dans la récupération, le dimensionnement et la fabrication, nous nous sommes rendus compte que nous n'avions peut-être pas les compétences nécessaires pour mener le projet de l'éolienne à bien. En effet, la mise en place de celle-ci dans un contexte urbain, les faibles rendements dûs à la faible vitesse du vent et à l'instabilité de celui-ci faisaient qu'une éolienne verticale n'était pas une solution pereine et réaliste à la question de l'énergie à Cuba. 

_______________________________

# Phase 2 : Remise en question : 

Nous avons dû prendre le temps de repenser réellement à ce que signifie l'énergie à Cuba. Nous avons donc pris le temps de contacter des personnes cubaines, ainsi que le designer cubain Ernesto Oroza, et leur poser des questions sur ce qui leur pose problème en terme d'énergie sur l'île. 
Un problème récurrent était celui de l'instabilité du voltage. En effet, pour faire des économies, les centrales thermiques baissent parfois l'apport en électricité des ménages, causant ainsi des mini-coupures de courant, voire des coupures de courant assez prolongées, selon les jours. Pour que leur matériel électronique et électroménager n'en pâtisse pas, les Cubain.e.s utilisent des stabilisateurs de voltage, qui s'achètent à la sauvette un peu partout sur l'île. Nous ne savions pas quoi faire de cette information là.

![screenshots de conversations avec des personnes cubaines](images/conversations.png)

En outre, après cette phase de recherches, ce que nous avons décidé de traiter, c'est les situations d'urgence récurrentes, notamment les ouragans. On se souvient tou.te.s de l'ouragan Irma de 2017, dévastateur dans les Caraïbes, et notamment à Cuba, où certains foyers sont restés sans électricité jusqu'à 3 semaines. Les personnes contactées nous ont dit que l'un des gros problèmes en cas d'ouragan, c'était le manque de connectivité avec le monde extérieur. En effet, près de deux millions de Cubain.e.s vivent en dehors de Cuba, et il est essentiel pour les personnes présentes sur l'île de pouvoir communiquer avec leurs familles. 
La saison des ouragans forçant les populations à rester souvent calfeutrées chez elles, nous voulons fabriquer un générateur qui ne dépende pas des conditions extérieures, qui marche en intérieur, et qui puisse produire 5 volts et 1 ampère (donc 5 watts).
_______________________________

# Phase 3 : Moteur Stirling

Le principe fondamental d'une éolienne, c'est que c'est un générateur qui convertit de l'énergie mécanique en énergie électrique. Un moteur thermique, quant à lui, nécessite une étape de plus. Il convertit de l'énergie thermique en énergie mécanique, qui la encore est convertie en énergie électrique.  

## Inspiration : 

<iframe width="560" height="315" src="https://www.youtube.com/embed/s79odgWz6BM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Récupération de matériel : 

![collage du matériel utilisé](images/materiel.png)

Nous avons essayé d'utiliser le maximum de matériel de récupération.
<br> Les roulements sont issus d'un vieux skateboard et d'une vieille meuleuse.
<br> Les pièces de jonction entre les bielles et les pistons sont des vieilles bouteilles plastique.
<br> Les bobines de cuivre sont issues de vieux micro-ondes.
<br> Le tube de jonction entre les seringues est une simple durite de mobylette.
<br> Le bois est une simple planche de MDF de récupération.
<br> Les aimants que nous voulions utiliser initialement sont issus de vieux disques durs d'ordinateurs.


Les seuls éléments que l'on a choisi d'utiliser neufs sont les seringues en verre et les aimants en néodyme (que l'on avait acheté par le passé pour essayer de faire une autre éolienne, et qui avaient déjà bien servis).

## Améliorations suggérées du prototype : 

Le moteur que l'on essaie de répliquer est celui-ci, par les Incroyables Expériences sur [YouTube](https://www.youtube.com/watch?v=s79odgWz6BM). Ceux-ci cherchent juste à allumer une diode, et arrivent à produire au maximum 2,4 W. Nous essayons de produire 5W, et pour ce faire, nous nous inspirons de ce que l'on voulait produire pour le rotor et le stator qui composent le générateur d'une éolienne.

![modèle initial](images/modele.png)

Ainsi, sur le modèle initial, les Incroyables Expériences ne mettent qu'un seul aimant, sur la tranche d'une roue, sur une petite bobine sur le plateau du support, qui alimente une petite LED.
Nous augmentons significativement le nombre d'aimants, et mettons de multiples bobines entre les deux roues, bobines excitées des deux côtés par des aimants d'une taille conséquente. Aussi, la pièce de jonction entre les bielles et les pistons était très lourde sur le modèle des Incroyables Expériences, et nous avons changé celle-ci par un petit système assez frugal, avec deux petites bouteilles plastique, un peu de colle, une tige filettée, des écrous et des rondelles.

![modèle initial](images/modele_nous.png)

Aussi, nous avons essayé leur méthode, qui consiste à mettre de la paille de fer dans les seringues. Nous trouvions que celle-ci salissait beaucoup les seringues, et créait beaucoup de frottements. Aussi, pour essayer de garder la seringue chaude la plus chaude possible, nous ajoutons une petite installation au dessus de celle-ci, pour concentrer la chaleur sur tous les côtés de la seringue.

## Challenges liés à la fabrication : 

Lors du montage, un des principaux problèmes du moteur est sa tendance à être ultra sensible au moindre frottement. En effet, une seule petite poussière dans les seringues, ou un petit écart d'un millimètre dans le placement des bielles pouvait faire que celui-ci ne fonctionnait plus du tout. Nous avons passé beaucoup de temps à monter et redémonter le générateur, pour ajouter une petite couche de vernis sur une tige un poil trop petite, ou ajouter un petit peu d'huile sur un roulement. 

## Tests de la partie mécanique 

Pour améliorer l'efficacité du moteur et sa capacité à tourner nous avons :
<br> - ajouté un linge humide frais au dessus de la seringue froide, pour assurer un plus grand écart de température entre les 2 seringues, et donc une plus grande différence de pression (loi des gaz parfaits)
<br> - changé la jonction bielle/piston avec le système de la bouteille découpée
<br> - surélevé le bruleur à l'aide de câles
<br> - graissé les roulements avec de l'huile
<br> - desserré les bielles autour de l'axe des roues, et assurer plus de jeu
<br> - mettre une petite couverture au dessus de la seringue chaude pour la chauffer plus
<br> - fait des trous bien droits dans la matière pour que les tiges filettées ne soient pas desaxées

![modèle initial](images/modele_fin.JPG)

Sans aimants, le moteur finit par tourner très vite, frôlant les 200 tours/minute.
<iframe width="560" height="315" src="https://www.youtube.com/embed/eIrm4VTe-ic" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Tests de la partie électrique

### Etape 1 : Bobine 120 spires + aimants de DD

Notre premier essai, fin décembre, a été de faire tourner les aimants des disques durs avec une perceuse autour d'une grosse bobine de 120 spires. Nous n'avions pas réussi à dépasser les 0.2 V.

![modèle initial](images/1test_petitsaimants.jpg)

### Etape 2 : Bobine 120 spires + 4 aimants de 3*4 cm

Après avoir commencé à monter le générateur, nous avons réalisé des essais avec 2 aimants de 3*4 cm de part et d'autre de la grosse bobine de 120 spires. Les résultats étaient peu concluants, car on n'atteignait toujours pas 0,5V en sortie, même en faisant tourner le rotor avec une perceuse 1800 tours/min.

![modèle initial](images/grossebobine2aimants.jpg)

### Etape 4 : Bobine 120 spires + 8 aimants de 3*4 cm :

Pour essayer de booster le rendement, nous essayons de multiplier le nombre d'aimants par deux. Cela augmente un peu le voltage en sortie, le faisant passer au dessus de 0,5 V.

![modèle initial](images/grossebobine4aimants.jpg)

### Etape 4 : Bobine 120 spires + 8 aimants de 3*4 cm bien orientés

Toujours insatisfaits, on se rend compte que les aimants ne sont pas bien positionnés par rapport au stator. En effet, pour exciter le maximum d'électrons, le [manuel Piggott](https://www.tripalium.org/manuel) recommande d'alterner la polarités des aimants sur le rotor. En effet, si les aimants sont alternativement orientés au nord, au sud, au nord, et au sud, il y a plus de changements de polarités dans les bobines, et donc plus de courant électromagnétique en sortie. On arrive à dépasser les 1V en sortie, et à obtenir presque 1,5V, en faisant tourner le 

![modèle initial](images/grossebobine4aimantsbienorientes.jpg)

### Etape 3 : Bobine 50 spires + 8 aimants de 3*4 cm bien orientés

On décide d'essayer le même branchement avec une plus petite bobine, en se disant que peut-être que la bobine est trop grosse et les "électrons se perdent dedans". Avec une règle de trois, on décide de faire une bobine de 50 spires. Les résultats sont peu concluants, et on redescend en dessous d'1 V, même en faisant tourner le rotor avec une perceuse.

![modèle initial](images/grossemoyennebobines.jpg)

### Etape 4 : Bobine de cuivre fin + 4 aimants de 3*4 cm 

En suivant les conseils d'Axel, électronicien du FabLab de l'ULB, on décide de changer la bobine de nouveau, et d'utiliser des bobines avec du fil très très fin, que l'on a récupéré sur des ventilateurs de micro-ondes. Les résultats ne se font pas attendre, et on arrive à 1,5 V en faisant tourner le moteur manuellement à 100 tours/minutes.

![modèle initial](images/petitebobine2aimants.jpg)

### Etape 5 : Deux bobines de cuivre fin + 4 aimants de 3*4 cm bien orientés

En branchant les deux bobines de cuivre fin entre elles en série, et en utilisant les aimants en alternant leurs polarités, on arrive à obtenir 2,5 V en sortie.

![modèle initial](images/petitebobine2aimantsbonsens.jpg)

### Etape 6 : Quatre bobines de cuivre fin + 8 aimants de 3*4 cm bien orientés

En branchant les 4 bobines de cuivre fin entre elles en série, en disposant les aimants en alternant leurs polarités, on arrive à obtenir 3,5 V en sortie en faisant tourner le rotor à une fréquence de 100 tours/minute.

![modèle initial](images/petitebobine4aimantsbonsens.jpg)

### Etape 7 : Quatre bobines de cuivre fin + 8 aimants de 3*4 cm bien orientés + forte vitesse de rotation

En faisant tourner le moteur à une vitesse de près de 400 tours/minutes, on parvient à obtenir plus de 5V en sortie.

![modèle initial](images/petitebobine4aimantsbonsensvitesse.jpg) 

## Résultat obtenu 

<iframe width="560" height="315" src="https://www.youtube.com/embed/nOqqSy_ClCY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br> Nous sommes parvenus à fabriquer un moteur qui tourne à une vitesse atteignant les 180 tours/minute, et produit 1,5V. Néanmoins, celui-ci ne tournait pas lorsque l'on voulait y mettre 8 aimants, donc le maximum que l'on ai pu obtenir était avec 4 aimants, faisant ainsi baisser les rendements considérablement.
