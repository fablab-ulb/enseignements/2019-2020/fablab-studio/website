# Système constructif parasismique en terre crue - Processus de recherche


Nous avons souhaité travailler sur la thématique du logement. Avant d’élaborer un projet, nous devions en apprendre plus sur le logement cubain.

### Logement 

Pour cela, nous avons donc lu un ouvrage écrit par Mira Kongstein et Ernesto Oroza intitulé *Editing Havana - Stories of Popular Housing*, plus particulièrement le  chapitre “3 Houses” que nous a conseillé Ernesto Oroza. Ce livre présentait 3 maisons et les modifications qui y sont faites selon l’évolution de la famille (naissance, mariage etc.). Nous avons synthétisé le contenu dans un [powerpoint](/docs/final_projects/adobe-parasismique/files/Evolution_du_logement_cubain.pdf) 

- KONGSTEIN, Mira, OROZA, Ernesto, 2011, Editing Havana - Stories of Popular Housing    
Le chapitre “3 houses” est disponible [ici](https://drive.google.com/drive/folders/1fSOWkr7oiJ8mN-1OFIKkH-CGvaJikHzH?fbclid=IwAR2qlL68Nfip0OSsZk_ycx0XZbNYRYAuvLgZsc3Psmr-ZYDiK7aAFhFNhRc)

Nous avons constaté que les maisons sont essentiellement construites en parpaing mais que cela coûte cher. Il existe cependant un marché noir mais les prix restent tout de même assez élevé. Nous commençons alors à chercher un matériau de construction qui soit peu cher et disponible facilement à Cuba. Notre instinct nous dirige vers la terre crue.

### L’architecture en terre crue

Nous pensons que c’est une bonne idée de travailler avec ce matériau car c’est une ressource gratuite et disponible partout sur le globe. Nous entamons donc des recherches l'architecture de terre à Cuba ainsi que sur les différentes mises en oeuvre possibles de la terre crue (pisé, adobe, torchis, BTC,....) Nous optons pour l’adobe (briques de terre) car c’est la technique la plus facile à mettre en oeuvre dans le contexte de l’atelier qui, rappelons-le pose la question suivante: *Comment la fabrication numérique et le réseau des fablabs peuvent-ils aider à transformer la manière dont Cuba et ses habitants entrent en résilience?* En effet, le pisé et le torchis sont des techniques très artisanales qui ne demandent pas d’apport numérique. Au contraire de l’adobe ou des briques de terre comprimées (BTC), qui elles peuvent être “améliorées” grâce à l’outil numérique. Notre idée est de créer, à l’aide de l’outil numérique, une machine pour concevoir des briques de terre crue.

Nous consultons alors des ouvrages écrits par des spécialistes comme CraTerre ou Amaco, deux centres de recherches sur la terre crue.

![Bâtir en terre](images/Traité_de_construction_terre.jpg)

![Bâtir en terre](images/Batir_en_terre.jpg)

- GUILLAUD, Hubert, HOUBEN, Hugo, 2015. Traité de construction en terre      
Une aperçu de l'ouvrage est disponible [ici](https://www.editionsparentheses.com/IMG/pdf/p161_traite_de_construction_en_terre.pdf)

- FONTAINE, Laetitia, ANGER, Romain, DOAT, Patrice, HOUBEN, Hugo, VAN DAMME, Henri, 2009. Bâtir en terre : du grain de sable à l’architecture      
L'ouvrage est disponible dans son intégralité [ici](https://fr.calameo.com/read/00322162288fe243ce175)


- GAUZIN-MÜLLER, Dominique, SÉMON, Pauline, DOAT, Patrice, FONTAINE, Laetitia, GUILLAUD, Hubert, 2016. Architecture en terre d’aujourd’hui: les techniques de la terre crue.



### S’entourer d’experts

Après en connaître un peu plus sur le sujet, nous avons contacté des experts sur le sujet afin qu’il puisse nous aider sur nos questionnements. 
Premièrement nous avons contacté **Alba Rivero Olmos** qui est architecte, née à Cuba mais qui travaille en France pour l'organisme [CraTerre](http://www.craterre.org/). Elle nous a proposé de faire un appel téléphonique pour répondre à toutes nos questions. Nous en avons tiré beaucoup d’infos très intéressantes à propos de l’évolution des logements selon les périodes de colonisation à Cuba, à propos des techniques de mise en oeuvre de la terre crue largement utilisé avec les colonisations successives. Elle nous a également fait part des tremblements de terre qui peuvent avoir lieu au sud de l’île.    
  
![mail](images/mail_alba_1.png)
![mail](images/mail_alba_2.png)


Nous avons également contacté Mariana Correira, coordinatrice de ProTerra, l’association ibérico-américaine de l’architecture et la construction en terre crue du Programme de patrimoine mondial pour l’architecture de terre (WHEAP) fondé par l’UNESCO. Comme notre questionnement est spécifique à Cuba, elle nous a redirigé vers Duznel Zerquera Amador, un architecte qui vit et travaille à Cuba.  
    
![mail](images/mail_proterra_1.png)
![mail](images/mail_proterra_2_reponse.png)

### Problématique

Après quelques semaines de recherches nous remettons de l’ordre dans tout ce que nous avons appris depuis le début. Nous arrivons à la conclusion qu'il est nécessaire de trouver un matériau de construction gratuit ou du moins très peu cher et facile à mettre en oeuvre car il s'agit beaucoup d'auto-construction à Cuba. Nous arrivons donc à la problématique suivante: **Comment les constructions en terre, patrimoine oublié, permettent des procédés d’auto-construction frugal n’ayant nul besoin d’un recours au marché public ou noir ?** 

### Construction parasismique

Alba nous a parlé des séismes et cette problématique est importante à nos yeux. Les constructions doivent être durable, et comme l’île est exposé aux tremblements de terre, notre objectif est de construire un moule afin de fabriquer des briques de terre crue et d'en faire un système parasismique. 

Le *Traité de construction en terre crue* nous a particulièrement aidé grâce au [chapitre](/docs/final_projects/adobe-parasismique/files/Ch._Construction_parasismique.pdf) sur les constructions parasinistres. Nous avons pu améliorer nos connaissances sur l’origine des séismes ainsi que les répercussions sur les bâtiments.    

![mail](images/Chapitre_seisme_1.png)
![mail](images/chapitre_seisme_2.png)

Alba Rivero Olmos, notre contact chez CraTerre, nous a également transmis un [guide la construction parasismique](docs/final_projects/adobe-parasismique/files/guia_de_construccion_parasismica.pdf) qu’elle a écrit avec Wilfredo Carazas Aedo. Cet ouvrage écrit en espagnol nous informe de l’origine des séismes jusqu’aux détails constructifs en passant par l’analyse de la terre. De plus, il est très bien illustré par des schémas simples et clairs.    

![mail](images/effets_seismes.png)

Le *[Tutoriel](/docs/final_projects/adobe-parasismique/files/Adobe_Tutorial_English_Blondet.pdf) sur la construction des bâtiments en adobe résistant aux tremblements de terre* écrit par Marcial Blondet nous a permis d’évaluer les dommages causés aux bâtiments après un séisme ainsi que les solutions pour contrer cela via l’ajout de renforcements horizontaux et verticaux.   
  
![mail](images/tuto_seisme_1.png)
![mail](images/tuto_seisme_2.png)

### Premier prototype

Après toutes ces recherches sur la construction parasismique en terre, nous développons un premier prototype de moule. Dans un premier temps, nous souhaitons le réaliser de manière low tech car nous ne savons pas si tous les cubains auront accès à un Fablab pour y fabriquer le moule de manière high tech. Les cubains ont l’habitude de fabriquer beaucoup de choses avec leur mains et des matériaux de récupération, alors nous nous mettons dans les mêmes conditions. Nous fabriquons ce moule à l’aide de tasseau de bois et des canettes en aluminium. Des réserves sont créés afin d’accueillir les armatures en bambou 

![mail](images/moule_low_tech.pdf)


Comme nous l'avait conseillé la coordinatrice de ProTerra, nous contactons l’architecte cubain **Duznel Zerquera Amador** en lui faisant part de notre idée de fabriquer un moule.

![mail](images/mail_duznel_1.png)

> Traduction FR    
Bonjour Monsieur Zerquera,     
J'ai obtenu votre adresse par Mariana Correia. Nous sommes deux étudiantes de la Faculté d’Architecture La Cambre-Horta à l’ULB (Bruxelles, Belgique). Nous travaillons actuellement sur le logement à Cuba et nous aimerions proposer un projet de briques pour faire un système constructif parasismique. Nous aimerions fabriquer un moule pour faire des briques:      
D’une part, de manière low tech, avec les matériaux disponibles à Cuba (cf. schéma en pièces-jointes). Les cubains ont-ils accès facilement à des planches en bois de 41x4x1,5cm?     
D’autre part, à l’aide des outils numériques disponibles en Fab Lab. Nous avons pensé à fabriquer un moule à l’aide d’une fraiseuse numérique (CNC).     
Mariana Correira nous a dit que le type de terre détermine le type de matériau de construction à utiliser. Par exemple, un sol plus argileux signifie que l’on utilisera généralement des techniques d’adobe et de torchis. Un sol plus sableux signifie que CEB (ou BTC en français) sera utilisé.     
Nous aimerions savoir quel type de terre disponible? Est-elle argileuse? sableuse? siliceuse?     
Avez-vous des références actuelles en lien avec les techniques de terre spécifiquement à Cuba?    
Merci d’avance pour votre aide,    
Katia MOT & Hélène Menu,     
Etudiantes MA2, atelier Digital Fabrication Studio (ULB)

Malheureusement, nous n’obtenons pas de réponse de sa part avant la première présentation de notre projet.

### Pecha kucha 1

Voici donc la première [présentation](/docs/final_projects/adobe-parasismique/files/Pecha_kucha_1_-_Adobe.pdf) de notre projet sous forme de pecha kucha - [qu’est-ce qu’un pecha kucha?](https://fr.wikipedia.org/wiki/Pecha_Kucha) - ainsi que le [texte](/docs/final_projects/adobe-parasismique/files/Oral_6_12.pdf) pour une meilleure compréhension du diaporama. 


### Deuxième prototype

Après cette première présentation, nous remettons en question la forme du moule. En effet, lors de la fabrication des briques et du démoulage, il n’était pas facile de le démouler avec les angles créés par les réserves pour insérer des tiges servant d’armatures. Nous réfléchissons alors à une forme plus simple pour couler les briques tout en intégrant facilement les armatures en bambou. Nous dessinons un moule en forme de papillon afin d’intégrer ce système. 

![mail](images/brique_papillon.jpg)

### S’entourer d’expert

Duznel revient vers nous en s’excusant d’avoir mis du temps avant de nous répondre. Nous l'informons alors de notre changement de forme de moule.

![mail](images/mail_duznel_2.png)

> Traduction FR:
Salut Katia mot  
Je ne sais pas pourquoi je vois votre message si tard, mais ce sera un plaisir de coopérer avec vous. Si c’est toujours dans votre intérêt, je suis à votre disposition.   
Cordialement    
Duznel Zerquera

![mail](images/mail_duznel_3.png)

> Traduction FR
Bonjour M. Zerquera,    
Merci pour votre disponibilité.    
Pour notre projet, nous aimerions offrir un système de construction en pisé résistant aux tremblements de terre avec des cadres en bambou. Par conséquent, nous cherchons à créer un moule en adobe qui intègre plus facilement les structures en bambou grâce à la morphologie de la brique. Après avoir observé les différents moules qui existaient, nous partons de l'idée d'une brique en forme de papillon pour créer de simples réserves pour couler la terre et laisser suffisamment d'espace pour intégrer les structures tout en ayant un joint régulier (voir croquis) est-ce logique? On peut penser à faire des rayures sur les côtés des briques pour créer une meilleure adhérence avec le mortier d'argile. Que pensez-vous de cette solution? Apportera-t-il de meilleures propriétés au système de construction?
Notre professeur nous a demandé de créer quelque chose d'innovant pour le contexte cubain. Pour l'instant, il semble réservé par rapport au système constructif que nous vous présentons. Par conséquent, nous cherchons à améliorer notre projet de construction d'un système de terre résistant aux tremblements de terre.     
Sincèrement vôtre,    
MENU Katia MOT & Hélène    

![mail](images/mail_duznel_4.png)

> Traduction FR:
Chère Katia,    
Cuba n’a pas généralisé le système d’adobe bien qu’il ait été utilisé aux premiers stades de la colonie, mais il a été rapidement abandonné, les deux systèmes prédominants étant la boue et la tapisserie, la tapisserie à La Havane, dont il y a une excellente thèse de doctorat d’Idannis Monteagudo, je l’ai et je peux te la donner, sur adobe il y a aussi une thèse de doctorat de Belkis Sarroza de l’université de Las Villas, je ne sais pas si vous avez consulté ces thèses. J’ai travaillé profondément sur la boue, plus connue comme le *quincha*, Le BTC aussi avec les professeurs italiens Roberto et Gloria Mattone.      
À Santiago de Cuba, la zone la plus sismique est celle où le *cuje* est utilisé : des structures mixtes à colombages de bois bâché sous forme de panneaux.       
Mes remarques:     
- Il y a des expériences sismiques résistantes dans les Amériques qui pourraient être généralisées et le Chili a sa propre réglementation en la matière. La logique voudrait qu’ils s’adaptent au cas de Cuba.    
- La conception d’un nouveau système de construction, une demande de validation fondée sur des facteurs sociaux et naturels qui sont les plus proches du lieu d’utilisation, en ce sens, je ne sais pas ce qu’ils font, mais deux facteurs décisifs. Être accepté socialement et en sécurité dans le climat, avec de fortes pluies, des ouragans et des tremblements de terre. Pas seulement évaluer un événement de catastrophe naturelle.    
- Le dessin présente un schéma d’amarrage qui pourrait être fonctionnel, il faut essayer les sollicitations de poussée transversale des vents essentiellement, et sismiques, le bambou a montré avoir la flexibilité et la résistance aux mouvements sismiques déjà de cela il ya des études.     
- Les caractéristiques des sols en pisé sont également nombreuses, mais elles peuvent être caractérisées sans problème en fonction du lieu des travaux, qui sera le facteur à prendre en compte, le type de sol sur lequel le chantier est situé et s’il n’est pas approprié d’évaluer le coût d’un matériau de prêt.    
- Les joints dans les ouvrages d’usine ont pour fonction de rassembler les éléments, pour évaluer le thème sismique, il sera tenu compte de la capacité de rompre la continuité du mouvement sans déformer la structure action qui fait à côté de l’élément structurel dans ce cas le bambou. Je ne suis pas expert en la matière, mais il y a beaucoup d’études que je pourrais vous fournir.     
- Pourquoi adobe à Cuba?.... Il existe des systèmes constructifs de terre validés qui pourraient être exploités comme historiquement reconnus. Les exemples cubains d’adobe n’ont pas proliféré socialement, ni avec notre climat autre que sismique d’intenses ouragans et des périodes pluvieuses.      
Je ne sais pas si vous êtes à Cuba, si c’est le cas, vous pourriez venir à la Trinité et je vous montrerai nos exemples. Je prendrais en charge vos frais de logement et de nourriture et vous assurerez le transport.     
Je vous en prie.      
Duznel

![mail](images/mail_duznel_5.png) 

> Traduction FR:   
Bonjour Monsieur Zerquera,   
Désolée d'avoir tardé à vous répondre. Nous avons eu une semaine très chargée avec beaucoup de rendus.    
Nous n'avons pas connaissances de ces thèses. Si vous pouvez nous les transmettre, ça serait génial! Nous avons vu des exemples de cuje, notre travail est parti de là.   
Merci beaucoup pour vos réponses. Nous allons nous renseigner à propos des constructions parasismiques au Chili et faire plus de recherches sur la résistance aux vents de notre système constructif.    
Malheureusement nous ne sommes pas à Cuba pour le moment. Un voyage va peut-être avoir lieu en avril avec notre école.    
Merci beaucoup pour l'aide que vous nous avez apporté.   
Nous vous souhaitons déjà de très belles fêtes de fin d'année.   
Katia & Hélène

![mail](images/mail_duznel_6.png) 

> Traduction FR
Salut Katia.    
Joyeux Noël et succès pour la nouvelle année, quand vous aimez venir à Cuba et dans la ville de Trinidad, nous nous en occupons avec plaisir.    
Je vous enverrai ces thèses via Google Drive et je vous informerai lorsque je les aurai envoyées.    
Salutations    
Duznel


### Pecha kucka 2 

Voile notre deuxième [présentation](/docs/final_projects/adobe-parasismique/files/pecha_kucha_2.pdf) qui montre l’évolution de la forme du moule ainsi que le [texte](/docs/final_projects/adobe-parasismique/files/Oral_20_12.pdf)
Le jury est en accord avec nous sur le nouveau moule et souhaite que l’on mette en pratique le système constructif.



### Troisième prototype

Afin de tester notre système constructif, nous commençons à réaliser des briques en série avec notre deuxième moule. Mais on s’est vite rendue compte que la prise en main du moule n’est pas idéale. Nous avons alors étudié le moyen le plus efficace pour ajouter des poignées et garantir une meilleure prise en main.

Au départ, nous étions parties sur une brique de 40x40cm. Or, la terre et l’eau rende cette brique très lourde dans ces dimensions et pas facile à manipuler. Nous réduisons la taille de la brique a 30x30cm pour la rendre plus manutentionnable.

Le moule a donc évolué au niveau de la forme et son échelle. Voici le nouveau prototype à échelle 1:1 qui a été découpé à CNC. Nous avons optimisé le découpage des différentes pièces pour utiliser le moins de matériaux possibles et le moins de manipulations afin que la production puissent être plus effectives. 

![mail](images/moule_poignee.jpg)

En parallèle, nous réalisons un muret avec différentes assises afin de vérifier si la mise en oeuvre est correcte et viable avec le système parasismique en bambou. Nous l’avons maçonné et attendons que celui sèche. 
La mise en oeuvre a été rapide et simple. Ceci a été fait dans le but que réaliser un tuto à l’intention de cubain. Ce [tutoriel](/docs/final_projects/adobe-parasismique/tutorial.md) complet illustré qui retracent toutes les étapes: du choix de la terre à l’appareillage des briques en passant par la découpe à la CNC

### Pecha kucha 3

Voici notre dernière [présentation](/docs/final_projects/adobe-parasismique/files/RENDU_PECHAAA_KUUUUCHA-min.pdf) qui retrace toute l’évolution de notre projet depuis le début nos recherches, ainsi que le [texte]() pour une meilleure compréhension des images
