# Adobe parasismic

## Tutorial


### How to choose a good clay ?
#### Example 1

![1](images/1.jpg)

**1.** Take a bucket, shovel, paper, felt

**2.** Dig 20cm below ground level and put the soil in the bucket

**3.** Give each sample a name

![2](images/2.jpg)

**1.** Take three identical jars and make a line at two thirds

**2.** Fill each jar with a sample of soil, respecting the line. Each sample must be sieved to 0.5cm

**3.** Fill each jar with water

**4.** Shake vigorously for 2 minutes

**5.** Let sit for 24 hours

**6.** Result after 24h


![3](images/3.jpg)


Observe each layer of sediments of different natures (sand, silt, clay) and note their thicknesses

![bis3](images/3bis.jpg)

Calculate the proportions of each layer and define the nature of the soil. It must be clay.

#### Example 2

![4](himages/4.jpg)

**1.**  Take a trowel, and a bucket

**2.** Moisten the test soil and wait for 12 hours

**3.** Shape a ball the size of a grapefruit


![5](images/5.jpg)

**1.** Shape a sausage without just rolling it with finger pressure

**2.** Obtain a sausage 3-4cm in diameter

**3.** Lift it carefully and slide it so that it breaks naturally

**4.** Measure the length of the sausage and if it between 5 and 14cm , the soil is good for adobe


### How to make the mold ?

![6](images/6.jpg)

You can program a CNC to cut. It is also possible to create it from low-tech means.

An STL file. corresponding to the model of the mold is available through this link :
[moule avec poignées](/docs/final_projects/adobe-parasismique/Fichier moule stl/Moule_poignees_v3.stl)
[moule simple](/docs/final_projects/adobe-parasismique/Fichier moule stl/Moule_30cm_v5.stl)

![7](images/7.jpg)

You get these two plates cut to size.

![8](images/8.jpg)

You must assemble these boards in this order using glue and / or nails.

![8bis](images/8bis.jpg)

The mold is ready to use ! It's your turn !

### How construct a wall ?

![9](images/9.jpg)

Just assemble the adobe bricks using the equipment described above.

![10](images/10.jpg)


**1.** To make the mortar, add water so that the soil is very plastic and spreads easily with a trowel. Do not flatten the surface so that the brick adhere to the maximum during assembly.

**2.** Mount the bricks, seat by seat, insisting on the gaps.

**3.** Finally, the joints of the facing face can be made after 24 hours of drying. The finish is up to everyone's choice (smooth, rugged, ...)
