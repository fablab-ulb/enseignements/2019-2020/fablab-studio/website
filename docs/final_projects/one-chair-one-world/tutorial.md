# TUTORIAL
# (OCOW) - ONE CHAIR ONE WORLD

### MATERIALS:
##### STRUCTURE

<img src="../images/banner_acero_corrugado.jpg" width="60%">

<ol><li>Corrugated steel bar 4.5m (Diam 10-12mm)</li></ol>

##### ENVELOPE (TO CHOOSE)

<br><img src="../images/Captura_de_pantalla_2020-01-10_a_las_6.45.58.png" width="70%">

<ol><li>Textile thread</li><li>Vegetal thread</li><li>Synthetic thread</li></ol>

### TOOLS:

<br><img src="../images/sierra-metal-300-mm-dexter-pro-8118755-1.jpg" width="30%"> <img src="../images/1-2-6-electrical-metallic-tubing-using.jpg_350x350.jpg" width="30%">

<ol><li>Saw</li><li>Metallic pipe (30-40cm)</li></ol>

### FABRICATION:
##### MANUAL

The chair construction manual is summarized in a sketch which must be previously understood before it can be developed. It is necessary to know that all the angles that can be seen in the photograph are all equivalent to 90 degrees, which makes understanding and manufacturing much easier. In the tools previously explained, hollow tubes appear to help you to bend the bars, then using a second point of support, such as a foot making strength with its own weight. Even so, you can make these angles of infinity of ways and forms, using in each case what is most within the reach of each one, from a simple lever to a special machine to bend bars under construction.

CAUTION!

When we mark the measurements on the bar for later bending, we must bear in mind that when a bar is bent, according to the direction in which we do it, the bar will gain or lose length. If we do not pay attention to this, the structure may be different or may not reach us with the length designed and raised.

<br><img src="../images/medidas.jpg" width="25%">

IMPORTANT:

something that we recommend reacting once we acquire the material, is that in the same area of ​​purchase, either asking not to do it or doing it ourselves we will do two doubles, one right in the middle and another 40 cm to the side you want, of In this way, we will have two 2 meter bars which are much easier to handle and transport. At the time of manufacturing it will also facilitate mobility.

##### PROCESS

In the images that can be seen below, you can see the steps that would summarize the construction of the structure. also in the other images it is shown how the interlacing of the skin of the structure is performed. in this case it was made with a thread of vegetable fibers, since it was material that it had, but as it is mentioned in the materials, it can be any type of material, whether synthetic or textile.

In this ([3D Model](../documents/chair.f3d)) I leave the model of the structure, which you can discard to be able to see better how it is manufactured in the event that there is still some doubt when making a fold.

<br><img src="../images/detalle7.jpg" width="25%"><img src="../images/detalle1.jpg" width="25%"><img src="../images/detalle2.jpg" width="25%"><img src="../images/detalle3.jpg" width="25%">
<br><img src="../images/detalle4.jpg" width="25%"><img src="../images/detalle5.jpg" width="25%"><img src="../images/detalle6.jpg" width="25%"><img src="../images/detalle8.jpg" width="25%">
