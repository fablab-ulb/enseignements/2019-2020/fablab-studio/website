![image]( images/chair.JPG )

# One Chair One World

# Problématique (FR) 

<ul><li>Salaires.</li><li>Relation entre revenu et prix.</li><li>Manque de produits.</li></ul>

Question

> Comment, à l'aide d'outils numériques, pouvons-nous améliorer la situation de la population cubaine, grâce à l'amélioration du logement, dans sa vie quotidienne?

Hypothèse:

Je propose un manuel qui permet aux Cubains d'être autonomes lors de la création de leurs propres meubles. Comme je l'ai souvent observé, les maisons sont des endroits où les familles configurées selon différents âges et besoins vivent ensemble. Ce qui génère des besoins différents, qui ne sont pas toujours satisfaits par le même objet.

Dès le début, je n'ai pas cherché à créer un nouvel objet qui allait révolutionner le monde, mais à repenser un objet existant, un objet qui n'est pas essentiel, mais c'est un objet que tout le monde a chez lui. Un élément aussi simple et aussi nécessaire qu'une chaise.

Après avoir constaté qu'une chaise en plastique peut coûter environ 90% du salaire moyen, je décide de me concentrer non seulement sur le design mais aussi sur les matériaux. Ces matériaux doivent non seulement être résistants, mais ils doivent être moins chers. Comment puis-je faire quelque chose de moins cher et c'est encore mieux que ce qui existe?

L'idée que je propose est une simplification de l'objet chaise, et de cette façon je génère une structure polyvalente, résistante et moins chère, qui permet à la population cubaine de s'auto-fabriquer. Le projet se présente comme un matériau de structure, des barres d'acier ondulées, qui sont utilisées dans la construction. Cette structure sera celle qui est tordue de fibres, au début elle est considérée comme des fils plastiques obtenus à partir de bouteilles, pour son coût nul et la réutilisation d'un tel matériau nocif. D'autres matériaux tels que des fils d'origine végétale pourraient également être utilisés.

# Problematic (EN) 

<ul><li>Salaries.</li><li>Relationship between income and prices.</li><li>Lack of products.</li></ul>

Question

> How, with the help of digital tools, can we improve the conditions of the Cuban population, through improvements in housing, in their daily lives?

Hypothesis:

I propose a manual which allows Cubans to be self-sufficient when creating their own furniture. As I have often observed, houses are places where families configured by different ages and needs live together. Which generates different needs, which are not always met by the same object.

From the beginning I did not try to create a new object that would revolutionize the world, but to redesign an existing object, an object that is not essential, but that is an object that everyone has in their homes. An element as simple and as necessary as a chair.

After observing that a plastic chair can cost about 90% of the average salary, I decide to focus not only on the design but also on the materials. These materials not only have to be resistant but they have to be cheaper. How can I make something cheaper and that is even better than what exists?

The idea that I propose is a simplification of the chair object, and in this way I generate a versatile, resistant and cheaper structure, which allows the Cuban population to self-manufacture. The project poses as structural material, corrugated steel bars, which are used in construction. This structure will be the one that is twisted with fibers, in the beginning it is considered plastic threads obtained from bottles, for its zero cost and reuse of such a harmful material. Other materials such as threads of plant origin could also be used.

# Problemática (ES) 

<ul><li>Salarios.</li><li>Relación entre ingresos y precios.</li><li>Falta de productos.</li></ul>

Pregunta

> ¿Cómo, con la ayuda de herramientas digitales, podemos mejorar las condiciones de la población cubana, a través de mejoras en la vivienda, en su vida diaria?

Hipótesis:

Planteo un manual el cual permita a los cubanos ser autosuficientes a la hora de crear su propio mobiliario. Como he observado, a menudo, las casas son lugares en los que conviven familias configuradas por diferentes edades y necesidades. Lo cual genera diferentes necesidades, las cuales no siempre son satisfechas por un mismo objeto. 

Desde un principio no intenté crear un objeto nuevo que revolucionara el mundo, sino rediseñar un objeto existente, un objeto que no sea imprescindible, pero que sí sea un objeto que todo el mundo tenga en sus casas. Un elemento tan sencillo y tan necesario como una silla. 

Tras observar que una silla de plástico puede llegar a costar cerca del 90% del sueldo medio, decido centrarme no solo en el diseño si no en los materiales. Estos materiales no solo tienen que ser resistentes si no que tienen que ser más baratos. ¿Cómo puedo fabricar algo más barato y que sea incluso mejor que lo existente?

La idea que planteo es una simplificación del objeto silla, y genero de esta forma una estructura versátil, resistente y más barata, la cual permita a la población cubana su auto-fabricación. El proyecto plantea como material estructural, barras de acero corrugado, las cuales se usan en la construcción. Esta estructura será la cual se trence con fibras, en un principio se plantea hilos plásticos obtenidos de botellas, por su coste cero y reaprovechamiento de un material tan perjudicial. También se podrían utilizar otros materiales como por ejemplo hilos de origen vegetal.