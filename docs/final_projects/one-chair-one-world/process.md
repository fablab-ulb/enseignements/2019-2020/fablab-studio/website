# PROCESSUS
# RECHERCHE ET DÉVELOPPEMENT DE (OCOW) - ONE CHAIR ONE WORLD

### POINT DE DÉPART

OCOW prend comme point de départ l'étude exhaustive de la population cubaine. Nous avions besoin de bien connaître le problème pour développer une solution fonctionnelle. Cuba, un pays connu pour ses graves problèmes économiques, dû à un communisme radical et dictatorial, a besoin d'un changement social, d'un changement des gens ordinaires. Ce projet tente de développer un projet qui permet aux Cubains d'améliorer leur vie quotidienne, en s'éloignant de l'idée d'inventer quelque chose de jamais connu et en se concentrant sur l'idée d'améliorer quelque chose qui existe déjà.

### SALAIRES

Pourquoi étudier les salaires? Alors que nous essayons de développer un projet qui aura une forme matérielle, nous devons savoir quel budget nous avons et comment le dépenser. en obtenant des informations de différentes sources, nous avons obtenu les salaires maximum, minimum et moyen de la population cubaine. L'échange monétaire équivaut à 25 pesos cubains = 1 $. Nous parlons donc de salaires de la population cubaine variant entre 20 $ et 40 $, le salaire moyen étant de 31 $. Comme vous pouvez le voir, ce sont des salaires étonnamment alarmants. Le graphique ci-dessous montre comment les Cubains ont un revenu par habitant très similaire à celui qu'ils avaient il y a 50 ans, ce qui montre clairement que quelque chose ne fonctionne pas bien dans le système économique. Cependant, d'autres pays comme Singapour ont connu une croissance très différente, tous deux partant d'une situation similaire.

<br><img src="../images/grafica.jpeg" width="35%">    <img src="../images/Imagen_1.png" width="35%">

En voyant ces salaires, nous pouvons penser que la vie à Cuba est beaucoup moins chère et que la nourriture et les matériaux maintiendront une relation économique. Mais ce n'est pas le cas, comme indiqué ci-dessous, nous pouvons voir les prix des aliments de base, tels que: pain, œufs, poulet ou lait. Bien que les salaires soient bas, les prix n'ont pas de relation, car certains de ces prix sont visibles en Belgique ou en Espagne. Vous pouvez également voir d'autres produits, tels qu'un téléviseur ou une boîte à détergent. Ce ne sont peut-être pas des articles nécessaires, mais ils les utilisent tous et les ont à la maison. Pour cette raison, il y a encore des gens qui ont des téléviseurs noir et blanc à la maison.

<br><img src="../images/Captura_de_pantalla_2020-01-09_a_las_19.12.09.png" width="40%">

### ENVIRONNEMENT DU LOGEMENT ET DU MOBILIER

En essayant de concentrer le développement du projet sur un aspect architectural et design, j'ai concentré l'analyse sur les maisons. Grâce aux différents entretiens, nous avons pu mieux connaître la vie de la maison cubaine. Nous avons appris que, dans la plupart des cas, différentes générations partagent le même logement, au fur et à mesure que les familles grandissent, mais n'ont pas assez d'argent pour devenir indépendantes. parce que les maisons n'ont pas de très grandes dimensions, dans de nombreux cas, la même pièce de la maison sert de deux pièces différentes, par exemple le salon de la chambre de nuit.

### CHAISES

En cherchant des images des maisons de la population cubaine, j'ai pu voir qu'il y avait une sorte de problème avec les meubles, et surtout avec les chaises. Comme le montrent les images ci-dessous, les meubles que la plupart des maisons contiennent sont de vieux meubles, tout comme les chaises. Une nouvelle chaise en plastique coûte environ 25 dollars, ce qui équivaut à 80% du salaire moyen d'un Cubain. C'est pourquoi, comme vous pouvez le voir sur l'image suivante, une fois qu'ils achètent une nouvelle chaise, ils s'en occupent de manière à ce qu'ils puissent même les scotcher afin qu'ils ne soient pas cassés, car ils n'auraient pas d'argent pour une nouvelle. Il est difficile pour une personne d'acheter une nouvelle chaise, mais il est impossible pour quelqu'un d'en acheter plus d'une.

<br><img src="../images/Imagen_2.png" width="30%">    <img src="../images/Imagen_3.png" width="30%">

### CHAISES DES 80 '

Une fois la gestion du projet définie, nous commençons par la recherche, mais cette fois pas si générale, mais focalisée sur le sujet à développer. J'ai commencé à étudier et à redessiner les dessins qui avaient une certaine hauteur à Cuba, de cette façon, je pouvais en savoir un peu plus sur la relation de ces meubles avec la population cubaine. J'ai aussi étudié différents designers cubains, mais surtout Luis Ramírez, un designer cubain qui a une belle carrière dans le monde des chaises. Grâce à leurs créations, j'ai pu voir que les Cubains sont audacieux, qu'ils ne sont pas conservateurs, qu'ils vont plus que l'objet qu'ils jouent avec les formes, les couleurs et surtout les matières.

### CHAISE TRANSFORM

Qu'est-ce qu'une chaise? En quelles parties puis-je le diviser ou le simplifier? Une chaise est égale à une structure plus une enveloppe. Je devais faire quelque chose de simple, j'ai donc dû réduire une chaise à l'idée la plus simple. J'ai décidé de me concentrer sur la recherche de designs dans lesquels, la partie principale était la structure, qui se concentrait sur la même idée, une structure enveloppée. Ci-dessous, je montre certains des concepteurs et leurs conceptions, qui ont servi de base et m'ont aidé à comprendre la simplification d'une chaise.

### DIFFÉRENTES RÉFÉRENCES

#### Luis Ramirez:

"Depuis ses débuts en 1991 en tant que designer industriel, il est constamment à la recherche d'une forme d'expression esthétique qui renvoie même inconsciemment à la fonction, ses créations d'une manière harmonieuse et intelligente parviennent à combiner différents matériaux, chacun s'exerçant efficacement la fonction technique ou fonctionnelle qui lui correspond. Son travail a toujours été lié à l'industrie, s'aventurant dans les technologies et y apportant des changements qui lui permettent d'apporter une touche d'originalité à chacune de ses créations."

<br><img src="../images/Captura_de_pantalla_2020-01-09_a_las_18.15.31.png" width="25%">    <img src="../images/Captura_de_pantalla_2020-01-09_a_las_18.15.39.png" width="25%">    <img src="../images/Captura_de_pantalla_2020-01-09_a_las_18.16.11.png" width="25%">

#### Åsa Kärn:

"Mon ambition est de concevoir des meubles et des produits en utilisant des matériaux respectueux de l'environnement avec une longue durabilité, en mettant l'accent sur des méthodes de production durables et des conditions de travail humaines. Ce sont les critères que j'utilise comme plateforme pour ma conception et ma production.

Dans mon design, mon objectif est de combiner de manière ludique et de repousser les frontières entre le design moderne et fonctionnel, l'art et l'artisanat de différentes cultures"

<br><img src="../images/Captura_de_pantalla_2020-01-09_a_las_18.14.06.png" width="30%">

#### Carolina Armellini y Paulo Biacchi:

"Le concept fétiche est intrinsèque aux produits développés par Carolina Armellini et Paulo Biacchi, à la recherche de références et de significations au-delà des parties formelles et fonctionnelles.

La définition du dictionnaire de ce qui est fétiche est quelque chose qui inspire vraiment le couple. Fétiche sm Objet animé ou inanimé, fabriqué par l'homme ou produit par la nature, attribué au pouvoir et aux cultes surnaturels."

<br><img src="../images/Captura_de_pantalla_2020-01-09_a_las_18.25.39.png" width="30%">    <img src="../images/Captura_de_pantalla_2020-01-09_a_las_18.25.27.png" width="30%">

### MATERIAUX

Contrairement à la plupart des designs, qui sont d'abord conçus puis pensés au matériau qui convient le mieux au design développé, dans ce cas, le projet est développé à l'envers, nous nous concentrons d'abord sur le matériau. Une fois que nous connaissions le matériau, nous commencions à penser à la forme. Cela se produit en raison du problème économique, car nous dépendons beaucoup du budget. un autre problème qui nous conditionne beaucoup est le manque de matériaux, le problème est non seulement que nous n'avons pas de budget, mais que même si nous avons de l'argent, nous ne pouvons pas en disposer.

#### Structure

Je me suis d'abord concentré sur la structure et j'ai décidé que les barres en acier ondulé, utilisées pour assembler le béton, seraient le matériau idéal. L'acier ondulé est actuellement utilisé en continu dans la construction, même à Cuba. Ils sont donc faciles à trouver car ils sont généralement utilisés dans la construction et vous pouvez les acheter ou même utiliser les restes de certains travaux. Ce matériau est facilement moulable, car vous n'avez pas besoin d'une machine spécifique pour son moulage, mais il a également une grande résistance, car il est utilisé dans la construction de structures.

#### Envelope

Dans une première approche de l'approche du matériau à utiliser pour l'enveloppe, j'ai décidé d'utiliser des bouteilles, que nous utiliserions pour faire des fils de plastique, ce qui nous permettrait de tresser la structure. Les bouteilles nous ont permis une grande possibilité en termes de design, car il y a différentes couleurs, ce qui nous a permis de créer différentes compositions.

La méthode pour obtenir le filament était déjà connue. Vous pouvez utiliser un objet comme celui conçu par mes camarades de classe ou tout simplement un ciseau. Dans ce cas, le coût serait nul, car nous n'achèterions pas le matériel pour fabriquer la chaise, mais nous recyclerions plutôt les conteneurs. C'était une très bonne chose, car si le filament se cassait, nous pourrions réagir rapidement et à un coût nul.

Enfin j'ai pensé qu'il n'était pas nécessaire de réparer le matériel, car nous ne pourrons pas savoir de quel matériel les utilisateurs qui souhaitent préparer ce projet auront plus facilement.

C'est pourquoi je décide de laisser le matériau d'enveloppe comme quelque chose de plus individuel, car différents matériaux peuvent être utilisés, tels que des fibres textiles, végétales ou synthétiques, selon le matériau disponible pour chacun.

<br><img src="../images/Captura_de_pantalla_2020-01-09_a_las_20.03.19.png" width="60%">
<img src="../images/Captura_de_pantalla_2020-01-09_a_las_20.06.22.png" width="60%">

### CONTACTO: ERNESTO OROZA

A pesar de buscar en internet sobre la facilidad de encontrar ciertos materiales, decidí ponerme en contacto con Ernesto Oroza, el cual conocía muy bien la situación real y podía ayudarme sin ningún problema a saber que materiales podrían encontrarse facilmente y cuales no. Le pregunté sobre el material inicialmente planteado, las barras de acero corrugado, utilizadas en la construcción.

Gracias a sus conocimientos y a algunas preguntas que le hizo a un amigo arquitecto, me confirmó que si que había barras de este tipo en el mercado. Algunas veces las vendía el estado, pero siempre las podías encontrar en el mercado negro. Estas barras miden unos 9 metros y hay de dos tipos de 12 y 14 milímetros de diámetro. El coste es de 10 cucs la barra, por lo que el metro cuesta alrededor de 1 cuc.

<br><img src="../images/ERNESTO-1.PNG" width="30%">  <img src="../images/ERNESTO-2.PNG" width="30%">  <img src="../images/ERNESTO-3.PNG" width="30%">

>TRADUCTION

>|IMAGE 1|IMAGE 2|IMAGE 3|
>|---|---|---|
>|<ul><li>DIEGO: Regardez, je vous ai parlé un peu de l'idée ... Je travaille sur la conception de chaises plus résistantes et moins chères et en plus elles sont attrayantes. Je me concentre sur la conception d'un type de structure qui me permet d'entrelacer une série de cordes, qu'il s'agisse de fils textiles, végétaux ou plastiques d'une seule forme. De cette façon, créer avec la même structure des chaises différentes. Le matériau que j'ai pensé générer cette structure sont des barres d'acier ondulées, qui sont utilisées dans la construction pour assembler le béton. Mais je ne sais pas s'ils sont faciles à trouver ...</li><li>ERNESTO: Oui, parfois l'État les vend.</li><li>ERNESTO: Bien que sur le marché noir il y ait toujours</li><li>ERNESTO: Neuf mètres de long</li></ul>|<ul><li>ERNESTO: Environ 12 ou 14 millimètres de diamètreDIEGO: Brillant</li><li>DIEGO: Et le prix sauriez-vous plus ou moins ce que cela pourrait être?</li><li>ERNESTO: Pas l'actuel, mais je demande à Sergio, un architecte de Copincha</li><li>ERNESTO: Je vous dis quand il répond</li><li>DIEGO: Merci beaucoup</li><li>DIEGO: J'attends</li><li>ERNESTO: ok j'ai déjà demandé</li><li>ERNESTO: 10 cuc chacun 9 mètres</li></ul>|<ul><li>DIEGO: Merci beaucoup</li><li>DIEGO: J'attends</li><li>ERNESTO: ok j'ai déjà demandé</li><li>ERNESTO: 10 cuc chacun 9 mètres</li><li>DIEGO: ok merci beaucoup</li><li>ERNESTO: Cela équivaut à 10 dollars ou 250 pesos cubains</li><li>DIEGO: ok</li><li>

### DESIGN

#### Problèmes

<ul><li>Le poids est le premier des problèmes obtenus lors de l'approche de la structure, car nous utilisons un matériau solide, et nous avons pu créer un design impossible à déplacer. Dans l'image, nous pouvons voir la différence de poids par unité linéaire, entre les deux types de barres disponibles.

Il a été calculé qu'une chaise a entre 4 et 5 mètres de structure, il y aurait donc une grande différence si vous utilisiez une tige ou l'autre. La chaise avec la barre la plus fine pèserait plus d'un kilo et demi de moins. Quelle grande différence.</li><li>
Un autre problème que j'ai trouvé était la différence entre le soudage et le pliage du matériau, car si je soudais le matériau, la chaise serait beaucoup plus résistante et stable, mais ce serait plus cher, cependant, si je la pliais simplement, la structure C'était moins stable mais moins cher.</li><li>

Quant à l'union de l'enveloppe avec la structure, comme on peut le voir sur les images, il ne s'agit pas d'une union fixe, mais il s'agit uniquement d'envelopper la structure en fabriquant une peau.</li><li>

Un autre problème que j'ai rencontré était la stabilité de cette chaise, bien que ce soit un matériau très résistant, j'ai dû faire attention au design, car je ne pouvais pas faire asseoir les gens sur des barres qui subissaient trop d'efforts.</li></ul>

<br><img src="../images/Captura_de_pantalla_2020-01-09_a_las_19.51.31.png" width="50%">

### DÉVELOPPEMENT:

##### PHASE 1: DÉFINIR LES BASES

Comme il s'agit d'un long processus de travail, il y a eu différentes étapes, qui pourraient être différenciées en trois phases. Le premier, comme dans tous les types de projets, s'est concentré sur l'étude approfondie du problème et l'élaboration du plan de travail. Chacune de ces phases s'est terminée par une présentation et une présentation du travail effectué au cours des semaines de travail précédentes. La première présentation ([PechaKucha 1](docs/final_projects/One-Chair-One-World/documents/OCOW_-_1.pdf)) est celle qui a le moins approché le processus de développement du prototype. pas moins important, car il s'agit même de la phase la plus importante du projet, car elle jettera les bases d'un bon développement du travail.

Vous pouvez voir ici la première approche du projet dans laquelle divers prototypes (fichiers 3D) ont été développés, grâce à des imprimantes 3D. Ces prototypes m'ont permis de mieux comprendre mon projet, car malgré le fait qu'ils n'en soient pas très satisfaits, les défauts et les vertus des designs se sont démarqués. Il était clair qu'il manquait beaucoup de travail, mais c'était une première approche et il y avait beaucoup de choses sur lesquelles travailler.

<br><img src="../images/IMG_1837.JPG" width="50%">

##### PHASE 2: PREMIERS PAS

Dans la deuxième phase, les idées étaient beaucoup plus claires. Bien que n'étant pas satisfait des prototypes réalisés lors de la première phase, j'étais très satisfait de la base générée et de l'organisation. À ce stade, je n'avais qu'à me concentrer sur le prototype, à explorer les matériaux, à les tester et à continuer à enquêter jusqu'à ce que nous trouvions la solution parfaite.

Dans la deuxième présentation ([PechaKucha 2](docs/final_projects/One-Chair-One-World/documents/OCOW_-_2.pdf)), je me suis davantage concentré sur le développement du prototype. Grâce à cela, au cours de cette phase, j'ai pu corriger des aspects plus techniques du prototype, qui, dans le premier, n'était qu'un contact avec le prototype. Ils m'ont redirigé sur certains aspects que je n'avais pas bien résolus et m'ont posé une question qui m'a fait repenser certaines choses. C'est une excellente méthode de travail pour effectuer des phases et exposer votre travail. Le point de vue d'une autre personne peut vous aider à voir des choses que vous n'avez pas vues ou inversement à renforcer des aspects dont vous doutiez.

<br><img src="../images/IMG_1851.JPG" width="25%">   <img src="../images/IMG_1854.JPG" width="25%">

##### PHASE 3: MODÈLE FINAL

enfin nous sommes dans la dernière phase de développement, la finalisation et la présentation du prototype final. Comme vous pouvez le voir au cours des images, le projet a pris différentes formes, maintenant et recherchant toujours une série de directives:

<ul> <li>
Polyvalent: Comme nous l'avons vu, les foyers sont souvent un chaos de personnes et pas toujours du même âge. Le projet essaie de développer un modèle qui peut être utilisé dans différentes positions. </li> <li>
résistant: nous recherchons la durabilité du modèle, des chaises en plastique sont actuellement vendues, qui finissent par se détériorer jusqu'à ce que le plastique cède et se casse. </li> <li>
Bon marché: nous essayons d'améliorer la qualité par rapport au prix. Nous ne pouvons pas nous permettre de dépenser 80% de notre salaire sur une chaise en plastique. Nous améliorons les matériaux en offrant une plus grande résistance et durabilité, pour un coût moindre. </li> <li>
Conception: Nous défendons depuis le début que quelque chose de bon marché ne doit pas être ennuyeux. Nous recherchons la relation des qualités précédentes, mais sans pour autant négliger la question esthétique, qui est souvent très importante.
</li> </ul>

Avec tout le travail expliqué ci-dessus, nous avons finalement atteint le prototype final, qui répond à toutes les directives mentionnées. Elle est cinq fois moins chère qu'une chaise conventionnelle, est plus résistante, a plus d'utilisations et a surtout un design plus attrayant et personnel. Ici vous pouvez voir la dernière présentation ([PechaKucha 3](docs/final_projects/One-Chair-One-World/documents/OCOW_-_3.pdf)) dans laquelle je montre l'ensemble du processus de manière synthétisée.

<br><img src="../images/silla_sola.png" width="30%">
