# Briques de bagasse

## Processus de recherche FR

![image en ligne](Images/maison2.jpg)
______________________

### Thématique

Après s'être documentés sur les réalités cubaines, on s'est vite rendu compte qu'il y avait des soucis liés à l'état du logement.
Structures en mauvais état, manque de matériel de construction afin de les restaurer, ...
On s'est donc intéressé à cette thématique du logement afin de, peut-être, trouver une solution à notre échelle.  

On s'est informé sur la question de plusieurs manières: sites, livres, et directement auprès de Ernesto Oroza

> Référence: "3 Houses" -> lien : [3 Houses book](https://drive.google.com/file/d/1ZqNEfq6NazxeA1RN9qjZPGxXpJtdTRVL/view?usp=sharing)

Pour comprendre l'état actuel du logement, nous sommes remontés un peu dans l'histoire de Cuba :

- Jusque dans les années 50, Cuba est corrompu sous le dictateur Fulgencio Batista. Cuba est alors comme Las Vegas d'aujourd'hui : un terrain de jeu pour de riches américains qui y ont de larges villas, drogue et jeux d'argent à gogo.

- En 1959, Fidel Castro renverse le pouvoir en place. Son idée : éliminer le système de classes -> tout le monde doit être sur le même pied d'égalité.
Il met alors en place une réforme urbaine : les riches sont expulsés de chez eux et tous le slogements sont redistribués selon la taille de la famille et l'ascendance.

- En 1963, les Etats-Unis décrètent l'embargo sur le commerce à Cuba : plus de matériaux de construction provenant des USA sont en vente à Cuba.
Avec la réforme, tous les cubains gagnent aussi la même somme, peu importe le métier : 40$ par mois. Ils n'avaient donc pas les moyens de se procurer du matériel autre part.

- Depuis la chute de l'URSS (période spéciale) en 1991, de nombreuses réformes sont mises en place.
Mais aujourd'hui, suite à des années d'interdiction de restaurer ou de construire, le logement est dans un état critique. Tout le monde veut corriger des années de détérioration mais la matériel manque ou est trop cher.

**Augmentation de la population -> manque de place -> besoin d'agrandir ou de subdiviser -> manque de matériel de construction ou manque de moyens -> logement rudimentaire.**

Nous avons alors constaté qu'il y avait un réel problème au niveau de la surpopulation et, ducoup, au niveau du manque de place dans les logements.  
La réponse des cubains face à cette problématique est la subdivision des maisons existantes.
La plupart d'entre-eux vivent aujourd'hui dans des "solars", ces fameuses maisons subdivisées.

![image en ligne](Images/solars.png)

Mais cette alternative crée des problèmes de coexistence : plusieurs générations vivent sous le même toit, avec parfois une même pièce pour manger et pour dormir, une grande proximité entre voisins ce qui peut créer des tensions et surtout un manque d'intimité.

Le cloisonnement de logements est généralement fait avec des parpaings. Ceux-ci présentent, selon nous, 2 problèmes majeurs:

- ils sont en quantités limitées sur l'île donc chers et ne sont donc pas accessibles à tous les habitants, qui devront alors cloisonner avec les moyens du bord, parfois du carton, parfois même un simple rideau.
- ils sont lourds et mettent encore plus de poids sur les structures des bâtiments, déjà en mauvais état.  

![image en ligne](Images/cloison.png)

______________________

### Question de recherche

> Comment répondre au besoin de cloisonnement d’espace pour la création de logements de manière accessible à tous - peu coûteux, facile à faire et en utilisant les ressources naturelles présentes à Cuba - et de manière légère afin de préserver au maximum les structures des bâtiments, pour remplacer les blocs de béton chers et lourds utilisés aujourd’hui ?

______________________

### Notre proposition

> Créer des briques utilisant les ressources disponibles à Cuba de manière gratuite, qui sont emboîtables ce qui permet le montage et le démontage facile, en fonction de l’évolution du logement.
En d'autres mots : *Comment créer des cloisons plus légères et pour 0 € ?*

______________________


### Recherche de matériaux

Nous commençons alors à chercher un matériau de construction disponible gratuitement à Cuba. L'objectif est de trouver un matériau le plus fort possible et le moins dense possible.

![image en ligne](Images/strenght_density.jpg)

On pense directement à la **terre crue** et aux **déchets**.

> Terre crue

Nous recherchons quel type d'argile est présente à Cuba: KAOLINITE.


On sait que des techniques existent déjà avec de l'argile :
- briques BTC
- adobe
- pisé
- torchis


> Déchets

Ensuite nous cherchons quels déchets sont présents en quantité à Cuba et qu'on pourrait récupérer.
On se met alors en contact avec Nicolas Lambillon, qui est coordinateur au Recy-K et qui a encadré un workshop à l'ISDI (institut supérieur de design industriel) à La Havana à propos de Dechets & Design.
Dans le cadre de ce workshop, les étudiants ont mené une enquête de terrain sur les déchets à Cuba, parfait pour nous!

![image en ligne](Images/ISDI.png)

On voit que les cannettes, les bouteilles PET, les sacs plastiques ou encore le carton sont présents en grande quantité. On commence donc à réflechir comment intégrer ces matières dans nos briques afin de les rendre plus légère.

Intégrer des déchets dans nos briques nous permettrait de répondre à 2 problématiques :
- celle du cloisonnement, qui est notre but final
- celle des déchets et de la réutilisation de ceux-ci

On réflechit donc aux différentes techniques possibles : broyer le plastique et le recompacter en forme de brique, faire fondre le plastique et le mouler, faire une sorte de papier mâché avec le carton, ...

![image en ligne](Images/plastickbrick.jpg)

> Déchets végétaux

En continuant nos recherches, on se rend alors compte que ces déchets ne sont pas si accessibles qu'ils en ont l'air.
- Soit des filières existent déjà à Cuba, où ils récupèrent les déchets afin de les réutiliser.
- Soit le gouvernement gère ces déchets et on y a donc plus accès: comme par exemple les cannettes qui sont envoyées au Japon afin de les faire recycler.
- Soit les quantités sont très limitées : apparemment chaque famille n'a que 5 bouteilles PET par mois.

On ne sait donc pas si les habitants auront réellement la possibilité d'en récupérer suffisamment pour les transformer en briques.

On essaye donc de chercher ailleurs. On pense alors qu'il faudrait faire une brique totalement bio-sourcées, c'est-à-dire avec des matériaux naturels.  
Récupérer les déchets c'est bien, mais leur transformation est généralement très énergivore et polluante.

On pense donc aux **déchets végétaux** et cherchons alors lesquels sont présents sur l'île.
On pense à :
- sciures de bois
- fibres de coco
- bambou
- déchets agricoles: son de blé, balle de riz, ...
- fibre de bananier
- paille
- coques de cacahuètes
- ...

On va donc essayer, avec les matières qu'on trouve en Belgique, de les lier avec de l'argile.

> BTC

La construction en terre crue nécessite un mélange d’⅓ de graviers, ⅓ de sable et ⅓ d’argile, pour une bonne imbrication des matériaux et donc une bonne cohésion d’ensemble.

- Gravier : entre 20 et 2 mm
- Sable : entre 2 et 0,06 mm
- Argile : < 0,002 mm

On commence par essayer de créer des briques BTC allégées, mais *quelles sont les proportions du mélange afin d'intégrer les fibres végétales?*

1. BTC (argile-gravier-sable)
2. Argile-copeaux de bois-paille
3. Argile-paille


![image en ligne](Images/tests_briques.jpg)

| Avantages  | Inconvénients  |
| --- | --- |
| <ul></li> <li></li> solides (car comprimées) <li></li> autoportantes (pas besoin de fixation aux murs) <li></li> emboîtables (pas besoin de mortier) si on leur donne une forme particulière <li></li> facile à installer et démonter <li></li> réutilisables | <ul></li> <li></li> assez lourd <li></li> nécessite une presse mécanique |



> Torchis

| Avantages  | Inconvénients  |
| --- | --- |
| <ul></li> <li></li> très léger <li></li> tout se fait à la main (très peu d'outils nécessaires) | <ul></li> <li></li> nécessite un cadre en bois/bambou sur lequel fixer le mélange <li></li> fixés aux murs existants (pas réutilisable ou modifiable) |


Référence:

![image en ligne](Images/batirenterre2.jpg)


> Chaux

En continuant notre recherche de matériaux, on s'intéresse à un matériau existant en Belgique : le béton végétal ou **le béton de chanvre-chaux**.
Celui-ci est moulé sous forme de bloc et utilisé comme isolation.

Ce matériaux éco-hybride (un produit végétal + un liant minéral) présente de nombreux avantages :
- isolant acoustique
- isolant thermique
- résistant au feu
- régule l'humidité
- 100% naturel
- durable
- biodégradable


Référence : [Isohemp](https://www.isohemp.com/)


Nous allons donc nous baser sur ce procédé pour créer notre brique -> on remplace notre liant à l'argile par un liant de chaux, ce qui allègera la brique.

- Densité bloc de chanvre-chaux : 340 à 500 kg/m3
- Densité brique de terre crue : 700 à 1500 kg/m3
- Densité bloc de béton : 2200 à 2400 kg/m3

______________________


### Mélange chaux-bagasse

Parmis les différentes possibilités de déchets végétaux, on retient principalment la **bagasse**.

![image en ligne](Images/bagasse2.jpg)

La bagasse est le résidu fibreux de la canne à sucre, après extraction du jus.
On a vu qu'à Cuba, il y a un grand nombre de machine à extraction de jus et donc beaucoup de résidus non-utilisés (à notre connaissance).

On a vu aussi qu'actuellemment, la bagasse est déjà utilisée pour plusieurs choses:
- création de papier et carton
- cendres utilisées pour créer de l'énergie
- amendement pour les sols agricoles
- BAGAPAN : panneaux agglomérés à base de bagasses
- XANITA board : panneaux de carton fabriqué à partir de fibres récupérées de boîtes recyclées et de bagasse
- ...

![image en ligne](Images/brique_bagasse.JPG)


On sait aussi que la bagasse est un très bon isolant thermique et qu'il resiste à des t° extrêmes (résistance au feu)

![image en ligne](Images/conductivité.png)

On ne connait cependant pas la performance phonique de la bagasse.
*Qu'est-ce qui fait qu'un matériau est isolant acoustiquement?*

[Isolation phonique](https://fr.wikipedia.org/wiki/Isolation_phonique)

En regardant le tableau de comparaison entre les différents isolants naturels ->  on suppose que la bagasse a un pouvoir absorbant du son similaire que les autre sisolants à base de fibres végétales.  

![image en ligne](Images/acoustique.png)
______________________


### Chaux naturelle ?

Nous avons un doute sur la disponibilité de la chaux à Cuba.
Nous avons donc recherché des manières d'obtenir de la chaux de manière facile et naturelle.

Si on le fait de manière classique, la chaux est le résultat d'une calcination d'une roche calcaire. Cela veut dire qu'il faudrait trouver de la roche calcaire, la broyer et la chauffer à environ 900°C, ce qui semble assez compliqué ...

Il existe dans la nature un procédé naturel qui utilise le même système que celle de l'homme, mais à température du corps (et non à 900°C). Il s'agit de la **biominéralisation**.

La biominéralisation est le processus par lequel les organismes vivants produisent des minéraux, souvent afin de durcir ou raidir leurs tissus pour former des tissus minéralisés. Ce terme désigne aussi « toute structure minérale élaborée par un organisme vivant ».

Ou en trouver à Cuba ? Dans les *coquilles d'oeufs* (composées à 95% de carbonate de calcium CaCO3) et dans les *coquillages* (principalement composés de CaCo3).


______________________


### Intégration des outils numériques

Nos briques peuvent se créer de manière tres artisanale, ce qui est idéal pour Cuba.
Cependant, dans le contexte de l'atelier, il nous est demandé d'intégrer les techniques numériques avec les outils appris (impression 3D - lasercut - CNC) afin de faciliter la reproduction de notre projet pour quiconque a accès aux outils nécessaires.

*Comment les outils numériques peuvent-ils nous aider à faciliter la création de notre brique en chaux-bagasse ?*

On décide donc de créer une presse afin de mouler nos briques dans la forme voulue grâce à la **machine CNC**. Cela permettra une découpe précise et facile et d'avoir un modèle 3D de la presse afin de l'adapter selon nos besoins.

Nous recherchons donc une forme de brique :

- emboîtable (afin de ne pas avoir recours à un mortier ou à des outils)
- sans interstice pour plus d'intimité
- facile à monter et à démonter en cas d'agrandissement ou de changement au sein du logement
- facile à créer pour tout le monde

Nous commençons par faire différents prototypes de formes à l'aide de l'**impression 3D**.

![image en ligne](Images/proto_briques.jpg)

Notre forme finale est celle-ci :

Elle nous semble idéale car elle permet l'emboitement des briques dans 1 seule position grâce à la forme papillon et ne laisse aucun interstice grâce à une partie centrale décalée.
Cependant, cette forme semble compliquée à mouler : des parties risquent de casser et la forme du moule en lui-même compliqué à créer. (image1)


Fichiers STL :

[Forme brique 1](Images/Brique_finale_jury.stl)

[Forme brique 2](Images/Forme_brique_prejury_.stl)

Nous nous rabattons donc sur une forme simplifiée (sans papillon) afin de faire notre prototype de presse. Celle-ci est tout aussi efficace pour en faire une cloison, mais de forme plus classique. (image2)

![image en ligne](Images/proto_final.jpg)
![image en ligne](Images/briques_emboitées.jpg)

Pour la presse, nous recherchons différents modèles existants sur lesquels se baser.

Nous cherchons un modèle assez petit et faisable facilement par n'importe qui. Evidemment il est plus simple de la faire à l'aide de la machine CNC, comme nous avons fait.
Nous nous basons donc sur ce modèle de presse à briques en papier : [Paper brick press](https://www.youtube.com/watch?v=djDMSfpEDsE)


Il serait idéal de la faire en métal mais, pour des raisons de compétences et d'outils, notre prototype sera fait en MDF. Le risque avec le MDF c'est qu'il craque facilement en le vissant, se déforme alors et absorbe l'eau du mélange chaux-bagasse.

Nous dessinons la presse en 3D sur un sketchup, plaçons toutes les parties séparées sur des planches de 50x90 puis on l'importe dans Fusion afin de le paramétrer pour la machine CNC.

-> Fichiers STL :

[Presse CNC 1](Images/Presse_CNC_1.stl)

[Presse CNC 2](Images/Presse_CNC_2.stl)

[Presse CNC 2](Images/Presse_CNC_3.stl)

______________________

Pecha Kucha de présentation : [Pecha Kucha](https://docs.google.com/presentation/d/1aQ7IU9r-1A_NZpKWNI2qLtTINGim41NhTnjax9oWdtc/edit?usp=sharing)

______________________
