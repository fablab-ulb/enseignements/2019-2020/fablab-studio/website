# Cubos a Cuba

## Tutorial : Sugarcane bricks (EN)

### Material

> For the mixture

 - A container
 - A scale
 - Lime
 - Bagasse (sugarcane fibres from which the juice has been extracted)
 - Water

 If we don't have lime:
 - Eggshells and/or powdered shells (mixed)


> For the press

 - 3 boards of MDF of 50x90 cm
 - A screwdriver and screws
 - Wood glue
 - Sanding paper
 - Bolts
 - Scotch
 - 16 and 20mm diameter metal pipes (broomstick type)
 - A metal saw

______________________


### The mixture

1. Mix the lime with the water until a smooth mixture is obtained in the following proportions: 35 kg of lime x 35 L of water x 100 L of bagasse
2. Add the bagasse
3. Mix until the dough is neither too wet nor too dry (add water if necessary).

![image en ligne](Images/matières.jpg)


______________________


### The press

1. Cut each part of the 4 parts of the machine (outer box - U-shaped box - cover - lever) according to these dimensions :

![image en ligne](Images/pièces.jpg)


2. Assemble the parts of each part using wood glue and screws :

Caution: remember to sand the edges well so that the parts fit together well

3. Tape all the inside surfaces of the machine so that the lime-bagasse mixture does not stick to the walls.

4. Attach the handles with bolts to allow rotation.
Caution: make sure that the handles can cross each other -> to do this, one pair of handles should be attached further apart than the other pair.

5. Cut the metal pipes to create handles, insert them into the holes provided and glue them.

Parts:

![image en ligne](Images/parties.jpg)

Steps:

![image en ligne](Images/étapes.jpg)


______________________


### Moulding

1. Insert the mixture into the outer box and the U-shaped box up to the maximum.
2. Place the lid on the mixture.
3. Lift up the handles to put pressure on the lid. The lid will gradually lower.
4. If possible, add more mixture.
5. Press down as hard as you can until the lid touches the outer box.
6. Lift up the lid.
7. Remove the U-box from the outer box.
8. Remove the brick from the U-box with great care.
9. Let the brick dry in a dry place.  

Steps:

![image en ligne](Images/moulage.jpg)

![image en ligne](Images/démoulagef.jpg)


STL files:

[Press CNC 1](Images/Presse_CNC_1.stl)

[Press CNC 2](Images/Presse_CNC_2.stl)

[Press CNC 3](Images/Presse_CNC_3.stl)

[Press](Images/Presse_finale_complete.stl)





______________________
