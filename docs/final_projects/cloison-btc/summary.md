# Cubos a Cuba

## Summary


![image en ligne](Images/protos_complets1.png)


## Problématique (FR)

- Crise du logement : besoin d’en créer de plus en plus
- Séparation des maisons actuelles par des cloisons
- Le matériel de construction est insuffisant et cher
- Cloisons en blocs de béton = cher et lourd, tout le monde n’y a pas accès et elles mettent du poids sur les structures des bâtiments

### Question

> Comment répondre au besoin de cloisonnement d’espace pour la création de logements de manière accessible à tous - peu couteux, facile à faire et en utilisant les ressources naturelles présentes à Cuba - et de manière légère afin de préserver au maximum les structures des bâtiments, pour remplacer les blocs de béton couteux et lourds utilisés aujourd’hui ?

### Hypothèse

On propose une cloison en briques que tout le monde peut construire. Les briques utilisent les ressources naturelles - des déchets végétaux - et sont emboitables ce qui permet le montage et le démontage facile, en fonction de l’évolution du logement.
Les fibres végétales sont l'isolant de la brique. Elles possèdent une densité très faible, ce qui rend la cloison très légère, mais lui confèrent les propriétés nécessaires (aussi utile que le béton) - isolation thermique, acoustique, résistance mécanique.
La fibre végétale trouvable en abondance à Cuba est la bagasse, c’est-à-dire les résidus de tiges de canne à sucre dont on a extrait le jus.
Le liant utilisé dans la brique est la chaux. Celle-ci est le résultat d'une calcination de roches calcaires. Elle se retrouve dans la nature par biométanisation dans les coquillages et les coquilles d'oeufs.

Nous obtenons donc une brique totalement gratuite (chaux et bagasse sont des résidus de matière), naturelle et biodégradable (utilisable en amendement de sol une fois qu'on en a plus besoin sous forme de brique) et plus légère que la brique en béton.

______________________


## Problematic (EN)

- Housing crisis: need to create more and more housing
- Separation of existing houses by partitions
- Construction equipment is insufficient and expensive
- Concrete block partitions = expensive and heavy, not everyone has access to them and they put weight on building structures

### Question

> How can we meet the need for space partitioning for the creation of housing in a way that is accessible to all - inexpensive, easy to do and using the natural resources present in Cuba - and in a light way in order to preserve the structures of the buildings as much as possible, to replace the expensive and heavy concrete blocks used today?

### Hypothesis

We propose a brick partition wall that anyone can build. The bricks use natural resources - vegetable waste - and are interlocking, allowing easy assembly and disassembly, depending on the evolution of the housing.
Plant fibres are the insulation of the brick. They have a very low density, which makes the partition very light, but give it the necessary properties (as useful as concrete) - thermal insulation, soundproofing, mechanical resistance.
The vegetable fibre found in abundance in Cuba is bagasse, the residue of sugar cane stems from which the juice has been extracted.
The binder used in the brick is lime. This is the result of calcination of limestone rocks. It is found in nature by biometanisation in shells and eggshells.

We thus obtain a brick that is totally free (lime and bagasse are residues of matter), natural and biodegradable (can be used as a soil amendment once it is no longer needed in the form of brick) and lighter than concrete brick.

______________________


## Problematica (ES)

- Crisis de la vivienda: la necesidad de crear más viviendas
- Separación de las casas existentes mediante particiones
- Los materiales de construcción son insuficientes y caros
- Tabiques de bloques de hormigón = caros y pesados, no todo el mundo tiene acceso a ellos y pesan sobre las estructuras de los edificios.

### Pregunta
> ¿Cómo responder a la necesidad de separar el espacio para la creación de viviendas de manera accesible para todos -económica, fácil de hacer y utilizando los recursos naturales presentes en Cuba- y de manera ligera para preservar al máximo las estructuras de los edificios y sustituir los costosos y pesados bloques de hormigón que se utilizan hoy?

### Hipótesis

Proponemos un tabique de ladrillos que cualquiera puede construir. Los ladrillos utilizan recursos naturales - residuos vegetales - y son encajables, lo que permite un fácil montaje y desmontaje, dependiendo de la evolución de la vivienda.
Las fibras vegetales son el aislamiento del ladrillo. Tienen una densidad muy baja, lo que hace que el tabique sea muy ligero, pero le da las propiedades necesarias (tan útiles como el hormigón) - aislamiento térmico, aislamiento acústico, resistencia mecánica.
La fibra vegetal que se encuentra en abundancia en Cuba es el bagazo, el residuo de los tallos de la caña de azúcar del que se ha extraído el jugo.
El aglutinante utilizado en el ladrillo es la cal. Este es el resultado de la calcinación de las rocas calizas. Se encuentra en la naturaleza por biometanización en cáscaras y cáscaras de huevo.

De esta forma obtenemos un ladrillo totalmente libre (la cal y el bagazo son residuos de materia), natural y biodegradable (puede ser utilizado como enmienda del suelo una vez que ya no se necesita en forma de ladrillo) y más ligero que el ladrillo de hormigón.

______________________
