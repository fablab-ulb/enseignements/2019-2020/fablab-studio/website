# Game, craft and teaching

## Tutorial

In response to this, there is an interest in making a handcrafted game that every Cuban could have at home and make it easily for children.

### Our goals:
- To use vegetable waste (vegetal fiber: sugar cane bagasse) and thus obtain an alternative solution to plastic toys.

### Create a game that allows the child :
- To create it himself ( or with the help of an adult )
- To use food waste and see it as a material that has potential in the manufacture of objects.
- To become familiar with geometric shapes
- To be imaginative and creative
- To solve problems
- To play with in different ways
- To assemble / disassemble at will
- To create as many pieces as he wishes
- The ideal toy imagined is a ball course that will be made with the recipe of bagasse and potato starch. The ball track will be composed of a repetitive module that will be made from a bagasse dough moulding process inside a mould made for.

![85](images/85.jpg)

### Recipe 1: Bagasse

#### Ingredients:
- Sugar cane
- Knife
- Cutting table
- Container

#### Prep:
### Cutting the sugar cane
- Cut a first portion of sugar cane
- Peel off the outer skin, which is hard and fibrous.
- Cut the portion in two pieces
- Cut into smaller portions

##### Extraction of sugar cane juice
- First possibility is to pass it through a juice extractor.
- The second possibility is to chew the small pieces of sugar cane previously cut in order to absorb all the juice.

### Getting the dried bagasse
- Once the juice has been completely removed, let the fibres rest to dry.
- Drying can be done in the open air for several days, or more quickly with a lighted radiator or over a chimney.

|![53](images/53.jpg)|![54](images/54.jpg)|![55](images/55.jpg)|![56](images/56.jpg)|
| --- | --- | --- | --- |

### Recipe 2 : Bagasse dough + potato starch

#### Ingredients:

- 1 portion of bagasse
- 4 portions of water
- 1 portion of potato starch
- 1 serving of vinegar
- Blue dye ( not required )

#### Prep:

- In a saucepan pour the water, bagasse and vinegar over low heat.
- When the bagasse is softer, pour in the potato starch gradually.
- Once the dough has become gelatinous, turn off the heat.
- Allow to cool
- Pour the dough into a mould

![dessin](images/dessin.jpg)

|![79](images/79.jpg) | ![80](images/80.jpg)|
| --- | --- |

| |Mold|Odour| Hardness| Flexibility| Touchability| Colour|Transparency| Moulding| Drying Time|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Potato starch |not |pleasant |+++++ |+++ |pleasant |green |not |easy |5 hours|

**Notes:** The texture and hardness of the mixture depends on the thickness of the object during moulding. The thinner the object, the more brittle and less flexible it will be. The recipe for potato starch gives satisfactory results to the equivalent of the recipe with agar agar.

### Recipe 3 : Potato starch

#### Ingredients:

- 1 kg of potatoes
- Mixer
- 500 ml of water
- Filter

#### Prep:

- Peel the potatoes to remove the fibrous part.
- Cut the potato into small pieces
- Pour the potato cubes into a blender with the water.
- Mix until homogeneous mixture is obtained
- Filter several times and then let the liquid obtained rest
- Pour the liquid deposit on a cellophane . The deposit corresponds to the starch
- Allow the starch to dry to a dehydrated texture that corresponds to potato starch.

|![81](images/81.jpg)|![82](images/82.jpg)|![83](images/83.jpg)|![84](images/84.jpg)|
| --- | --- | --- | --- |

**Notes:** For 1 kilogram of potato, we get 80 grams of potato starch.
Potato starch is the starch extracted from potato tubers. It is used in technical applications such as wallpaper glue, fabric finishes and coatings, paper coating and coating, and as an adhesive in paper bagging and gummed paper tapes.

## Craft prototype

|![86](images/86.jpg)|![87](images/87.jpg)|![88](images/88.jpg)|
| --- | --- | --- |

## Prototype 1 digital mold

![89](images/89.jpg)

#### What's not working
A first attempt at a three-compartment mould, which was not very conclusive. Once we had put the dough inside the mould because we couldn't extract the dough without damaging it.

## Digital prototype 2 of the mould
We decided to make openings to facilitate the extraction of the dough.

[Tuto Video](https://www.youtube.com/watch?v=bIEQBOz_mDc)
<br>[STL File](STL/moule.stl) - [STL File Cover](STL/moulecover.stl)

|![90](images/90.jpg)|![t1](images/t1.jpg)|
| --- | --- |
|![t2](images/t2.jpg)|![t3](images/t3.jpg)|
|![t4](images/t4.jpg)|![t5](images/t5.jpg)|
|![t6](images/t6.jpg)|![t7](images/t7.jpg)|
|![t8](images/t8.jpg)|![t7](images/t9.jpg)|
|![t10](images/t10.jpg)|![t11](images/t11.jpg)|
|![t12](images/t12.jpg)|![t13](images/t13.jpg)|
|![t14](images/t14.jpg)|![t15](images/t15.jpg)|
|![t16](images/t16.jpg)|![t17](images/t17.jpg)|
|![t18](images/t18.jpg)|![t19](images/t19.jpg)|
|![t20](images/t20.jpg)|![t21](images/t21.jpg)|
|![t22](images/t22.jpg)|![t23](images/t23.jpg)|
|![t24](images/t24.jpg)|![t25](images/t25.jpg)|
|![t26](images/t26.jpg)|![t27](images/t27.jpg)|

![moule](images/moule.JPG)

## Mounting the handcrafted prototype with copper wire rails

![91](images/91.jpg)

## Mounting the digital prototype with printed rails

[Tuto Video](https://www.youtube.com/watch?v=HV4JXKfIEq0)
<br>[STL File](STL/rail.stl)

|![rail](images/rail.jpg) |![r1](images/r1.jpg)|
| --- | --- |
|![r2](images/r2.jpg)|![r3](images/r3.jpg)|
|![r4](images/r4.jpg)|![r5](images/r5.jpg)|
|![r6](images/r6.jpg)|![r7](images/r7.jpg)|
|![r8](images/r8.jpg)|![r7](images/r9.jpg)|
|![r10](images/r10.jpg)|![r11](images/r11.jpg)|
|![r12](images/r12.jpg)|![r13](images/r13.jpg)|
|![r14](images/r14.jpg)|![r15](images/r15.jpg)|
|![r16](images/r16.jpg)|![r17](images/r17.jpg)|

![92](images/92.jpg)

## Final

![final](images/final.JPG)

## Other example of casting

![93](images/93.jpg)
