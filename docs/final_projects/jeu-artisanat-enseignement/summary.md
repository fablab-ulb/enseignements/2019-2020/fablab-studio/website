# Jeu-artisanat-enseignement

![sammary](images/DFS__v6_recovered_.jpg)

### Problématique (FR)

- Les jouets ont commencé à disparaître
- Les jouets sont chères et de très mauvaise qualité
- La disparition de la tradition du « Trois Rois » ( le jour où les enfants reçoivent des cadeaux )
- Jouer, s'amuser et donner vie à ses rêves, c’est devenue une tâche extrêmement difficile à Cuba.
- La déception des Cubains grandit devant le prix des jouets. N'ayant pas la capacité d'acheter des jouets aux enfants, les magasins de jouets se transforment en musée.
- Les enfants trouvent comme excuse de ne pas avoir de jeu chez eux pour pouvoir sortir et jouer à l'extérieur avec presque rien avec les enfants du quartier.
- Malheureusement, certains parents ne permettent pas à leur enfant de sortir.
- Sans la permission de sortir et sans jouet à la maison, les enfants ne possède que leur imagination.

### Question

Par rapport aux problèmes des jouets, on se demande comment est-ce que l’enfant cubain pourrait s'intégrer dans le processus de fabrication de ses jouets , au lieu de  devoir acheter à l’extérieur a des prix inabordables par les parents, tout en appuyer sur leur valeur liée à la création artisanale, fait maison ?

### Hypothèse :

Sachant que les enfants et les adultes sont très compétents en matière de fabrication artisanale et de créativité, nous avons décidé que le jouet idéal serait un jeu:

Que l’enfant pourra créer lui-même avec ses parents, ses voisins, ses professeurs
Qui lui permettra d’être créatif, inventif, imaginatif. .
Qui sera composé de pièce rapidement remplaçable en cas de cassure, ou de perte.
A partir de ce début de réponses, l’intérêt s’est porté aux matériaux présents sur place. Pour cela, la méthode a été basée sur une recherche dans les deux livres mis à dispositions durant ce quadrimestre: “ Con nuestros propios esfuerzos “ et “ El libro de la familia” .

### Nos objectifs :

•	Utiliser la matière des déchets végétaux (fibre végétal : la bagasse de canne à sucre)
<br>•	Trouver la recette idéale avec le mélange de bagasse
<br>•	Au niveau de la fabrication digitale : Créer un moule qui pourrait être remplacé de manière artisanale très simplement
<br>•	Avoir un moulage et démoulage facile à réaliser

### Quel type de jeu ?

Un jeu qui permet à l’enfant:
<br>•	De le créer lui-même ( ou avec l’aide d’un adulte )
<br>•   D’utiliser les déchets alimentaires et les voir comme un matériel qui a du potentiel dans la fabrication d’objet
<br>•   De se familiariser avec les formes géométriques
<br>•   D’être Imaginatif et créatif
<br>•   De résoudre des problèmes
<br>•   De jouer avec de différentes manières
<br>•   De le Monter / le démonter à son gré
<br>•   De créer autant de pièces qu’il le souhaite
<br>•   Le jouet idéal imaginé est un parcours de bille qui sera fabriqué avec la recette de bagasse et de fécule de pomme de terre. Le parcours de bille sera composé d’un module répétitif qui sera fabriquer à partir d’une procédure de moulage de pâte de bagasse à l’intérieur d’un moule fabriqué pour.

______________________

### Problematic (EN)

- The toys started to disappear
- The toys are expensive and of very poor quality
- The disappearance of the "three kings" (the day when the children will have toys as a gift)
- Play, have fun and give life to their fantasies. It is something that has become an extremely difficult task in Cuba.
- The disappointment of Cubans is growing at the price of toys on the island.
- For children, it is always a good excuse to meet friends, play and have fun in a peaceful environment (The absence of toys at home)

### Question

> Regarding the problems of toys, we wonder how could the Cuban child sneak into the manufacture of his toys having to buy it outside at prices unaffordable by the parents, while supporting their value linked to artisanal creation, homemade?

### Hypothesis :

In response to this, we are interested in artisanal manufacturing and the tools that every Cuban child could find at home.

### Our goals :

• Use the material of plant waste (vegetable fiber: sugar cane bagasse)
<br> • Find the perfect recipe.
<br> • At the digital manufacturing level: Create a mold.
<br> • Mold the dough obtained for the creation of the game.

### What type of game?

A game that allows children to:
<br> • From creating it himself or with his parents or at school.
<br> • To become familiar with geometric shapes.
<br> • To be Imaginative and creative.
<br> • To solve problems
<br> • To play with different ways
<br> • To assemble / disassemble it as desired.
<br> • If one of its parts breaks or gets lost, it can make it.

______________________

### Problemática (ES)

- Los juguetes comenzaron a desaparecer
- Los juguetes son caros y de muy mala calidad
- La desaparición de los "tres reyes" (el día en que los niños tendrán juguetes como regalo)
- Jugar, divertirse y dar vida a sus fantasías. Es algo que se ha convertido en una tarea extremadamente difícil en Cuba.
- La decepción de los cubanos está creciendo al precio de los juguetes en la isla.
- Para los niños, siempre es una buena excusa para encontrarse con amigos, jugar y divertirse en un ambiente tranquilo (La ausencia de juguetes en casa)

### Pregunta

Con respecto a los problemas de los juguetes, nos preguntamos cómo podría el niño cubano colarse en la fabricación de sus juguetes teniendo que comprarlos afuera a precios que los padres no pueden pagar, al tiempo que aumentan su valor. vinculado a la creación artesanal, casera?

### Asunción:

En respuesta a esto, estamos interesados ​​en la fabricación artesanal y las herramientas que todo niño cubano podría encontrar en casa.

### Nuestros objetivos:

• Utilice el material de los desechos vegetales (fibra vegetal: bagazo de caña de azúcar)
<br> • Encuentra la receta perfecta.
<br> • A nivel de fabricación digital: cree un molde.
<br> • Moldear la masa obtenida para la creación del juego.

### Qué tipo de juego?

Un juego que permite a los niños:
<br> • De crearlo él mismo o con sus padres o en la escuela.
<br> • Familiarizarse con las formas geométricas.
<br> • Ser imaginativo y creativo.
<br> • Para resolver problemas
<br> • Para jugar de diferentes maneras
<br> • Para montarlo / desmontarlo como desee.
<br> • Si una de sus partes se rompe o se pierde, puede hacerlo.
