# Jeu, artisanat et enseignement

Á Cuba certains magasins offrent aux parents la possibilité d’acheter des jouets à leurs enfant. Mais à quel prix ? Sont-ils abordables pour les parents ?

### La fête des Trois Rois

![1](images/1.jpg)

La tradition du “Three Kings” a disparu de l’île alors qu’elle fût une fête très répandue.
Cette fête rend hommage à la venue des trois rois mage à Bethléem, Gaspard, Melchior et Balthazar. Elle est fêtée le 6 Janvier et est l’équivalent de l’Epiphanie.
Célébrant l'adoration des Rois Mages, venus offrir des présents à la naissance de Jésus.. Dans son usage populaire, elle est célébrée dans certains pays, comme chez nous, par l'achat d'une galette des rois.

"Dia de reyes «, la fête des “ Trois Rois”, est une tradition venue de l'Espagne et qui se célèbre presque dans tous les pays d’Amérique du Sud. C'est le jour où les trois rois arrivent à Bethléem avec de cadeaux pour Jésus. Ce jour-là, les parents cubains offraient des cadeaux aux enfants.

A Cuba, après la révolution en 1959, comme Fidel Castro avait interdit la religion à Cuba et avait dit que Noël été une tradition capitaliste, les cubains avaient arrêté de célébrer Noël. Les décorations et les sapins avaient disparu. Après la visite du pape Jean Paul II le gouvernement a déclaré le 25 décembre comme étant une journée fériée à Cuba.
Sans fêter Noël. L'équivalent de Noël est la nuit de l'an à Cuba. Toutes les cubains vont fêter et rendre visite à leurs familles, amis et elle est considérée comme la plus grande célébration familiale de l'année.

### Le jouet à Cuba

Alors que dans le passé,  les parents étaient en capacité d’offrir des jouets à leurs enfants, aujourd’hui la situation est complètement inversée et les parents sont désespérés face à cette hausse des prix des jouets.

|Écrit dans une publication d’ Havana Time : <br>*“À mon époque, nous étions toujours dans l’enthousiasme des «Three Kings» et des cadeaux qu’ils apporteraient, ce qui n’était rien de plus qu’un fantasme, mais très agréable pour tous les enfants qui dormaient le 5 janvier, attendre que les rois arrivent avec leurs cadeaux. Au fil du temps, le magnifique fantasme qu'avaient les enfants commençait à se perdre présente. Par la suite, les jouets ont également commencé à disparaître du marché national dans une période de crise catastrophique, ici à Cuba. Et nous avons vécu avec des pénuries pendant de nombreuses années.”* par Miguel Arias Sanchez|![2](images/2.jpg)|
| --- | --- |

|![3](images/3.jpg)|_"Avant, les jouets coûtent 15 cents et maintenant, ils coûtent 5 cuc", ajoute un père qui souligne le drame vécu par ces familles où elles ont plus d'un enfant: "Un jouet pour trois enfants, ce n'est pas du jambon, mais ce qu'ils font fajar . (...) Dans ce magasin, il est possible de trouver une Barbie à 30,95 CUC (773,75 CUP) = 28 euro, des bébés à 65,95 CUC (1648,75 CUP) ou un avion à 92,95 CUC (2323,75 CUP) " _|
| --- | --- |

|Certains jouets valent l’équivalent de trois salaires mensuels. Les parents sont dans l’incapacité d’offrir un jeu à leurs enfants. Dans les quartiers les plus pauvres, certains foyers se partagent des jouets afin de permettre aux enfants de s’épanouir.<br><br>La production nationale industrielle est de mauvaise qualité. Les seuls magasins vendant des jouets se transforment en musée  à cause des prix inabordable. Les parents et les enfants vont regarder les jouets et prennent des photos avec ces jeux exposés , et ne finissent jamais par les acheter.|![4](images/4.jpg)|
| --- | --- |


### Comment les enfants cubains jouent-ils aujourd’hui ?

La plupart des enfants jouent dans les rues avec les autres enfants du quartier. Les jouets avec lesquels ils jouent sont fabriqués de manière artisanale. Ils sont issus de matériaux recyclés ou d’objets qui ne leur servent plus dans leur quotidien.
Dans les quartier populaire, les enfants ne jouent pas à la playstation, ou aux jeux vidéos comme ailleurs dans le monde. Ils jouent avec ce qu’ils trouvent, parfois avec un simple caillou ou une bouteille en plastique. Libre court à leur imagination.

|![5](images/5.jpg)|![6](images/6.jpg)|
| --- | --- |
|![7](images/7.jpg)|![8](images/8.jpg)|
|![9](images/9.jpg)|![10](images/10.jpg)|

Cette série de photo, remportée lors du prix **People's Choice Prize** de l’édition 2014 The Fence, Boston, lors de la Photoville à New York. , montre la vie quotidienne des enfants cubains. Ils développent des liens très noués avec leurs voisins. Par faute de moyen, et de jeux d’intérieur, les enfants trouvent toujours une bonne excuse pour sortir et jouer à l’extérieur. Ce qui leur permettent de rencontrer des amis, de jouer et de s’amuser dans un environnement paisible et sécurisé.

### Les jeux traditionnels cubains

|Le jouet de tous les temps est le chivichana. Créé à la base par les enfants cubains pour s'amuser à partir de matériaux recyclés et quelques morceaux de bois, les adultes se sont inspirés pour fabriquer un chivichana version adulte pour se déplacer ou déplacer des charges lourdes. Le chivichana est une sorte de skateboard recyclé|![11](images/11.jpg)|
| --- | --- |

|![12](images/12.jpg)|Le jeu pour les garçons est le jeu de billes. Tous les garçons y ont joués dans les rues avec les autres enfants du quartier. souvent le perdant lègue sa bille au gagnant à la fin de la partie de jeu.|
| --- | --- |

|La toupie est très répandue également et de tous les temps. Il est souvent fabriqué en plastique, ou en bois. Vendu pour 5 à 10 pesos.|![13](images/13.jpg)|
| --- | --- |

|![14](images/14.jpg)|Le Yaquis est joué par tous les enfants à l’école lors de la récréation de 15 minutes. *C’est un jeu très populaire. Il est composé de 12 morceaux en plastique coloré et une balle en caoutchouc. Les règles de base sont simples: il faut jeter le yaquis sur le sol et lancer  la balle en l'air. Le but est d’attraper un maximum de yaquis d'une seule main avant que la balle ne rebondisse, puis vous ramassez la balle avec la même main.*|
| --- | --- |

|La Pañoleta  est un jeu de bataille entre deux groupes qui consiste à attraper un foulard situé au centre sans se faire prendre saisir l'écharpe par leur adversaire. |![15](images/15.jpg)|
| --- | --- |

|![16](images/16.jpg)|Cerf-volant tout comme le chivichana, de nombreux enfants préfèrent fabriquer leur propre cerf-volant. Ils le fabriquent souvent avec leur parent à partir de de très fines tiges de bois et de papier. On les retrouve la plupart du temps des enfants jouant au cerf-volant près de la côte. |
| --- | --- |

|Les dominos à Cuba sont sacrés et se jouent dans tous les coins de rue. <br>Comme le dit si bien le dicton: "il n'y a pas de samedi sans soleil, et pas de fête cubaine sans dominos".<br>Les dominos sont un jeu de société dans lequel des tuiles rectangulaires divisées en deux carrés sont utilisées, chacune étant marquée de zéro à neuf points.|![17](images/17.jpg)|
| --- | --- |

D’autre jeux de sociétés:  jeu de l’oie, jeu de dame, l’échec, touche-touche, cache-cache, quatre coins, etc.

### A l’ère du numérique et des jeux vidéos à Cuba

![18](images/18.jpg)

Les premiers réseaux sociaux cubains ont été créé par les enfants qui jouaient avec la cable intranet du téléphone. D’ailleurs il est illégal au Cuba d’en utiliser. Les policiers guettent les rues à la recherche de ces câbles qui voyagerait à travers différentes maisons dans un quartier.

Des clubs d’informatiques existent dans certaines villes à Cuba. Les enfants peuvent accéder à internet à partir de  1 CUC. Ils ont accès au Wi-fi ainsi qu’à des formations informatiques dans lesquels ils apprennent à coder, ou alors à a se former sur certains logiciels. La plupart de ces formations ne sont pas payantes. Mais il est nécessaire de posséder une carte de résident cubain pour pouvoir accéder à ces cours.

Cuba possède aujourd’hui une connexion beaucoup plus accessible qu’avant. Les cubains commencent à prendre les habitudes que nous avons chez nous.

Les enfants et les jeunes ne sont pas épargnés par cette tendance. Il est courant que les enfants demandent de plus en plus à leur parents en cadeau des jeux vidéos, playstations et téléphone portable. A Cuba, certains parents peuvent se permettre d’offrir des jeux vidéos à leurs enfants grâce à l’aide fournit par leur famille qui vivent à l’extérieur de Cuba.

**Les enfants commencent à perdre l’habitude de jouer aux jeux traditionnels faute à l’ère informatique principalement, mais pas que…**

A partir d’un certain âge,  les enfants commencent à avoir envi de courir dans tous les sens surtout à  l’extérieur, Certains parents ont peur pour leurs enfants, et ne leur permettent pas de jouer à l’extérieur.  Un garçon témoigne:

_«J'aime jouer à la toupie, au ballon… mais presque jamais mes parents ne me quittent parce qu'ils disent que c'est dangereux. C'est pourquoi je suis presque toujours sur l'ordinateur », a-t-il déclaré._

*“ll y a longtemps, il était courant de voir des quartiers jouer des hauts, des balles, monter des poussettes, des cerfs-volants raides; (mais) les intérêts des petits ont changé. Les parents ne leur inculquent plus ce type de passe-temps, certains enfants préfèrent d'autres options . (...) "aujourd'hui c’est Nintendo ou la PlayStation, l'ordinateur ou la tablette. “*

### Les jeux traditionnels face à la technologie

Les jeux traditionnels rapportent de la sagesse et de l'expérience qui sont transmises naturellement. Ils renforcent la coopération et l'apprentissage des règles de comportement collectif, d'une discipline.

_“Aujourd'hui, les enfants ne veulent pas se séparer de la télévision parce que les parents eux-mêmes les ont utilisés. Il est plus facile de les asseoir sur un canapé et de mettre des vidéos pour gagner du temps dans les tâches ménagères. Et je comprends même que les pères et les mères s'adaptent à cette méthode de neutralisation qu'est la télévision, car vraiment notre vie moderne ne nous laisse pas beaucoup de temps. Même les adultes, parfois, nous préférons communiquer par e-mail ou SMS plutôt que par téléphone ou parler personnellement.”_

Les enfants perçoivent tous les comportement et gestes. Ils ne sont pas absents des  habitudes qu’ont les parents, ils sont une réplique de comportement qu’ils observent.

Les jeux traditionnels pour enfants sont des mécanismes qui contribuent à générer dans les processus individuels d'identification avec leur communauté, avec leur région, avec leur pays, à partir des mécanismes de socialisation qui leur sont inhérents.

Le jeu pour l’enfant lui permet de surmonter ses peurs et à s’épanouir  tout en apprenant à connaître ses limites et à les dépasser dans l’adversité et dans le jeu de société.

### Enquête à  travers les réseaux sociaux

![19](images/19.jpg)

Pour connaître d’avantage sur le sujet des jeux cubains, la meilleure chose à faire est d’entrer en contact avec un témoin.
Concrètement, les recherches ont débuté directement sur les réseaux sociaux pour pouvoir rentrer en contact avec les cubains. Ils ont été très réceptifs et nous ont permis de comprendre davantage à propos du jeu chez l’enfant cubain.

D’abord sur Facebook. L’avantage de ces réseaux est de pouvoir profiter de témoignage en directe de vraies personnes qui racontent leurs expériences, leurs vécues, et leurs souvenirs lorsqu’ils vivaient encore à  Cuba . Cela nous permet de comprendre mieux la situation et leurs quotidiens.

| ![19-1](images/19-1.jpg) <br> ![19-2](images/19-2.jpg) | ![19-3](images/19-3.jpg) |
| --- | --- |
| ![19-4](images/19-4.jpg) | ![19-5](images/19-5.jpg) |

Puis une recherche un peu plus approfondie à travers Instagram. L’avantage est de pouvoir profiter d’un large témoignages photographiques faits par les voyageurs, par les habitants, et par des chaînes de journaux.

| ![20](images/20.jpg) | ![21](images/21.jpg) | ![22](images/22.jpg) |
| --- | --- | --- |
| ![23](images/23.jpg) | ![24](images/24.jpg) | ![25](images/25.jpg) |
| ![26](images/26.jpg) | ![27](images/27.jpg) | ![28](images/28.jpg) |
| ![29](images/29.jpg) | ![30](images/30.jpg) | ![31](images/31.jpg) |

## Problématique

Alors que partout dans le monde, les enfants jouent à des jeux informatiques, sur leurs téléphones portables, tablettes ou ordinateurs portables, à Cuba, la rue et le quartier occupent encore une place centrale et importante pour la vie des enfants.

Les rues  pour les la plupart des quartiers, sont des endroits sûrs, où les enfants peuvent développer leur fantaisie et se faire des amis. La plupart des jeux de rues sont des jeux qui se jouent à plusieurs, ce qui leur permet de créer un lien fort avec les enfants du quartier.

Malgré cette confiance qu’ont les parents en leur quartier, certains parents préfèrent avoir leurs enfants près de chez eux à la maison pour plus de sécurité.

Avec l’embargo, les jouets ont commencé à se faire rare ou de mauvaise qualité sur l’île, jusqu’à petit à petit disparaître dans la vie de la plupart des enfants cubains.

Des magasins de jouets ont commencé à se mettre en place, mais malheureusement avec le prix qu’ils demandent pour un jouet, qui équivaut parfois jusqu’à un triple salaire cubain, ces magasins se transforment au final en musée d’expositions où les parents et les enfants s’y promènent et se prennent en photo.

Dans certains foyers cubains, les enfants ont la possibilité de jouer avec des jeux modernes. Ces jeux sont arrivés à Cuba grâce à leurs connaissances, ou familles qui ont immigré à l’extérieur de l’île. Certains ont la possibilité, toujours avec l’aide venant de l’extérieur de l’île, d’offrir des jeux informatiques, qui sont hors de prix pour d’autres parents.

**La problématique se porte sur la question du jeu  artisanal chez l’enfant qui ne peut pas sortir à l’extérieur. Mais qui sera destinés également  à tous jeunes enfants et adultes. Un jeu qui permettrait à l’enfant de s'épanouir personnellement  et d’élargir son champ d’ apprentissage.**

###  Des questions se posent très rapidement

Comment fabriquer un jouet idéal qui touche l’artisanat et l'enseignement en même temps tout en gardant l’identité de la fabrication artisanale cubaine ?
Quels matériaux utilisés pour que n’importe quel cubain puisse le fabriquer d’une manière artisanal ou numérique s’il possède les outils?
Comment le jeu sera composé ?
Quelle dimension sera le jeu ?
Quel serait le jouet idéal qui permettrait á l’enfant de se projeter ? d’imaginer ? De parcourir et de fabriquer ?
Qu'est-ce qui manque à Cuba Qu'est-ce qu'on pourrait leur rapporter alors que les cubains sont déjà très créateur et habile des mains?
A quel besoin notre projet va-t-il répondre?

## Hypothèse

Sachant que les enfants et les adultes sont très compétents en matière de fabrication artisanale et de créativité, nous avons  décidé que le jouet idéal serait un jeu:

* Que l’enfant pourra créer lui-même avec ses parents, ses voisins, ses professeurs
* Qui lui permettra d’être créatif, inventif, imaginatif. .
* Qui sera composé de pièce rapidement remplaçable en cas de cassure, ou de perte.

A partir de ce début de réponses, l’intérêt s’est porté aux matériaux présents sur place.  Pour cela, la méthode a été basée sur une recherche dans les deux livres mis à dispositions durant ce quadrimestre: _“ Con nuestros propios esfuerzos “_ et _“ El libro de la familia”_ .

### Con nuestros propios esfuezos et El libro de la familia


| Articles concernant le jeu dans le livre: Con nuestros propios esfuezos | Articles concernant le jeu dans le livre: El libro de la familia |
|---|---|
| **Chapitre 10 : Culture, sport et récréation**<br>Balle de Baseball - 210 / 01<br>Jeu de Baseball - 210 / 02<br>Fabrication de battes - 210 / 03<br>Mitte pour jouer au baseball - 211 / 01<br>Gym rustique - 211 / 02<br>Jeu de Parcheesi - 211 / 03<br>Jeu de poids - 212 / 01<br>Barre fixe - 212 / 02<br>Jeu d'échecs - 212 / 03<br>Jeu de dames - 212 / 04<br>Jeu des anneaux - 212 / 05<br>Équipement de raccordement de cordes pour guitares - 213 / 01<br>Boitier électrique pour connexion simultanée - 214 / 01<br>Tableau électrique - 214 / 02<br>Maracas - 214 / 03<br>Fixation de microphone sur podium - 214 / 04<br>Le camping dans la belle traversée - 215 / 01<br>Expériences dans l'éducation, la culture et le sport - 215 / 02<br><br> **Chapitre 11 : Enseignement**<br>Méthodes d'enseignement pour l'amélioration de la deuxième année de vie - 218 / 1<br>Marionnettes et animaux pour activités indépendantes - 218 / 2<br>Tambourins - 218 / 3<br>Jouet en forme de hoche - 218 / 5<br>L'aquarium de la connaissance - 219 / 1<br>Feuille magnétique - 219 / 3<br>La poupée Mama Inés : 220 / 1<br>Jeu pour enseigner le calcul mathématique - 221 / 1<br>Moyens d'enseignement avec du papier mâché - 221 / 4<br>Porte-chiffres - 222 / 1<br>Jeu didactique de Dominos - 222 / 2<br>Jeux et moyens de jouer - 222 / 3<br>Château de Blanche Neige - 222 / 4<br>Puzzles - 222 / 5<br>Corde à sauter - 223 / 1<br>Marionnettes à manipuler avec les doigts - 223 / 2<br>Craie de chaux - 223 / 3<br>Colle de résine de cèdre ou d'acajou - 223 / 4<br>Crayons - 224 / 1<br>Confection de pinceaux - 224 / 2<br>Modèle du relief de Cuba - 224 / 3<br>Structure interne de la terre - 225 / 1<br>Tableau portable - 225 / 3<br>Maquette Globe Terre-Soleil - 225 / 4<br>Cartes pour l'enseignement des malvoyants - 225 / 5<br>Boules de polyfoam - 226 / 1<br>Volleyball - 226 / 3<br>Pistolets de sport rustiques - 227 / 1<br>Tableau de démantèlement de la pompe d'injection à quatre emplois - 227 / 2<br>Extracteurs pour vannes de refoulement dans les pompes à injection - 228 / 1<br>Clés à anneau renforcées pour équipement lourd - 228 / 2<br>Rose trigonométrique – 228 / 3<br>Modèle didactique pour l'unité motrice - 230 / 2<br><br> **Chapitre 12 : Jouets et artisanat** <br>Jouets - 250 / 01 <br>Dauphins - 235 / 05<br>Poupée conteneur - 235 / 06<br>Outil pour faire des objets à la main - 236 / 01 | <br> Poupées de chiffon - 340 / 1<br>Comment faire une poupée avec un sac - 342/ 1<br>Comment faire des marionnettes  - 342/ 1<br>Comment faire un puzzle - 348/1<br>Comment faire un petit train - 348/2<br>Bâton Tambourine - 348/3<br>Comment fabriquer du papier à partir de déchets de papier - 254/1<br>Comment faire la presse - 254 / 2<br>Comment décorer une bouteille avec du papier mâché - 352 / 1<br>Comment fabriquer un tambour - 350/ 1<br>Ensemble salon  - 350/ 1<br>Comment faire des bijoux - 350/ 1|


### Matériaux retirés des livres

Après avoir recherché les matériaux disponibles sur place, nous avons fait un tri des matériaux intéressants à utiliser pour les expérimentations.

- Tissus recyclé à partir de vêtement, drap, etc.
- Fibres végétales ( banane, cidre, feuille de vigne, Ova, canne à sucre,
- Contreplaqués ( peu en quantité )
- Papier mâché,
- Papier journal
- Fil de cuivre
- Canette
- Bouteille en plastique
- Cellophane,
- Carton ( peu en quantité )
- Brique de lait ( peu en quantité )
- Sac de jute

## Prototypages

Nous avons procédé en deux méthodes: artisanale et digitale.

### Prototype 01

Après avoir visionné une [vidéo](https://www.youtube.com/watch?v=WHD-Y0ST7-E) d’un touriste offrant une voiture miniature moderne à un enfant, le jouet a procuré une telle joie à  l’ enfant qu’il se sépare de son jouet artisanal qu’il avait en main.  En réaction à cette vidéo, nous avons tenté une première expérience artisanale. Le but était de créer des jouets à partir de matériaux très basiques qu’on trouve chez nous.

Matériaux utilisés:

Bâtonnet de bois: afin de créer la structure, qui pourrait être remplacé par des tiges finement coupée d’un tronc ou morceau de bois, ou du carton
élastique: pour créer un mouvement lors du déplacement, qui pourrait être remplacé par une ficelle,
bouchons de bouteille en plastique : pour créer des roues et faciliter un déplacement.
de la colle
des pailles, qui pourrait être remplacé par des pailles en papier.

### Artisanal

Nous avons créé deux voiturettes et une toupie

![47](images/47.jpg)

### Digital

Nous avons pensé à un jeu de construction avec des pièces qui permettraient d’assembler différentes modèles. Nous avons réussi à monter 3 versions de voitures différentes.

![48](images/48.jpg)

##### Ce qui ne fonctionne pas dans le prototype 1

D’après Ernesto Oroza, chaque famille cubaine possède plus ou moins 6 bouteilles en plastique. Utiliser des matériaux qui sont à la base très rare poserait un problème dans la conceptualisation du projet de jeu pour enfant. De plus, dans la version digitale, le problème serait de créer pour chaque pièce un moule qui permettrait de créer les pièces.
Le nombre élevé de moules, et ensuite d’injection du plastique afin de créer ces pièces ne résoudrait aucun manque sur l’île.
De plus, un jeu de construction deviendra commercialiser très rapidement à Cuba. On revient donc au même problème de départ, qui est le prix inabordable de jeu destiné aux enfants.

### Prototype 2

Nous nous sommes dirigés vers des matériaux que les cubains pourraient trouver chez eux. D’après le livre que nous avons traduit, Con nuestros propios esfuezos , les fils de cuivre seraient présent à Cuba.

### Artisanal

![49](images/49.jpg)

Matériaux utilisés:

- Fil de cuivre
- Une pince pour plier le fil

Le but est de créer et comprendre le processus d’articulation et de mouvement dans un jouet tout en utilisant le même matériau de différentes épaisseurs.

### Prototype 3

### digitale


![50](images/50.jpg)
D’après l’article de journal, l’école permettrait d’inculquer les valeurs et les traditions qui commencent à se perdre face à la technologie. Nous avons pensé à un jeu qui pourrait être fabriqué à l’école, et être enseigné dans les  branche telle que la géographie et l’histoire, où les enfants pourraient fabriquer un globe terrestre en forme de puzzle compliqué.

Nous avons donc imprimé un modèle existant sur [thinkiverse](https://www.thingiverse.com/thing:3401688) d’un puzzle casse tête en forme de globe. L’idée est de créer des moules qui permettrait de créer le globe avec les enfants et les enseignants.

##### Ce qui ne fonctionne pas

Le globe c’est un jouet qui va permettre aux enfants de voyager et connaître tous les pays du monde. Le désavantage est d’offrir un jouet aux enfants qui les fera espérer de pouvoir un jour voyager autour du monde. Le seul problème c’est que l’île est complètement isolée et en embargo. Seules les personnes ayant une double nationalité à la permission de sortir de l’île librement. Le prototype 3 serait un projet qui vendait du rêve et de l’espoir à un peuple rempli  de gentillesse.


##### Ce qui fonctionne

Le globe c’est un jouet qui va permettre aux enfants de voyager et connaître tous les pays du monde. Avec l’école, ça serait un jeu qui leur permettra d’avoir énormément de connaissance et de technique de moulage.

## Canne à sucre

![51](images/51.jpg)

Nous nous sommes penchés sur des matériaux qui ne seraient pas utilisés. Nous avons pensé aux déchets végétaux, principalement à ceux de la canne à sucre.
Sachant que Cuba était jusqu’aux années 70’ le 6e producteur mondial de sucre et que la canne à sucre occupe le tiers de la surface cultivée du pays, nous avons décidés de procéder nos essais avec la canne à sucre.

### Bagasse

![52](images/52.jpg)

La bagasse est composée des résidus fibreux issus du broyage (écrasement) de la canne à sucre coupée pour l'extraction du jus de canne. La bagasse représente environ 30 % du poids de canne coupée amenée en usine. Son taux d'humidité se situe entre 40 et 50 %, et elle contient encore une petite quantité de sucre résiduel. La bagasse séchée est composée pour moitié de cellulose, l'autre moitié étant principalement de l'hémicellulose et de la lignine.

### Utilisations de la bagasse en industrie
Environ 60 % de la production mondiale est utilisée comme combustible dans les sucreries,  pour chauffer les fours et pour la production d'électricité.
Elle est également utilisé pour la fabrication de papier, des panneaux de particules, de la litière pour les animaux, servir de nourriture pour le bétail, et pourrait  être valorisé comme base de compost.

### Recette 1: Bagasse

##### Ingrédients:
- Canne à sucre
- Couteau
- Plateau de découpe
- Récipient

##### Préparation:
##### Découpe de la canne à sucre
- Couper une première portion de canne à sucre
- Éplucher la peau extérieure, qui est dure et fibreuse
- Couper la portion en deux morceaux
- Couper en plus petites portions

##### Extraction du jus de canne à sucre
- Première possibilité est de le passer dans un extracteur de jus de fruit
-  Deuxième possibilité est de mâcher les petits morceaux de canne à sucre coupé antérieurement afin d’absorber tout le jus

##### Obtention de la bagasse séchée
- Une fois le jus entièrement enlevé, laisser reposer les fibres afin qu’ils sèchent
- Le séchage peut se faire à l’air libre durant plusieurs jours ou alors d’une manière plus rapide à l’aide d’un radiateur allumé ou au dessus d’une cheminée

|![53](images/53.jpg)|![54](images/54.jpg)|![55](images/55.jpg)|![56](images/56.jpg)|
| --- | --- | --- | --- |

### Recette 2 : Pâte à la bagasse + agar agar

Le but est de fabriquer une pâte malléable avec la bagasse afin de pouvoir le mouler

##### Ingrédients :
- 5 portions de bagasse
- 4 portions d’eau
- 1 portion d’agar agar (l’agar-agar est une substance mucilagineuse transparente. On l’obtient à partir de certaines espèces d’algues rouges du genre Gelidium )

##### Préparation :
- Mélanger le tout dans une casserole à feu doux
- Mixer afin d’obtenir un mélange homogène
- Laisser jusqu’à l’évaporation totale de l’eau.

|![57](images/57.jpg)|![58](images/58.jpg)|![59](images/59.jpg)|
| --- | --- | --- |
|![60](images/60.jpg)|![61](images/61.jpg)|![62](images/62.jpg)|

##### Moulage :

- Dans un récipient avec une forme spécifique souhaité , venir déposer le mélange obtenue avec la bagasse
- Laisser reposer quelques minutes, jusqu’à obtention d’une pâte dure
- Laisser sécher  jusqu’à ce que la pâte sera déshydratée ( plusieurs jours si séchage naturelle, sinon plusieurs heure au dessus d’une cheminée pour un séchage rapide )

|![63](images/63.jpg)|![64](images/64.jpg)|
| --- | --- |

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Agar Agar|Non|Aucune|+++++|+++++|agréable|Beige|Non|facile|5 heures|

**Remarques:** Facilité de manipulation de la pâte de bagasse mélangé à l’agar agar. Le désavantage c’est qu’il n’existe pas de variante d’agar agar à Cuba. La pâte fonctionne très bien pour une procédure de moulage

### Recette 3 : Pâte à la bagasse + feuille de gélatine

##### Ingrédients:
- 5 portions de bagasse
- 4 portions d’eau
- 2 feuilles de gélatine

##### Préparation:
- Verser la bagasse et l’eau dans une casserole à feu doux
- Mélanger la gélatine avec de l’eau froide
- Une fois la gélatine ramollie, l'ajouter dans la casserole
- Remuer jusqu’à ce qu’eau s’évapore un maximum
- Verser dans un moule le mélange
- Laisser reposer et refroidir afin que le mélange durcisse
- Laisser sécher  jusqu’à ce que la pâte sera déshydratée ( plusieurs jours si séchage naturelle, sinon plusieurs heure au dessus d’une cheminée pour un séchage rapide )

|![65](images/65.jpg)|![66](images/66.jpg)|
| --- | --- |

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Gélatine|Non|Aucune|+++|+++|agréable|Beige|Non|facile|5 heures|

### Recette 4 : Pâte à la bagasse + Farine

##### Ingrédients:

- 5 portions de bagasse
- 4 portions d’eau
- 2 portions de farine

##### Préparation:

- Mélanger la bagasse et la farine dans un récipient
- Verser l’eau petit à petit jusqu’à obtention d’une pâte malléable
- Placer la pâte dans un moule
- Laisser sécher

|![67](images/67.jpg)|![68](images/68.jpg)|
| --- | --- |

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Farine|Non|Aucune|+++|- - -|dur|Beige|Non|collant|une journée|

### Recette 5 : Pâte à la bagasse + Fécule de maïs

##### Ingrédients :

- 5 portions de bagasse
- 4 portions d’eau
- 2 portions de fécule de maïs

##### Préparation :

- Verser l’eau et la bagasse dans une casserole
- Mélanger jusqu’à ce que la bagasse soit tendre ( jusqu’à 15 minutes à feu doux)
- Verser petit à petit la fécule de maïs dans la casserole en mélanger sans s’arrêter jusqu’à obtention d’une pâte malléable

|![69](images/69.jpg)|![70](images/70.jpg)|
| --- | --- |

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Fécule de maïs|oui|Aucune|---|+++|friable|Beige|Non|cassant|une journée|

**Remarques:** Cette première recette à base de fécule de maïs est un échec. Lors du moulage de la pâte de fécule de maïs mélangé à la bagasse, la pâte devient vite friable. Le démoulage est difficile , voir impossible sans que la pâte se casse.

### Recette 6 : Pâte à la bagasse + Fécule de maïs

##### Ingrédients :

- 1 portions de bagasse
- 4 portions d’eau
- 1 portions de fécule de maïs
- 1 cuillère à soupe de vinaigre
- colorant bleu ( pas obligatoire )

##### Préparation :

- Verser l’eau, la bagasse et  le vinaigre dans une casserole
- Mélanger jusqu’à ce que la bagasse soit tendre ( jusqu’à 15 minutes à feu doux)
- Verser petit à petit la fécule de maïs dans la casserole en mélanger sans s’arrêter jusqu’à obtention d’une pâte malléable.

|![71](images/71.jpg)|![72](images/72.jpg)|
| --- | --- |
|![73](images/73.jpg)|![74](images/74.jpg)|

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Fécule de maïs + vinaigre|non|Aucune|+++|+++|dur|Beige|Non|Facile|une journée|
|Fécule de maïs + vinaigre + colorant|non|Aucune|+++|+++|dur|Vert|Non|Facile|une journée|

**Remarques :** l’ajout du vinaigre permet une meilleure malléabilité de la pâte et ne casse pas lors du séchage. Une fois le colorant bleu ajouté dans le mélange, la couleur devient verte.

### Recette 7 : Pâte à la bagasse + Résine

##### Ingrédients :

- 1 portions de bagasse
- 4 portions d’eau
- 3 cuillère à soupe de résine d’arbre naturelle

##### Préparation :

- Verser l’eau et la bagasse dans une casserole et mettre à feu doux
- Mélanger jusqu’à ce que la bagasse devienne plus tendre et que l’eau brunisse
- Mixer la résine d’arbre afin d’obtenir une poudre
- Ajouter la poudre de résine d’arbre dans la casserole
- Mélanger jusqu’à obtention d’une pâte malléable

|![75](images/75.jpg)|![76](images/76.jpg)|
| --- | --- |

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Résine|non|fraîche|++++|++|dur|brun clair|Non|Facile|une journée|

**Remarque :** bien mélanger pour ne pas avoir de grumeaux . S’il y a la présence de grumeau au séchage, l’objet risque d’avoir moins de dureté et sera plus facilement cassable.

### Recette 8 : Pâte à la bagasse + Graines de basilic

##### Ingrédients :

- 1 portions de bagasse
- 4 portions d’eau
- 3 cuillère à soupe de graines de basilic moulues

![77](images/77.jpg)

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Résine|non|agréable|++++|+|cassable|brun |oui|Facile|une journée|

### Recette 9 : Pâte à la bagasse + algue verte

##### Ingrédients :

- 1 portions de bagasse
- 4 portions d’eau
- 1 feuille d’algue verte

![78](images/78.jpg)

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Algue Verte|non|marine|++++|+|cassable|brun |oui|difficile|de 9 heures|

### Recette 10 : Pâte à la bagasse + fécule de pomme de terre

##### Ingrédients :

- 1 portions de bagasse
- 4 portions d’eau
- 1 portion de fécule de pomme de terre
- 1 portion de vinaigre
- Colorant bleu ( non obligatoire )

##### Préparation :

- Dans une casserole verser l’eau, la bagasse et le vinaigre à feu doux
- Lorsque la bagasse est plus tendre, verser la fécule de pomme de terre petit à petit
- Une fois la pâte devenue gélatineuse, couper le feu
- Laisser refroidir
- Verser la pâte dans un moule

|![79](images/79.jpg)|![80](images/80.jpg)|
| --- | --- |

| |Moisissure|Odeur|Dureté|Flexibilité|Touché|Couleur|Transparence|Moulage|Temps de séchage|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Fécule pdt|non|agréable|+++++|+++++|agréable|vert|non|facile|5 heures|

**Remarques :** la texture et la dureté du mélange dépend de l’épaisseur de l’objet lors du moulage. Plus l’objet est fin, plus il sera friable et moins flexible. La recette de la fécule de pomme de terre donne des résultats satisfaisant à l’équivalent de la recette avec de l’agar agar.

### Recette 11 : Fécule de pomme de terre

##### Ingrédients :

- 1 kg de pomme de terre
- Mixeur
- 500 ml d’eau
- Filtre

##### Préparation :

- Eplucher les pommes de terre afin d’enlever la partie fibreuse
- Couper la pomme de terre en petit morceau
- Verser les cubes de pomme de terre dans un mixeur avec l’eau
- Mixer jusqu’à obtention d’une mélange homogène
- Filtrer plusieurs fois puis laisser reposer le liquide obtenu
- Verser le dépôt du liquide sur un cellophane . Le dépôt correspond à l’amidon
- Laisser sécher l’amidon jusqu’à obtention d’une texture déshydratée qui correspond à la fécule de pomme de terre.

|![81](images/81.jpg)|![82](images/82.jpg)|![83](images/83.jpg)|![84](images/84.jpg)|
| --- | --- | --- | --- |

**Remarques :** Pour 1 kilogramme de pomme de terre, nous obtenons 80 grammes de fécule de pomme de terre.
Fécule de pomme de terre est  l’amidon extrait des tubercules de pomme de terre. On l'emploie dans des applications techniques comme la colle à papier peint, les apprêts et enduits des tissus, le couchage et l'enduction du papier et comme adhésif dans la sacherie de papier et les rubans de papier gommé.

# Projet Final

En réponse à cela, on s’intéresse à la fabrication artisanale d’un jeu que chaque  cubain pourrait avoir chez lui et le fabriquer facilement pour les enfants.

### Nos objectifs :
- Utiliser les déchets végétaux (fibre végétal : la bagasse de canne à sucre) et ainsi obtenir une solution alternative aux jouets en plastique

### Créer un jeu qui permet à l’enfant :
- De le créer lui-même ( ou avec l’aide d’un adulte )
- D’utiliser les déchets alimentaires et les voir comme un matériel qui a du potentiel dans la fabrication d’objet
- De se familiariser avec les formes géométriques
- D’être Imaginatif et créatif
- De résoudre des problèmes
- De jouer avec de différentes manières
- De le Monter / le démonter à son gré
- De créer autant de pièces qu’il le souhaite
- Le jouet idéal imaginé est un parcours de bille qui sera fabriqué avec la recette de bagasse et de fécule de pomme de terre. Le parcours de bille sera composé d’un module répétitif qui sera fabriquer à partir d’une procédure de moulage de pâte de bagasse à l’intérieur d’un moule fabriqué pour.

![85](images/85.jpg)

## Prototype artisanal

|![86](images/86.jpg)|![87](images/87.jpg)|![88](images/88.jpg)|
| --- | --- | --- |

## Prototype 1 digital du moule

![89](images/89.jpg)

#### Ce qui ne fonctionne pas
Un premier essaie de moule composé de trois compartiments, qui n’était pas très concluant. Une fois avoir déposé la pâte à l’intérieur du mouleà cause de nous n’arrivions pas à extraire la pâte sans l’abimer.

## Prototype 2 digital du moule
Nous avons décidés de faire des ouvertures pour faciliter l’extraction de la pate.

![90](images/90.jpg)

## Montage du prototype artisanal avec des rails en fil de cuivre

![91](images/91.jpg)

## Montage du prototype digitale avec des rails imprimés

![92](images/92.jpg)

## Autre exemple de moulage

![93](images/93.jpg)

## Bibliographie


Cabalgata de Reyes magos (Chevauchée des Rois mages) en Espagne https://www.routard.com/guide_agenda_detail/4245/cabalgata_de_reyes_magos_(chevauchee_des_rois_mages)_en_espagne.htm , consulté le 12 décembre 2019

L’esprit de Noël à Cuba et le sens caché des traditions, http://www.fides.org/fr/news/3291-AMERIQUE_CUBA_On_recommence_a_vivre_l_esprit_de_Noel_dans_la_societe_cubaine_mais_beaucoup_ont_oublie_ou_ignorent_le_sens_chretien_de_ces_traditions , consulté le 12 décembre 2019

 Cuban Children and Their Toys, Miguel Arias Sanchez , 18 juillet 2017, https://havanatimes.org/diaries/miguel-ariass-diary/cuban-children-and-their-toys/ , consulté le 10 décembre 2019.

The eternal drama of toys in Cuba,  Cuba Headlines , 03 juin 2019,
https://www.cubaheadlines.com/2019-03-06-p1-the-eternal-drama-of-toys-in-cuba-that-they-take-them-out-in-the-industrial-stores-not, consulté le 10 décembre 2019

Photographies par Sebastian Gil Miranda Photograph http://www.sebastiangilmiranda.com/children-playing-on-the-street-of-cuba.html, consulté le 10 décembre 2019

Éternel cauchemar des jouets cubains, https://www.cubaheadlines.com/2019-03-06-p1-the-eternal-drama-of-toys-in-cuba-that-they-take-them-out-in-the-industrial-stores-not, consulté le 21 décembre 2019

Vidéo du touriste à Cuba ,
https://www.youtube.com/watch?v=WHD-Y0ST7-E , consulté le 30 novembre 2019

Globe terrestre en forme de puzzle casse tête,  https://www.thingiverse.com/thing:3401688 , consulté le 3 décembre 2019

Histoire du sucre cubain , https://www.cubania.com/blog/cubania-2/post/histoire-du-sucre-cubain-1-3-332, consulté le 9 décembre 2019, https://www.doc-developpement-durable.org/file/Culture-canne-a-sucre/Canne-a-sucre_Wikipedia.pdf

 Agar agar, https://www.passeportsante.net/fr/Solutions/PlantesSupplements/Fiche.aspx?doc=agar-agar_nu, consulté le 9 décembre 2019
Fécule de pomme de terre,
https://fr.wikipedia.org/wiki/F%C3%A9cule_de_pomme_de_terre, consulté le 21 décembre 2019  


Articles concernant les jeux traditionnels cubains joués par les enfants:

https://www.cibercuba.com/lecturas/los-7-juegos-infantiles-mas-populares-en-cuba
https://www.cubanet.org/actualidad-destacados/se-pierden-los-juegos-infantiles-en-cuba
