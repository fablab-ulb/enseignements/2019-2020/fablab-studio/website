# Planteur de patates

## Processus de recherche

### Les recherches et étape d'amélioration :

### Le déplacement :
Stabilité rechercher et roue qu il ne doivent pas se trouver dans l'axe centrale.
<br> 1) Avec deux cadres de vélo = Trouver deux vélos identiques pour faire correspondre les axes, presque impossible
<br>2) Avec un seul cadre de vélo, deux roues et des skis = pas assez stable, manque le système de mise en rotation du dépose patate.
<br>3) Amélioration : un seul châssis avec quatre roues dont deux plus larges, jantes de mobylette, cela s'enfonce moins dans la terre.

### Les outils (creuser le sillon et reboucher)
#### Solidité, réglable en hauteur, dimension.
<br>1) Avec des bidons en plastique vissés et découpés, à fixer sur la fourche avant pour le rebouchage, trop souple, trop difficile a modeler.
<br>2) Utilisation de la deuxième partie du baril de 200L pour y découper tous les outils, solides et déformables.
#### Réglage outil :
<br>3) Blocage de l'outil avant à une certaine hauteur grâce à une vis prés de l'axe = pas pratique pour y accéder un seul niveau.
<br>4) Réglage de l'outil avant grâce à un câble de frein et plusieurs morceaux de pédales fixés sur le baril, ils font office de crochet pour mettre l'outil plus ou moins haut.
<br>5) Outil arrière fixé directement sur la fourche = donc pas de réglage de hauteur possible 
<br>6) Outil fixé sur un autre élément (une fourche) elle sera fixée à l'axe de rotation avant, ce qui permet une rotation et donc un réglage possible a l'aide de vis de niveau puis un morceau de pédale.

### Le réservoir :
Contenir une quantité de patates, les faire circuler.
<br>1) Découpe du baril de 200L en deux utilisations, d'un seul fixé sur le guidon et la barre supérieur du cadre = forme de base peut adapter pour l'écoulement des patates.
<br>2) Ajout de paroi en métal pour créer un goulot proche de la où vont sortir les patates

### La dépose :
Déposer la patate à intervalle régulier, la récupérer du conduit.
<br>1) Grille de ventilateur avec des bouteille en plastique coupées = plastique peu solide
<br>2) Remplacement du plastique par des boîtes de conserve en plus, dimension plus grande.

### Encore en recherche :
<br>Liaison conduit/dépose.
<br>1) Souci du nombre de patates qui tombe dans un seul emplacement, puis écoulement difficile 