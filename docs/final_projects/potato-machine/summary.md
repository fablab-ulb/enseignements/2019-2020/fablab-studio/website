# Potato Planting Machine

### Summary

![schema](image/shema-patata.JPG)


### Problématique (FR) :

Pendant et aprés la période speciale le mode d'agriculture a changé, le pétrole n'est plus disponible ainsi que les produits chimiques du fait de la rupture des importations des Etats-Unis ainsi que de la chute de l'Union Soviétique. De ce fait, les cubains se sont adaptés à une autre manière de cultiver. Pas de produits chimiques, tout est bio, et les procédés sont manuels en majorité.
- Sur l'ile la patate est un aliment trés cultivé, mais le procedé de cette culture est délicat et nécessite beaucoup de main d'oeuvre.


### Question :

> Comment, avec le matériel trouvé sur l'ile pourrait-on planter des patates en symplifiant la manutention ?

_Hypothèse :_

Pour ce faire, j'ai décidé de fabriquer une planteuse de patates, qui effetuerait ces trois actions (creuser sillon, déposer la patate, reboucher en faisant un monticule). Cela de façon mécanique, qui serait tiré par un animal. Toute la conception serait faite avec des matériaux de récuperation se trouvant sur l'île.
La structure principale (un cadre de velo), système de dépose de la patate à intervalle régulier (grille de ventilateur, bouteille en plastique), le réservoir (un demi baril en acier), les outils pour creuser et reboucher (découpés dans l'autre partie du baril

_____________________

### Problematic (EN):

During and after the special period the mode of agriculture changed, petroleum and chemical products were no longer available due to the break in imports from the United States and the fall of the Soviet Union. As a result, Cubans have adapted to another way of farming. No chemicals, everything is organic, and most of the processes are manual.
- On the island the potato is a very cultivated food, but the process of this culture is delicate and requires a lot of manpower.


### Question:

> How, with the equipment found on the island, could we plant potatoes by simplifying handling?

_Hypothesis :_

To do this, I decided to make a potato planter, which would effect these three actions (digging a furrow, depositing the potato, filling up with a mound). This mechanically, which would be pulled by an animal. The whole design would be made with salvage materials found on the island.
The main structure (a bicycle frame), system for removing the potato at regular intervals (fan grille, plastic bottle), the tank (half a steel barrel), the tools for digging and filling up (cut out of the other part of the barrel

_____________________

### Problemática (ES):

Durante y después del período especial, el modo de agricultura cambió, el petróleo y los productos químicos ya no estaban disponibles debido a la interrupción de las importaciones de los Estados Unidos y la caída de la Unión Soviética. Como resultado, los cubanos se han adaptado a otra forma de agricultura. Sin productos químicos, todo es orgánico, y la mayoría de los procesos son manuales.
- En la isla, la papa es un alimento muy cultivado, pero el proceso de esta cultura es delicado y requiere mucha mano de obra.


### Pregunta :

> ¿Cómo, con el equipo encontrado en la isla, podríamos plantar papas simplificando el manejo?

_ Hipótesis: _

Para hacer esto, decidí hacer una sembradora de papas, lo que afectaría estas tres acciones (cavar un surco, depositar la papa, llenarla con un montículo). Esto mecánicamente, que sería tirado por un animal. Todo el diseño se haría con materiales de rescate encontrados en la isla.
La estructura principal (un cuadro de bicicleta), el sistema para quitar la papa a intervalos regulares (rejilla del ventilador, botella de plástico), el tanque (medio barril de acero), las herramientas para excavar y llenar (recortar otra parte del barril
