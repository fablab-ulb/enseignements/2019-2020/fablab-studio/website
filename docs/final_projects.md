# ULB - Fablab Studio

## Final Projects summaries

* [Adapto - Adapted Toys](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/adapto-adapted-toys/summary/)
* [Emergency generator](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/emergency-generator/summary/)
* [Meteorological Buoy](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/meteorological-buoy/summary/)
* [parasismic Adobe](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/adobe-parasismique/summary/)
* [Domestic Agriculture](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/domestic-culture/summary/)
* [UV Water Filter](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/filtre-uv/summary/)
* [Solar Cooker](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/filtre-uv/summary/)
* [Artisanal Toys](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/jeu-artisanat-enseignement/summary/)
* [Plastic Ropes](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/plastic-ropes/summary/)
* [Potato Machine](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/potato-machine/summary)
* [One Chair One World](https://fablab-ulb.gitlab.io/enseignements/2019-2020/fablab-studio/website/final_projects/one-chair-one-world/summary)
